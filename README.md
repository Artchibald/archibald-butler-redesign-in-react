This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

IMPORTANT MUST RUN V node 16 only nothing above!

Use nvm to downgrade node, works like a charm;

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
source ~/.nvm/nvm.sh
nvm install 10.13

watch tuto 
https://www.youtube.com/watch?v=ohBFbA0O6hs&t=426s

#to do
creat 404
react seo gatsby
ab home pop up 
all artwork needs timer
earthgang link post wrong
change thumbs per page to 6!
relink spinning icons in posts
Link to top of page in hash router work tile 9 bug
remove this from prod projects/archibald-butler-redesign-2019/about/
Maybe add a 1 second load delay to each artwork piece to avoid unsytled flash with fade in
add home link to logo top right
owl not loading on 4g reduce all?
unify all links
dont forget 404

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
