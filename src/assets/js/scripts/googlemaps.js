import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const ArchibaldButlerMap = ({ text }) => <div style={{ backgroundColor: "white", padding: '5px', fontSize: '14px', minWidth: '90px', fontFamily: 'Josefin Sans' }}>{text}</div >;

class SimpleMap extends Component {
    static defaultProps = {
        center: {
            lat: 51.510408,
            lng: -0.123818
        },
        zoom: 11
    };

    render() {
        return (
            // Important! Always set the container height explicitly
            <div style={{ height: '100%', width: '100%', minHeight: '300px' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyD90crMEQZelNU5dCnacR-DLOcv7_BB9dE" }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}

                >
                    <ArchibaldButlerMap
                        lat={51.510208}
                        lng={-0.12381}
                        text=" &#x2196; My&nbsp;office is&nbsp;here"
                    />
                </GoogleMapReact>
            </div>
        );
    }
}

export default SimpleMap;
