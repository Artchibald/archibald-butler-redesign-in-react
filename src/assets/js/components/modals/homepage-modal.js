import React, { Component } from 'react';
import '../../../css/c_modals.scss';
import { BrowserRouter as Route } from "react-router-dom";
// import { BrowserRouter as Link } from "react-router-dom";
class HomePageModal extends Component {
    //import artBg from '../img/archies-art.png';



    render() {
        return (
            <div className="modal fade" id="intro-text-1" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                <Route />
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">
                                <span
                                    aria-hidden="true">
                                    <i className="fa fa-times"></i>
                                </span></button>
                            <h4 className="modal-title pr-3" id="myModalLabel">About the creator of this site....</h4>
                        </div>
                        <div className="modal-body" id="myInput">
                            <h1>Your web animation specialist in&nbsp;London</h1>
                            <p>Hi! I am a London raised freelance web animation specialist, illustrator and web developer with 10 years
 of experience focusing on my main passion: Animating CSS in full-width web pages.
 If you need a free quote or my day rate, best to get in touch. I am also looking
 for commissions. Get your home page customized today!</p>


                            {/*  <Link
                                data-dismiss="modal"
                                className="btn btn-primary"
                                to="/about/"
                            >
                                About</Link>

                            <Link
                                data-dismiss="modal"
                                className="btn btn-primary"
                                to="/work/"
                            >
                                Portfolio</Link>

                            <Link
                                data-dismiss="modal"
                                className="btn btn-primary"
                                to="/news-1/"
                            >
                                News</Link>  */}
                            <h2 className="mt-4">Tech stack</h2>
                            <p>I am proficient in the following stack:</p>

                            <button type="button" className="btn btn-primary skill1">Web design</button><button type="button"
                                className="btn btn-primary skill1">UX/UI</button><button type="button"
                                    className="btn btn-primary skill1">key skills</button>

                            <button type="button" className="btn btn-primary skill2">Adobe Suite</button><button type="button"
                                className="btn btn-primary skill2">.Indd</button><button type="button"
                                    className="btn btn-primary skill2">.AI</button><button type="button"
                                        className="btn btn-primary skill2">.PSD</button>

                            <button type="button" className="btn btn-primary skill3">HTML5</button><button type="button"
                                className="btn btn-primary skill3">Foundation</button><button type="button"
                                    className="btn btn-primary skill3">Bootstrap</button>

                            <button type="button" className="btn btn-primary skill1">CSS</button><button type="button"
                                className="btn btn-primary skill1">SASS</button><button type="button"
                                    className="btn btn-primary skill1">SCSS</button><button type="button"
                                        className="btn btn-primary skill1">LESS</button>

                            <button type="button" className="btn btn-primary skill2">Compile</button><button type="button"
                                className="btn btn-primary skill2">Grunt</button><button type="button"
                                    className="btn btn-primary skill2">Gulp</button><button type="button"
                                        className="btn btn-primary skill2">Node</button><button type="button"
                                            className="btn btn-primary skill2">LiveReload</button>

                            <button type="button" className="btn btn-primary skill3">Repos</button><button type="button"
                                className="btn btn-primary skill3">Github</button><button type="button"
                                    className="btn btn-primary skill3">Bitbucket</button>

                            <button type="button" className="btn btn-primary skill1">JS</button><button type="button"
                                className="btn btn-primary skill1">jQuery</button><button type="button"
                                    className="btn btn-primary skill1">React</button><button type="button"
                                        className="btn btn-primary skill1">Angular</button>

                            <button type="button" className="btn btn-primary skill2">PHP</button><button type="button"
                                className="btn btn-primary skill2">Wordpress</button><button type="button"
                                    className="btn btn-primary skill2">Drupal</button><button type="button"
                                        className="btn btn-primary skill2">Custom</button>

                            <button type="button" className="btn btn-primary skill3">SQL</button><button type="button"
                                className="btn btn-primary skill3">PhpMyAdmin</button><button type="button"
                                    className="btn btn-primary skill3">DB manipulation</button><button type="button"
                                        className="btn btn-primary skill3">SQL Scripts</button>
                            <h2 className="mt-4">Offering</h2>
                            <p>It is best to hire me on an hourly rate which varies depending on whether it is within normal office
                            hours or not. Occasionally, I do take on websites per job. Priority is given to in-house jobs rather
 than remote work as I prefer to work in a team environment. For my rates, please drop me an email.</p>
                            <h2 className="mt-4">Background</h2>
                            <p>Born in London, my parents moved me to France as a child for ten years. I spent my childhood "drawing my
                            way" through school. You would also have found me practicing my surf on the beach in the Medoc(near
 Bordeaux(glug glug)).</p>

                            <p>Nowadays, I am based back in London working as a freelance web developer. I am also specialized in
                            React.js. Some of my clients include top tech, large design and marketing agencies with some fashion
 thrown in the mix here and there(ASOS, New Look)!</p>
                            <h2 className="mt-4">Commissioned illustrations</h2>
                            <p>I am fascinated with the UX possibilities the web offers. In my spare time, I like to learn new tech
                            through creating digital web artwork. I am looking for commissions in this type of CSS animation
 projects. Let me know if you would be interested in some artwork similar to my homepage.</p>

                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default correct-align-btn" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default HomePageModal;