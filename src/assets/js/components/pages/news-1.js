import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
//seo
import { Helmet } from "react-helmet";
import "../../../css/h_thumbs.scss";

import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";
import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";

import cruzImg from "../../../img/internals/work/animated-gif-illustration-animation-thumbnail.gif";
import killuImg from "../../../img/internals/work/killuminati-animated-political-cartoon.gif";
import asosImg from "../../../img/internals/work/asos.jpg";
import studyrightImg from "../../../img/internals/work/gif-css-animate-illustration.gif";
import GucciGigImg from "../../../img/internals/gucci-gig/gucci-gig-gif.gif";
import deliverooImg from "../../../img/internals/work/Deliveroo-freelancing-london.jpg";

class News1 extends Component {
 componentDidMount() {
  //make sure we load at top of page
  window.scrollTo(0, 0);
 }

 render() {
  return (
   <div className="container-fluid news">
    <Helmet>
     <meta charSet="utf-8" />
     <title>
      News and Web animation work by Archibald Butler, a specialist based in
      London.
     </title>
     <meta
      name="description"
      content="Checkout my news! I also have web projects for download too! Have a browse and let me know what you think!"
     />
    </Helmet>
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="Gucci Gig with Dazed and Confused" src={GucciGigImg} />
       <figcaption>
        <h2>
         DAZED AND <span>CONFUSED</span>
        </h2>
        <p>In partnership with GUCCI. Built in React.js.</p>
        <Link to="/gucci-gig/">View more</Link>
       </figcaption>
      </figure>
     </div>

     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An animated web design illustration by Archibald Butler"
        src={cruzImg}
       />
       <figcaption>
        <h2>
         Commission for <span>Cruz</span>
        </h2>
        <p>A hand illustrated animation commission that the client loves!</p>
        <Link to="/cruz-portfolio/">See it</Link>
       </figcaption>
      </figure>
     </div>

     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A Gambling website designed by Archibald Butler"
        src={deliverooImg}
       />
       <figcaption>
        <h2>
         Working at <span>Deliveroo</span>
        </h2>
        <p>I had the pleasure of freelancing there recently!</p>
        <Link to="/freelancing-at-deliveroo-hq-in-london/">View more</Link>
       </figcaption>
      </figure>
     </div>

     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A website illustration for production that never completed. An illustration by Archibald Butler"
        src={killuImg}
       />
       <figcaption>
        <h2>
         Kill<span>uminati</span>
        </h2>
        <p>My first political cartoon animation</p>

        <Link to="/killuminati-portfolio/">See it</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A bespoke web dev job on Tumblr for ASOS by Archibald Butler"
        src={asosImg}
       />
       <figcaption>
        <h2>
         I worked freelance at <span>ASOS for 3 years</span>
        </h2>
        <p>Most fun and friendly place I have ever worked!</p>

        <Link to="/asos-jobs/">See it</Link>
       </figcaption>
      </figure>
     </div>

     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="Skateboard illustration of Archie comics"
        src={studyrightImg}
       />
       <figcaption>
        <h2>
         New <span>illustrations!</span>
        </h2>
        <p>Been busy drawing and coding for new clients</p>
        <Link to="/study-right/">See it</Link>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/work/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-2/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News1;
