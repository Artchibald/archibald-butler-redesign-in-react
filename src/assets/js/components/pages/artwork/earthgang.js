import React, { Component } from 'react';
import { TimelineLite } from "gsap/TweenMax";
import { BrowserRouter as Route, Link } from "react-router-dom";
import '../../../../css/g_earthgang.scss';
import mainBgTile from '../../../../img/earthgang/background-earth-gang-illustration.jpg';
import circleLosImg from '../../../../img/earthgang/circle-losanges.png';
import toxicCloud from '../../../../img/earthgang/toxic-cloud.png';
import eyeSprite from '../../../../img/earthgang/eye-sprite.png';
import logoBg from '../../../../img/earthgang/logo-bg.png';
import EForEarthImg from '../../../../img/earthgang/e-for-earth.png';
import AForEarthImg from '../../../../img/earthgang/a-for-earth.png';
import RForEarthImg from '../../../../img/earthgang/r-for-earth.png';
import TForEarthImg from '../../../../img/earthgang/t-for-earth.png';
import HForEarthImg from '../../../../img/earthgang/h-for-earth.png';
import gangImg from '../../../../img/earthgang/gang.png';
import maryImg from '../../../../img/earthgang/mary-j.png';
import hqImg from '../../../../img/earthgang/hq.png';
import arrow from '../../../../img/earthgang/arrow.png';
import tinyJohnyImg from '../../../../img/earthgang/tiny-johny.png';
import tinyHeadImg from '../../../../img/earthgang/tiny-head.png';
import sparkleTooth from '../../../../img/earthgang/sparkle.gif';
import pyramidImg from '../../../../img/earthgang/pyramid.png';
import spinningNutImg from '../../../../img/earthgang/spinning-nut.png';
import elLeft from '../../../../img/earthgang/el-left.png';
import elRight from '../../../../img/earthgang/el-right.png';
import elTop from '../../../../img/earthgang/el-top.png';
import demon from '../../../../img/earthgang/demon.png';
import demonArm from '../../../../img/earthgang/demon-arm.png';
import goldBg from '../../../../img/earthgang/gold.gif';
import shipOne from '../../../../img/earthgang/ship-one.png';
import shipOneFireOne from '../../../../img/earthgang/ship-one-fire-one.png';
import shipOneFireTwo from '../../../../img/earthgang/ship-one-fire-two.png';
import smoke from '../../../../img/earthgang/smoke.png';
import speakerOne from '../../../../img/earthgang/speaker.png';
import speakerTwo from '../../../../img/earthgang/speaker-2.png';
import speakerThree from '../../../../img/earthgang/speaker-3.png';
import artistOneHead from '../../../../img/earthgang/artist-head.png';
import artistOneArmOne from '../../../../img/earthgang/artist-1-arm-1.png';
import artistOneArmTwo from '../../../../img/earthgang/artist-1-arm-2.png';
import shipTwo from '../../../../img/earthgang/ship-two.png';
import artistTwoHead from '../../../../img/earthgang/artist-head-2.png';
import artistTwoArmOne from '../../../../img/earthgang/artist-2-arm-1.png';
import artistTwoArmTwo from '../../../../img/earthgang/artist-2-arm-2.png';
import bigSpeaker from '../../../../img/earthgang/big-speaker.png';
import footerBg from '../../../../img/earthgang/footer-bg.png';
import footerBg2 from '../../../../img/earthgang/footer-bg-2.jpg';
import cameraFlashes from '../../../../img/earthgang/camera-flash.png';
import epComingSoon from '../../../../img/earthgang/ep-coming-soon.png';

import ArrowUp from '../../../../img/arrow-up.png';
import ArrowDown from '../../../../img/arrow-up.png';



var mainBgTileImgPull = {
  backgroundImage: 'url(' + mainBgTile + ')'
};

var elLeftImgPull = {
  backgroundImage: 'url(' + elLeft + ')'
};

var elRightImgPull = {
  backgroundImage: 'url(' + elRight + ')'
};
var lavaOneImgPull = {
  backgroundImage: 'url(' + goldBg + ')'
};

class EarthGang extends Component {

  constructor(props) {
    super(props);
    this.circleLosImg = null;
    this.toxicCloud = null;
    this.eyeSprite = null;
    this.logoBg = null;
    this.moveEForEarth = null;
    this.moveAForEarth = null;
    this.moveRForEarth = null;
    this.moveTForEarth = null;
    this.moveHForEarth = null;
    this.gangImg = null;
    this.maryImg = null;
    this.hqImg = null;
    this.tinyJohnyImg = null;
    this.tinyHeadImg = null;
  }

  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    // change css to display header and footer well
    var headerBorderNone = document.getElementById('header');
    headerBorderNone.style.borderBottom = "0px";
    headerBorderNone.style.position = "absolute";
    var FooterPlacement = document.getElementById('footer');
    FooterPlacement.style.marginTop = "-2px";


    this.circleLosImg = new TimelineLite({ paused: false })
      .to(this.circleLosImg, 60, {
        rotation: 360,
        ease: "Linear.easeNone",
        repeat: -1
      });

    this.toxicCloud = new TimelineLite({ paused: false })
      .to(this.toxicCloud, 18, {
        xPercent: 0,
        left: "0%",
        yPercent: -1.5,
        top: "-1.5%",
        yoyo: true,
        ease: "Bounce.easeOut",
        y: 0,
        x: 0,
        opacity: 0.7,
        delay: .1,
        repeat: -1
      });

    this.eyeSprite = new TimelineLite({ paused: false })
      .to(this.eyeSprite, 1, {
        // xPercent:10,
        // left:"10%", 
        //  ease:"SteppedEase.config(16)", 
        //  repeat:-1
      });

    this.logoBg = new TimelineLite({ paused: false })
      .to(this.logoBg, 1, {
        yPercent: 1.2,
        top: "1.2%",
        opacity: 1,
        delay: 3,
        // yoyo: true,
        // repeat: -1,
        // repeatDelay: 6.58
      });

    this.moveEForEarth = new TimelineLite({ paused: false })
      .to(this.EForEarthImg, 1, {
        yPercent: 4,
        top: "4%",
        opacity: 1,
        delay: 3,
        // yoyo: true,
        // repeat: -1,
        // repeatDelay: 6.58
      });

    this.moveAForEarth = new TimelineLite({ paused: false })
      .to(this.AForEarthImg, 1, {
        yPercent: 4.2,
        top: "4.2%",
        opacity: 1,
        delay: 3.1,
        // yoyo: true,
        // repeat: -1,
        // repeatDelay: 6.58
      });

    this.moveRForEarth = new TimelineLite({ paused: false })
      .to(this.RForEarthImg, 1, {
        yPercent: 4.2,
        top: "4.2%",
        opacity: 1,
        delay: 3.2,
        // yoyo: true,
        // repeat: -1,
        // repeatDelay: 6.58
      });

    this.moveTForEarth = new TimelineLite({ paused: false })
      .to(this.TForEarthImg, 1, {
        yPercent: 4.2,
        top: "4.2%",
        opacity: 1,
        delay: 3.3,
        // yoyo: true,
        // repeat: -1,
        // repeatDelay: 6.58
      });

    this.moveHForEarth = new TimelineLite({ paused: false })
      .to(this.HForEarthImg, 1, {
        yPercent: 4.2,
        top: "4.2%",
        opacity: 1,
        delay: 3.4,
        // yoyo: true,
        // repeat: -1,
        // repeatDelay: 6.58
      });

    this.gangImg = new TimelineLite({ paused: false })
      .to(this.gangImg, 1, {
        xPercent: 48.9,
        left: "48.9%",
        yPercent: 3.1,
        top: "3.1%",
        force3D: true,
        scaleX: 1,
        scaleY: 1,
        transformOrigin: "50% 50%",
        ease: "Bounce.easeOut",
        y: 0,
        x: 0,
        opacity: 1,
        delay: 3.8,
        // yoyo: true,
        // repeat: -1,
        // repeatDelay: 6.58
      });

    this.maryImg = new TimelineLite({ paused: false })
      .to(this.maryImg, 3, {
        yPercent: 11.9,
        top: "11.9%",
        opacity: 1,
        repeat: -1,
        delay: 5.4,
        yoyo: true
      });
    this.hqImg = new TimelineLite({ paused: false })
      .to(this.hqImg, 3, {
        yPercent: 10.9,
        top: "10.9%",
        opacity: 1,
        repeat: -1,
        delay: 1.4,
        yoyo: true
      });

    this.tinyJohnyImg = new TimelineLite({ paused: false })
      .to(this.tinyJohnyImg, 3, {
        yPercent: 10.9,
        top: "10.9%",
        opacity: 1,
        repeat: -1,
        delay: 2,
        yoyo: true
      });

    this.tinyHeadImg = new TimelineLite({ paused: false })
      .to(this.tinyHeadImg, 3, {
        yPercent: 10.9,
        top: "10.9%",
        opacity: 1,
        repeat: -1,
        delay: 1,
        yoyo: true
      });
  }

  componentWillUnmount() {
    // change css back to display header and footer for internals
    var headerBorderNone = document.getElementById('header');
    headerBorderNone.style.borderBottom = "1px solid #000000";
    headerBorderNone.style.position = "relative";
    var FooterPlacement = document.getElementById('footer');
    FooterPlacement.style.marginTop = "0%";
  }

  render() {
    return (
      <main className="earthgang-content Crop-App-to-bg" style={mainBgTileImgPull}>
        <Route />
        <div id="navi-arrows">
          <Link
            className="navi-button"
            to="/"

          >
            <img id="arrow-up" className="img-fluid" alt="Archibald Butler: web design, surfing and travel blog" src={ArrowUp} />
          </Link>
          <Link
            className="navi-button2"
            to="/political-cartoon-animations-africas-diamonds/"

          >
            <img id="arrow-down" className="img-fluid" alt="Archibald Butler: web design, surfing and travel blog" src={ArrowDown} />
          </Link>
        </div>
        <div className="EarthGang" style={mainBgTileImgPull}>
          <img ref={img => this.circleLosImg = img} id="circleLosImg" src={circleLosImg} alt="Earthgang" />
          <img ref={img => this.toxicCloud = img} id="toxicCloud" src={toxicCloud} alt="Earthgang" />
          <img id="toxicCloud2" src={toxicCloud} alt="Earthgang" />
          <img id="toxicCloud3" src={toxicCloud} alt="Earthgang" />
          <div id="eyeSpriteContain">
            <a aria-label="artwork links" href="/">
              <img ref={img => this.eyeSprite = img} id="eyeSprite" src={eyeSprite} alt="Earthgang" />
            </a>
          </div>
          <img ref={img => this.logoBg = img} id="logoBg" className="logo-bg" src={logoBg} alt="Earthgang" />
          <a aria-label="artwork links" href="/">
            <img ref={img => this.EForEarthImg = img} id="EForEarthImg" src={EForEarthImg} alt="Earthgang" />
          </a>
          <a aria-label="artwork links" href="/">
            <img ref={img => this.AForEarthImg = img} id="AForEarthImg" src={AForEarthImg} alt="Earthgang" />
          </a>
          <a aria-label="artwork links" href="/">
            <img ref={img => this.RForEarthImg = img} id="RForEarthImg" src={RForEarthImg} alt="Earthgang" />
          </a>
          <a aria-label="artwork links" href="/">
            <img ref={img => this.TForEarthImg = img} id="TForEarthImg" src={TForEarthImg} alt="Earthgang" />
          </a>
          <a aria-label="artwork links" href="/">
            <img ref={img => this.HForEarthImg = img} id="HForEarthImg" src={HForEarthImg} alt="Earthgang" />
          </a>
          <a aria-label="artwork links" href="/">
            <img ref={img => this.gangImg = img} id="gangImg" src={gangImg} alt="Earthgang" />
          </a>
          <img ref={img => this.maryImg = img} id="maryImg" src={maryImg} alt="Earthgang" />
          <a aria-label="artwork links" href="/">
            <img ref={img => this.hqImg = img} id="hqImg" src={hqImg} alt="Earthgang" />
          </a>
          <img id="arrow" src={arrow} alt="Earthgang" />
          <div id="dudeOne">
            <a aria-label="artwork links" href="/">
              <img ref={img => this.tinyJohnyImg = img} id="tinyJohnyImg" src={tinyJohnyImg} alt="Earthgang" />
            </a>
            <img ref={img => this.sparkleTooth = img} id="sparkleTooth" src={sparkleTooth} alt="Earthgang" />
          </div>
          <div id="dudeTwo">
            <a aria-label="artwork links" href="/">
              <img ref={img => this.tinyHeadImg = img} id="tinyHeadImg" src={tinyHeadImg} alt="Earthgang" />
            </a>
            <img ref={img => this.sparkleTooth = img} id="sparkleTooth" src={sparkleTooth} alt="Earthgang" />
          </div>
          <div id="lavaOne" style={lavaOneImgPull}>
          </div>
          <div id="lavaTwo" style={lavaOneImgPull}>
          </div>
          <div id="lavaThree" style={lavaOneImgPull}>
          </div>
          <div id="lavaFour" style={lavaOneImgPull}>
          </div>
          <div id="lavaFive" style={lavaOneImgPull}>
          </div>
          <div id="lavaSix" style={lavaOneImgPull}>
          </div>
          <div id="lavaSeven" style={lavaOneImgPull}>
          </div>
          <div id="lavaEight" style={lavaOneImgPull}>
          </div>
          <div id="lavaNine" style={lavaOneImgPull}>
          </div>
          <div id="lavaTen" style={lavaOneImgPull}>
          </div>
          <div id="lavaEleven" style={lavaOneImgPull}>
          </div>
          <div id="lavaTwelve" style={lavaOneImgPull}>
          </div>
          <img id="pyramidImg" src={pyramidImg} alt="Earthgang" />
          <img id="spinningNutImg" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg2" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg3" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg4" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg5" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg6" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg7" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg8" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg9" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg10" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg11" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg12" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg13" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg14" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg15" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg16" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg17" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg18" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg19" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg20" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg21" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg22" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg23" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg24" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg25" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg26" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg27" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg28" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg29" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg30" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg31" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg32" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg33" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg34" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg35" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg36" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg37" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <img id="spinningNutImg38" className="spinningNuts" src={spinningNutImg} alt="Earthgang" />
          <div id="liftLeftWrap">
            <div id="elLeft" style={elLeftImgPull}>
            </div>
          </div>
          <div id="liftRightWrap">
            <div id="elRight" style={elRightImgPull}>
            </div>
          </div>
          <img id="elTop" className="elTop" src={elTop} alt="Earthgang" />
          <img id="demon" className="demon" src={demon} alt="Earthgang" />
          <img id="demonArm" className="demonArm" src={demonArm} alt="Earthgang" />
          <a aria-label="artwork links" href="/">
            <div id="spaceshipOne">
              <div id="spaceshipOneFireOneContain">
                <img id="shipOneFireOne" className="shipOneFireOne" src={shipOneFireOne} alt="Earthgang" />
              </div>
              <div id="spaceshipOneFireTwoContain">
                <img id="shipOneFireTwo" className="shipOneFireTwo" src={shipOneFireTwo} alt="Earthgang" />
              </div>
              <img id="shipOne" className="shipOne" src={shipOne} alt="Earthgang" />
              <div id="smokeContain">
                <img id="smoke" src={smoke} alt="Earthgang" />
              </div>
              <div id="speakerOneContain">
                <img id="speakerOne" src={speakerOne} alt="Earthgang" />
              </div>
              <div id="speakerTwoContain">
                <img id="speakerTwo" src={speakerTwo} alt="Earthgang" />
              </div>
              <div id="speakerThreeContain">
                <img id="speakerThree" src={speakerThree} alt="Earthgang" />
              </div>

              <img id="artistOneHead" src={artistOneHead} alt="Earthgang" />
              <img id="artistOneArmOne" src={artistOneArmOne} alt="Earthgang" />
              <img id="artistOneArmTwo" src={artistOneArmTwo} alt="Earthgang" />
            </div>
          </a>
          <a aria-label="artwork links" href="/">
            <div id="spaceshipTwo">

              <div id="spaceshipOneFireOneContain">
                <img id="shipOneFireOne" className="shipOneFireOne" src={shipOneFireOne} alt="Earthgang" />
              </div>
              <div id="spaceshipOneFireTwoContain">
                <img id="shipOneFireTwo" className="shipOneFireTwo" src={shipOneFireTwo} alt="Earthgang" />
              </div>
              <img id="shipTwo" className="shipTwo" src={shipTwo} alt="Earthgang" />
              <div id="speakerOneContain">
                <img id="speakerOne" src={speakerOne} alt="Earthgang" />
              </div>
              <div id="speakerTwoContain">
                <img id="speakerTwo" src={speakerTwo} alt="Earthgang" />
              </div>
              <div id="speakerThreeContain">
                <img id="speakerThree" src={speakerThree} alt="Earthgang" />
              </div>
              <img id="artistTwoHead" src={artistTwoHead} alt="Earthgang" />
              <img id="artistTwoArmOne" src={artistTwoArmOne} alt="Earthgang" />
              <img id="artistTwoArmTwo" src={artistTwoArmTwo} alt="Earthgang" />
            </div>
          </a>
          <div id="bigSpeakerContain">
            <img id="bigSpeaker" src={bigSpeaker} alt="Earthgang" />
          </div>
          <div id="bigSpeakerContain2">
            <img id="bigSpeaker2" src={bigSpeaker} alt="Earthgang" />
          </div>
          <div id="bigSpeakerContain3">
            <img id="bigSpeaker3" src={bigSpeaker} alt="Earthgang" />
          </div>
          <div id="bigSpeakerContain4">
            <img id="bigSpeaker4" src={bigSpeaker} alt="Earthgang" />
          </div>
          <div id="bigSpeakerContain5">
            <img id="bigSpeaker5" src={bigSpeaker} alt="Earthgang" />
          </div>
          <div id="bigSpeakerContain6">
            <img id="bigSpeaker6" src={bigSpeaker} alt="Earthgang" />
          </div>
          <div id="bigSpeakerContain7">
            <img id="bigSpeaker7" src={bigSpeaker} alt="Earthgang" />
          </div>
          <img id="footerBg" src={footerBg} alt="Earthgang" />
          <img id="footerBg2" src={footerBg2} alt="Earthgang" />
          <div id="cameraFlashesContain">
            <img id="cameraFlashes" src={cameraFlashes} alt="Earthgang" />
          </div>
          <div id="cameraFlashesContain2">
            <img id="cameraFlashes2" src={cameraFlashes} alt="Earthgang" />
          </div>
          <div id="cameraFlashesContain3">
            <img id="cameraFlashes3" src={cameraFlashes} alt="Earthgang" />
          </div>
          <div id="cameraFlashesContain4">
            <img id="cameraFlashes4" src={cameraFlashes} alt="Earthgang" />
          </div>
          <div id="cameraFlashesContain5">
            <img id="cameraFlashes5" src={cameraFlashes} alt="Earthgang" />
          </div>
          <div id="cameraFlashesContain6">
            <img id="cameraFlashes6" src={cameraFlashes} alt="Earthgang" />
          </div>
          <div id="cameraFlashesContain7">
            <img id="cameraFlashes7" src={cameraFlashes} alt="Earthgang" />
          </div>
          <a aria-label="artwork links" href="/">
            <img id="epComingSoon" src={epComingSoon} alt="Earthgang" />
          </a>
        </div>
      </main>
    );
  }
}

export default EarthGang;