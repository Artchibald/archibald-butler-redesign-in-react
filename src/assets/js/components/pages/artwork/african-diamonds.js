import React, { Component } from 'react';
import { TimelineLite } from "gsap/TweenMax";
import '../../../../css/g_african_diamonds.scss';
import { BrowserRouter as Route, Link } from "react-router-dom";
//import { BrowserRouter as Link } from "react-router-dom"; 

//images
import sky from '../../../../img/african-diamonds/sky-illustration.jpg';
import mountains from '../../../../img/african-diamonds/mountains-illustration.png';
import hills from '../../../../img/african-diamonds/hills-illustration.png';
import sand from '../../../../img/african-diamonds/sand-illustration.png';
import cloud1Img from '../../../../img/african-diamonds/cloud1.png';
import cloud2Img from '../../../../img/african-diamonds/cloud2.png';
import cloud3Img from '../../../../img/african-diamonds/cloud3.png';
import sludge1img from '../../../../img/african-diamonds/sludge1.png';
import sludge2img from '../../../../img/african-diamonds/sludge2.png';
import sludge3img from '../../../../img/african-diamonds/sludge3.png';
import legsimg from '../../../../img/african-diamonds/legs.gif';
import sparkleimg from '../../../../img/african-diamonds/sparkle.gif';
import contentimg from '../../../../img/african-diamonds/content.png';
import tvscreenimg from '../../../../img/african-diamonds/tv-screen.gif';
import phoneimg from '../../../../img/african-diamonds/phone.gif';
import tabletimg from '../../../../img/african-diamonds/tablet.gif';
import ArrowUp from '../../../../img/arrow-up.png';
import ArrowDown from '../../../../img/arrow-up.png';


var mountainsBg = {
    backgroundImage: `url(${mountains})`
};
var hillsBg = {
    backgroundImage: `url(${hills})`
};
var sandBg = {
    backgroundImage: `url(${sand})`
};

class AfricanDiamonds extends Component {
    //greensock
    constructor(props) {
        super(props);
        this.mountainsAnim = null;
        this.hillsAnim = null;
        this.sandAnim = null;
        this.cloud1 = null;
        this.cloud2 = null;
        this.cloud3 = null;
        this.sludge1 = null;
        this.sludge2 = null;
        this.sludge3 = null;

    }


    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        // change css to display header and footer well
        var headerBorderNone = document.getElementById('header');
        headerBorderNone.style.borderBottom = "0px";
        headerBorderNone.style.position = "absolute";
        var FooterPlacement = document.getElementById('footer');
        FooterPlacement.style.marginTop = "-2px";

        this.mountainsAnim = new TimelineLite({ paused: false })
            .from(this.mountainsBg, 800, { repeat: -1, repeatDelay: 0, backgroundPosition: "-=13500px" });

        this.hillsAnim = new TimelineLite({ paused: false })
            .from(this.hillsBg, 400, { repeat: -1, repeatDelay: 0, backgroundPosition: "-=13500px" });

        this.sandAnim = new TimelineLite({ paused: false })
            .from(this.sandBg, 200, { repeat: -1, repeatDelay: 0, backgroundPosition: "-=13500px" });

        this.cloud1 = new TimelineLite({ paused: false })
            .from(this.cloud1Img, 50, { repeat: -1, repeatDelay: 0, xPercent: 450, left: "450%" });

        this.cloud2 = new TimelineLite({ paused: false })
            .from(this.cloud2Img, 40, { repeat: -1, repeatDelay: 0, xPercent: 450, left: "450%" });

        this.cloud3 = new TimelineLite({ paused: false })
            .from(this.cloud3Img, 30, { repeat: -1, repeatDelay: 0, xPercent: 450, left: "450%" });

        this.sludge1 = new TimelineLite({ paused: false })
            .to(this.sludge1Img, 11.9, { repeat: -1, repeatDelay: 5, scale: 5, xPercent: 110, left: "110%" });

        this.sludge2 = new TimelineLite({ paused: false })
            .to(this.sludge2Img, 11.9, { repeat: -1, repeatDelay: 5, delay: 5, scale: 5, xPercent: 110, left: "110%" });

        this.sludge3 = new TimelineLite({ paused: false })
            .to(this.sludge3Img, 11.9, { repeat: -1, repeatDelay: 5, delay: 10, scale: 5, xPercent: 110, left: "110%" });

        // //sludge1
        // TweenLite.defaultEase = Power0.easeNone;
        // var sludge1Anim = new TimelineMax({repeat:-1, repeatDelay:5});
        // sludge1Anim.to(sludge1, 11.9, {scale:5, xPercent:110, left:"110%", yPercent:-1, top:"1%", force3D:true});
        // //sludge2
        // TweenLite.defaultEase = Power0.easeNone;
        // var sludge2Anim = new TimelineMax({repeat:-1, repeatDelay:5});
        // sludge2Anim.to(sludge2, 11.9, {scale:5, delay:5, xPercent:110, left:"110%", yPercent:-1, top:"1%", force3D:true});
        // //sludge3
        // TweenLite.defaultEase = Power0.easeNone;
        // var sludge3Anim = new TimelineMax({repeat:-1, repeatDelay:5});
        // sludge3Anim.to(sludge3, 11.9, {scale:5, delay:10, xPercent:110, left:"110%", yPercent:-1, top:"1%", force3D:true});


    }
    componentWillUnmount() {
        // change css back to display header and footer for internals
        var headerBorderNone = document.getElementById('header');
        headerBorderNone.style.borderBottom = "1px solid #000000";
        headerBorderNone.style.position = "relative";
        var FooterPlacement = document.getElementById('footer');
        FooterPlacement.style.marginTop = "0%";
    }

    render() {
        return (
            <div className="african-diamonds-content Crop-App-to-bg">
                <img id="sky" alt="sky of illustration composition" src={sky} />
                <Route />
                <div id="navi-arrows">
                    <Link
                        className="navi-button"
                        to="/earthgang-an-html-art-project/"
                    >
                        <img id="arrow-up" className="img-fluid" alt="Archibald Butler: web design, surfing and travel blog" src={ArrowUp} />
                    </Link>
                    <Link
                        className="navi-button2"
                        to="/killuminati-first-political-cartoon/"
                    >
                        <img id="arrow-down" className="img-fluid" alt="Archibald Butler: web design, surfing and travel blog" src={ArrowDown} />
                    </Link>
                </div>
                <div id="mountains"
                    style={mountainsBg}
                    ref={img => this.mountainsBg = img}
                >
                </div>
                <div id="hills"
                    style={hillsBg}
                    ref={img => this.hillsBg = img}
                >
                </div>
                <div id="sand"
                    style={sandBg}
                    ref={img => this.sandBg = img}
                >
                </div>
                <div id="cloud1">
                    <img id="cloud1Img"
                        ref={img => this.cloud1Img = img}
                        src={cloud1Img}
                        alt="Archibald Butler's bespoke hand drawn web animation of Hilary Clinton" />
                </div>
                <div id="cloud2">
                    <img id="cloud2Img"
                        ref={img => this.cloud2Img = img}
                        src={cloud2Img}
                        alt="Archibald Butler's bespoke hand drawn web animation of Hilary Clinton" />
                </div>
                <div id="cloud3">
                    <img
                        ref={img => this.cloud3Img = img}
                        src={cloud3Img}
                        alt="Archibald Butler's bespoke hand drawn web animation of Hilary Clinton" />
                </div>

                <div id="content">
                    <img id="sludge1"
                        ref={img => this.sludge1Img = img}
                        src={sludge1img}
                        alt="Archibald Butler's bespoke hand drawn web animation of sludge" />
                    <img id="sludge2"
                        ref={img => this.sludge2Img = img}
                        src={sludge2img}
                        alt="Archibald Butler's bespoke hand drawn web animation of sludge" />
                    <img id="sludge3"
                        ref={img => this.sludge3Img = img}
                        src={sludge3img}
                        alt="Archibald Butler's bespoke hand drawn web animation of sludge" />
                    <img id="legs"
                        src={legsimg}
                        alt="Archibald Butler's bespoke hand drawn web animation of an unfair economy" />
                    <img id="content-piece"
                        src={sparkleimg}
                        alt="Archibald Butler's bespoke hand drawn web animation of a sparkle" />
                    <img id="content-piece"
                        src={contentimg}
                        alt="Archibald Butler's bespoke hand drawn web animation of the sun" />
                    <img id="tv-screen"
                        src={tvscreenimg}
                        alt="Archibald Butler's bespoke hand drawn web animation of the television" />
                    <img id="phone"
                        src={phoneimg}
                        alt="Archibald Butler's bespoke hand drawn web animation of the mobile phone" />
                    <img id="tablet"
                        src={tabletimg}
                        alt="Archibald Butler's bespoke hand drawn web animation of the mobile tablet" />
                </div>
                {/*
                    Comments
                    */}
            </div>
        );
    }
}
export default AfricanDiamonds;