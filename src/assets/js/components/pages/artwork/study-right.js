import React, { Component } from 'react';
import '../../../../css/g_study_right.scss';
//import { BrowserRouter as Link } from "react-router-dom";
import { BrowserRouter as Route, Link } from "react-router-dom";
//images
import bg from '../../../../img/study-right/cool-school.jpg';
import professor from '../../../../img/study-right/professor.gif';
import kid1 from '../../../../img/study-right/1tissue-thrower-animated.gif';
import kid2 from '../../../../img/study-right/2jumping-girl.gif';
import kid3 from '../../../../img/study-right/3sleeping-boy.gif';
import kid4 from '../../../../img/study-right/4slap-girl.gif';
import kid5 from '../../../../img/study-right/5study-boy.png';
import kid6 from '../../../../img/study-right/6study-girl.png';
import kid7 from '../../../../img/study-right/7study-girl.gif';
import kid8 from '../../../../img/study-right/8chewing-girl.gif';
import gumbuildup from '../../../../img/study-right/gumbuildup.gif';
import kid9 from '../../../../img/study-right/9study-boy.png';
import kid10 from '../../../../img/study-right/10fist-boy.gif';
import kid11 from '../../../../img/study-right/11game-boy.gif';
import kid12 from '../../../../img/study-right/12throw-boy.gif';
import paperbuildup from '../../../../img/study-right/paperbuildup.gif';
//import vectors from '../../../../img/study-right/vector-bubbles.gif';
// import science from '../../../../img/study-right/science.png';
// import math from '../../../../img/study-right/math.png';
// import english from '../../../../img/study-right/english.png';
// import creative from '../../../../img/study-right/creative.png';
import courses from '../../../../img/study-right/courses.gif';
import contact from '../../../../img/study-right/contact.gif';
import pricing from '../../../../img/study-right/pricing.gif';
import plane from '../../../../img/study-right/aeroplane.gif';
import logo from '../../../../img/study-right/study-right-logo.png';
import dog from '../../../../img/study-right/dog.gif';
import ArrowUp from '../../../../img/arrow-up.png';
import ArrowDown from '../../../../img/arrow-up.png';


class StudyRight extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        // change css to display header and footer well
        var headerBorderNone = document.getElementById('header');
        headerBorderNone.style.borderBottom = "0px";
        headerBorderNone.style.position = "absolute";
        var FooterPlacement = document.getElementById('footer');
        FooterPlacement.style.marginTop = "-2px";
    }
    componentWillUnmount() {
        // change css back to display header and footer for internals
        var headerBorderNone = document.getElementById('header');
        headerBorderNone.style.borderBottom = "1px solid #000000";
        headerBorderNone.style.position = "relative";
        var FooterPlacement = document.getElementById('footer');
        FooterPlacement.style.marginTop = "0%";
    }



    render() {
        return (
            <div className="study-right-content Crop-App-to-bg">
                <img id="background" alt="Private tuition in London" src={bg} />
                <Route />
                <div id="navi-arrows">
                    <Link
                        className="navi-button"
                        to="/killuminati-first-political-cartoon/"
                    >
                        <img id="arrow-up" className="img-fluid" alt="Archibald Butler: web design, surfing and travel blog" src={ArrowUp} />
                    </Link>
                    <Link
                        className="navi-button2"
                        to="/animate-css-stories/"
                    >
                        <img id="arrow-down" className="img-fluid" alt="Archibald Butler: web design, surfing and travel blog" src={ArrowDown} />
                    </Link>
                </div>
                <img id="professor" src={professor} alt="Private tuition in London" />
                <img id="kid1" src={kid1} alt="Private tuition in London" />
                <img id="kid2" src={kid2} alt="Private tuition in London" />
                <img id="kid3" src={kid3} alt="Private tuition in London" />
                <img id="kid4" src={kid4} alt="Private tuition in London" />
                <img id="kid5" src={kid5} alt="Private tuition in London" />
                <img id="kid6" src={kid6} alt="Private tuition in London" />
                <img id="kid7" src={kid7} alt="Private tuition in London" />
                <img id="kid8" src={kid8} alt="Private tuition in London" />
                <img id="gumbuildup" src={gumbuildup} alt="Private tuition in London" />
                <img id="kid9" src={kid9} alt="Private tuition in London" />
                <img id="kid10" src={kid10} alt="Private tuition in London" />
                <img id="kid11" src={kid11} alt="Private tuition in London" />
                <img id="kid12" src={kid12} alt="Private tuition in London" />
                <img id="paperbuildup" src={paperbuildup} alt="Private tuition in London" />
                <a aria-label="Artwork" href="/"><img id="courses" src={courses} alt="Private tuition in London" /></a>
                <a aria-label="Artwork" href="/"><img id="contact" src={contact} alt="Private tuition in London" /></a>
                <a aria-label="Artwork" href="/"><img id="pricing" src={pricing} alt="Private tuition in London" /></a>
                <img id="plane" src={plane} alt="Private tuition in London" />
                <a aria-label="Artwork" href="/"><img id="logo" alt="private tuition in London" src={logo} /></a>
                <img id="dog" alt="private tuition in London" src={dog} />
                <div>
                    {/*
                    <img id="vectors" src={vectors} alt="Private tuition in London" />
                                    <a aria-label="Artwork" href="courses.html#science"><img id="science" src={science} alt="Private tuition in London" /></a>
                <a aria-label="Artwork" href="courses.html#math"><img id="math" src={math} alt="Private tuition in London" /></a>
                <a aria-label="Artwork" href="courses.html#english"><img id="english" src={english} alt="Private tuition in London" /></a>
                <a aria-label="Artwork" href="courses.html#creative"><img id="creative" src={creative} alt="Private tuition in London" /></a>
                    style="display:none!important;"
                    <audio id="audio1" controls="controls" autoplay="" loop="" onloadeddata="setLowVolume()">
                        <source src="img/study-right-class-room-sounds.mp3" type="audio/mpeg">
                        </audio>
                 */}
                </div>


            </div>

        );
    }
}
export default StudyRight;







