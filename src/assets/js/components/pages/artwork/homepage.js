import React, { Component } from "react";
import { TimelineLite } from "gsap/TweenMax";
import "../../../../css/e_homepage.scss";
import "../../../../css/c_modals.scss";
import { BrowserRouter as Route, Link } from "react-router-dom";
import HomePageModal from "../../../../js/components/modals/homepage-modal.js";
//seo
import { Helmet } from "react-helmet";

//img
import backgroundJPG from "../../../../img/homepage/archibald-butler-web-design-surfing-travel-blog.jpg";
import backgroundWEBP from "../../../../img/homepage/archibald-butler-web-design-surfing-travel-blog.webp";
import thePixelLottery from "../../../../img/homepage/the-pixel-lottery.png";
import planeImgPNG from "../../../../img/homepage/illustrated-aeroplane.png";
import planeImgWEBP from "../../../../img/homepage/illustrated-aeroplane.webp";
import planeTrail from "../../../../img/homepage/illustrated-cloud-gif.gif";
import titleImg from "../../../../img/homepage/archibald-butler-a-roaming-alchemist-of-web-dev-and-illustration.png";
import ReactLogoImg from "../../../../img/homepage/react-logo.png";
import NowBuiltInImg from "../../../../img/homepage/now-built-in.png";
import ReactJsImg from "../../../../img/homepage/react-js.png";
import lightCanvasImg from "../../../../img/homepage/light-canvas.png";
import sunImg from "../../../../img/homepage/sun-person-office-illustration.png";
import sunTie from "../../../../img/homepage/arrow-tie-illustration.png";
import boatImg from "../../../../img/homepage/illustration-of-a-green-boat-for-animated-digital-web-design.png";
import mineImg from "../../../../img/homepage/illustration-of-an-ocean-mine-for-animated-digital-web-design.png";
import boatImg2 from "../../../../img/homepage/a-dream-of-surf-boat.png";
import cliffImg from "../../../../img/homepage/archibald-butler-web-design-cliff-illustration.png";
import gateImg from "../../../../img/homepage/illustration-of-a-magic-gate-for-animated-digital-web-design.png";
import apacheImg from "../../../../img/homepage/illustration-apache-animation.png";
import apacheAnim from "../../../../img/homepage/heli-animated-gif-parts.gif";
import oldLady from "../../../../img/homepage/old-woman-with-zimoframe-illustration-moving-gif-illustration.gif";
import hummerImg from "../../../../img/homepage/hummer-gang-illustration.png";
import humtrailImg from "../../../../img/homepage/hummer-trail-gif-animation-clouds-illustration.gif";
import coupleImg from "../../../../img/homepage/couple.gif";
import archieswebImg from "../../../../img/homepage/archies-web.png";
import beenyImg from "../../../../img/homepage/beeny.png";
import goddessImg from "../../../../img/homepage/goddess-fitness-dance.png";
import freddieImg from "../../../../img/homepage/freddie.png";
import asosImg from "../../../../img/homepage/asos.png";
import edwinsfordImg from "../../../../img/homepage/edwinsford.png";
import utopiaImg from "../../../../img/homepage/archies-utopia.png";
import odaImg from "../../../../img/homepage/our-design-agency.png";
import roamingImg from "../../../../img/homepage/roaming-giraffe.png";
import buddImg from "../../../../img/homepage/buddhist-fox.png";
import oneImg from "../../../../img/homepage/one-mega-collective.png";

import ArrowUp from "../../../../img/arrow-up.png";
import ArrowDown from "../../../../img/arrow-up.png";

class HomePage extends Component {
 constructor(props) {
  super(props);

  this.title = null;
  this.moveTitle = null;
  this.plane = null;
  this.movePlane = null;
  this.tieImg = null;
  this.moveTue = null;
  this.boatImg = null;
  this.moveBoat = null;
  this.mineDiv = null;
  this.moveMine = null;
  this.v2boatDiv = null;
  this.moveV2Boat = null;
  this.apacheDiv = null;
  this.moveApacheDiv = null;
  this.moveApacheDiv2 = null;
  this.oldLadyImg = null;
  this.moveOldLadyImg = null;
  this.hummerImg = null;
  this.moveHummerImg = null;
  this.moveCoupleImg = null;
  this.coupleImg = null;

  //img loaded
  // super(props);
  // this.state = { loaded: false };
  // this.handleImageLoaded = this.handleImageLoaded.bind(this);
  // this.image = React.createRef();
 }

 componentDidMount() {
  //make sure we load at top of page
  window.scrollTo(0, 0);

  // homepage greensock stuff
  this.moveTitle = new TimelineLite({ paused: false })
   //.to(this.title, 2, { x: 500 })
   .from(this.title, 1, {
    xPercent: 0,
    left: "0%",
    yPercent: 65,
    top: "65%",
    force3D: true,
    scaleX: 0.1,
    transformOrigin: "50% 25%",
    ease: "Bounce.easeOut",
    y: 0,
    delay: 2,
    opacity: 0,
   });
  this.movePlane = new TimelineLite({ paused: false }).to(this.plane, 24, {
   xPercent: -170,
   left: "170%",
   yPercent: -1,
   top: "1%",
   force3D: true,
   repeat: -1,
   repeatDelay: 3,
  });
  this.moveTie = new TimelineLite({ paused: false }).to(this.tieImg, 1, {
   yPercent: 63,
   top: "63%",
   force3D: false,
   ease: "Bounce.easeOut",
   repeat: -1,
   repeatDelay: 2,
  });
  this.moveBoat = new TimelineLite({ paused: false }).from(this.boatImg, 60, {
   xPercent: 870,
   left: "870%",
   yPercent: 1,
   top: "1%",
   force3D: true,
   repeat: -1,
   repeatDelay: 2,
  });
  this.moveMine = new TimelineLite({ paused: false }).to(this.mineDiv, 120, {
   xPercent: -170,
   left: "170%",
   yPercent: 4,
   top: "4%",
   force3D: true,
   repeat: -1,
   repeatDelay: 2,
  });
  this.moveV2Boat = new TimelineLite({ paused: false }).from(
   this.v2boatDiv,
   60,
   {
    xPercent: -170,
    left: "170%",
    yPercent: 1,
    top: "1%",
    force3D: true,
    repeat: -1,
    repeatDelay: 66,
   }
  );
  this.moveApacheDiv = new TimelineLite({ paused: false }).from(
   this.apacheDiv,
   6,
   {
    xPercent: -170,
    left: "170%",
    yPercent: -1,
    top: "1%",
    force3D: true,
    scale: 1.6,
    ease: "Back.easeOut.config(1.7)",
    delay: 11,
   }
  );
  this.moveApacheDiv2 = new TimelineLite({ paused: false }).to(
   this.apacheDiv,
   16,
   {
    xPercent: -40,
    left: "-40%",
    yPercent: -1,
    top: "1%",
    force3D: true,
    scale: 1.6,
    ease: "Bounce.easeOut",
    delay: 35,
   }
  );
  this.moveOldLadyImg = new TimelineLite({ paused: false }).to(
   this.oldLadyImg,
   424,
   { xPercent: -170, left: "170%", repeatDelay: 33 }
  );
  this.moveHummerImg = new TimelineLite({ paused: false }).to(
   this.hummerImg,
   8,
   {
    xPercent: -170,
    left: "170%",
    yPercent: -1,
    top: "1%",
    force3D: true,
    repeat: -1,
    repeatDelay: 6,
   }
  );
  this.moveCoupleImg = new TimelineLite({ paused: false }).from(
   this.coupleImg,
   100,
   { xPercent: 120, left: "120%", repeat: -1, repeatDelay: 1 }
  );

  function hideBordersOnArt() {
   // change css to display header and footer well
   const headerBorderNone = document.getElementById("header");
   headerBorderNone.style.borderBottom = "0px";
   headerBorderNone.style.position = "absolute";
   const FooterPlacement = document.getElementById("footer");
   FooterPlacement.style.marginTop = "143%";
  }
  hideBordersOnArt();

  //img loaded
  // const img = this.image.current;
  // if (img && img.complete) {
  //    this.handleImageLoaded();
  // }
 }

 componentWillUnmount() {
  function AddBordersOnArtClose() {
   // change css to display header and footer well
   // change css back to display header and footer for internals
   const headerBorderNone = document.getElementById("header");
   headerBorderNone.style.borderBottom = "1px solid #000000";
   headerBorderNone.style.position = "relative";
   const FooterPlacement = document.getElementById("footer");
   FooterPlacement.style.marginTop = "0%";
  }
  AddBordersOnArtClose();
 }

 // handleImageLoaded() {
 //    if (!this.state.loaded) {
 //       console.log('image loaded');
 //       this.setState({ loaded: true });
 //    }
 // }

 render() {
  return (
   <main className="HomePage-content">
    <Helmet>
     <meta charSet="utf-8" />
     <title>
      Web animation by Archibald Butler, a specialist based in London.
     </title>
     <meta
      name="description"
      content="I am a web animation specialist based in London. I also have web projects for download too! Have a browse and let me know what you think!"
     />
    </Helmet>
    <Route />
    <div id="navi-arrows">
     <Link className="navi-button" to="/animate-css-stories/">
      <img
       id="arrow-up"
       className="img-fluid"
       alt="Archibald Butler: web design, surfing and travel blog"
       src={ArrowUp}
       ref={this.image}
      />
     </Link>
     <Link className="navi-button2" to="/earthgang-an-html-art-project/">
      <img
       id="arrow-down"
       className="img-fluid"
       alt="Archibald Butler: web design, surfing and travel blog"
       src={ArrowDown}
      />
     </Link>
    </div>
    <picture id="background">
     <img
      alt="Archibald Butler: web design, surfing and travel blog"
      src={backgroundWEBP}
     />
     <img
      alt="Archibald Butler: web design, surfing and travel blog"
      src={backgroundJPG}
     />
    </picture>
    <img
     id="whiteboard-illustration"
     src={lightCanvasImg}
     alt="Billboard illustration"
    />
    <img id="react-logo" src={ReactLogoImg} alt="React JS Logo" />
    <img id="now-built-in" src={NowBuiltInImg} alt="Now built in" />
    <img id="react-js" src={ReactJsImg} alt="React Dot JS" />
    <div id="plane" ref={(div) => (this.plane = div)}>
     <Link to="/Contact">
      <picture id="planeimg">
       <source
        srcSet={planeImgWEBP}
        alt="Archibald Butler's bespoke hand drawn web animation aeroplane"
       />
       <img
        src={planeImgPNG}
        alt="Archibald Butler's bespoke hand drawn web animation aeroplane"
       />
      </picture>
     </Link>
     <img
      id="trail"
      src={planeTrail}
      alt="Archibald Butler's bespoke hand drawn web animation aeroplane made in web animation"
     />
    </div>
    <div className="title" id="title">
     <button
      href="#"
      data-toggle="modal"
      data-target="#intro-text-1"
      className="invisible-btn"
     >
      <img
       ref={(img) => (this.title = img)}
       id="titleimg"
       src={titleImg}
       alt="Archibald Butler, a roaming alchemist of web dev and illustration.png, made in web animation"
      />
     </button>
    </div>
    <div id="sun">
     <img
      id="sunimg"
      src={sunImg}
      alt="Archibald Butler's bespoke hand drawn web animation of the sun made in web animation"
     />
     <img
      id="tie"
      ref={(img) => (this.tieImg = img)}
      src={sunTie}
      alt="Archibald Butler's bespoke hand drawn web animation of a tie"
     />
    </div>
    <div id="boat">
     <img
      id="boatimg"
      ref={(img) => (this.boatImg = img)}
      src={boatImg}
      alt="Archibald Butler's bespoke hand drawn web animation of a green boat"
     />
    </div>
    <div id="mine" ref={(Div) => (this.mineDiv = Div)}>
     <img
      id="mineimg"
      src={mineImg}
      alt="Archibald Butler's bespoke hand drawn web animation of an ocean mine"
     />
    </div>
    <div id="v2boat" ref={(Div) => (this.v2boatDiv = Div)}>
     <img
      id="boatimg2"
      src={boatImg2}
      alt="Archibald Butler's bespoke hand drawn web animation of a red boat looking for waves"
     />
    </div>
    <img
     id="cliff"
     src={cliffImg}
     alt="Archibald Butler's bespoke hand drawn webillustration of a cliff office"
    />

    <img
     id="gateimg"
     src={gateImg}
     alt="Archibald Butler's bespoke hand drawn web drawing of a magic gate"
    />

    <div id="apache" ref={(Div) => (this.apacheDiv = Div)}>
     <img
      id="apacheimg"
      src={apacheImg}
      alt="Archibald Butler's bespoke hand drawn web animation of an Apache helicopter flying"
     />
     <img
      id="apacheanim"
      src={apacheAnim}
      alt="Archibald Butler's bespoke hand drawn web animation of an Apache helicopter flying"
     />
    </div>
    <div id="oldlady" ref={(div) => (this.oldLadyImg = div)}>
     <img
      id="oldladyimg"
      src={oldLady}
      alt="Archibald Butler's bespoke hand drawn web animation of an old lady with a zimo frame"
     />
    </div>
    <div id="couple" ref={(div) => (this.coupleImg = div)}>
     <img
      id="coupleimg"
      src={coupleImg}
      alt="Archibald Butler's bespoke hand drawn web animation of an old lady with a zimo frame"
     />
    </div>
    <div id="hummer" ref={(div) => (this.hummerImg = div)}>
     <img
      id="hummerimg"
      src={hummerImg}
      alt="Archibald Butler's bespoke hand drawn web animation hummer"
     />
     <img
      id="humtrail"
      src={humtrailImg}
      alt="Archibald Butler's bespoke hand drawn web animation of a hummer driving"
     />
    </div>
    <Link to="/archies-web/">
     <img
      id="archiesweb"
      src={archieswebImg}
      alt="Archibald Butler's feature in advanced photoshop magazine"
     />
    </Link>
    <Link to="/sarah-beeny/">
     <img
      id="beeny"
      src={beenyImg}
      alt="Archibald Butler's design for Sarah Beeny"
     />
    </Link>
    <Link to="/emma-ridley/">
     <img
      id="goddess"
      src={goddessImg}
      alt="Archibald Butler's design for Emma Ridley and Goddess fitness dance"
     />
    </Link>
    <Link to="/freddie-2-get-fit/">
     <img
      id="freddie"
      src={freddieImg}
      alt="Archibald Butler's design for Freddie to get fit"
     />
    </Link>
    <Link to="/asos-jobs/">
     <img
      id="asos"
      src={asosImg}
      alt="Archibald Butler's design work for ASOS"
     />
    </Link>
    <Link to="/edwinsford/">
     <img
      id="edwin"
      src={edwinsfordImg}
      alt="Archibald Butler's design work for Edwinsford"
     />
    </Link>
    <Link to="/web-utopia/">
     <img id="utopia" src={utopiaImg} alt="Archibald Butler's design work" />
    </Link>
    <Link to="/our-design-agency/">
     <img
      id="oda"
      src={odaImg}
      alt="Archibald Butler's design work for Our Design Agency"
     />
    </Link>
    <Link to="/roaming-giraffe/">
     <img
      id="roam"
      src={roamingImg}
      alt="Archibald Butler's design work for Roaming Giraffe"
     />
    </Link>
    <Link to="/news-4/">
     <img
      id="budd"
      src={buddImg}
      alt="Archibald Butler's buddhist fox meditation art"
     />
    </Link>
    <Link to="/one-mega-management/">
     <img
      id="onemega"
      src={oneImg}
      alt="Archibald Butler's design work for One Mega Collective"
     />
    </Link>
    <Link to="/the-pixel-lottery/">
     <img
      id="thepixellottery"
      src={thePixelLottery}
      alt="Archibald Butler's design work for The Pixel Lottery"
     />
    </Link>
    {/* Modal */}
    <HomePageModal />
   </main>
  );
 }
}

export default HomePage;
