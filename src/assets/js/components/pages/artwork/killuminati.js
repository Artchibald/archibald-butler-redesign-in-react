import React, { Component } from 'react';
import { TimelineLite } from "gsap/TweenMax";
import '../../../../css/g_killuminati.scss';
import { BrowserRouter as Route, Link } from "react-router-dom";
//import { BrowserRouter as Link } from "react-router-dom";

//images
import bg from '../../../../img/killuminati/bg.jpg';
import spiral from '../../../../img/killuminati/ring.png';
import pyramid from '../../../../img/killuminati/pyramid.png';
import hilaryimg from '../../../../img/killuminati/hilary.png';
import trump from '../../../../img/killuminati/trump.png';
import bottom from '../../../../img/killuminati/bottom.png';
import hilstringimg from '../../../../img/killuminati/hilaryring.png';
import trumpstringimg from '../../../../img/killuminati/trumpring.png';
import ArrowUp from '../../../../img/arrow-up.png';
import ArrowDown from '../../../../img/arrow-up.png';


class KilluminatiArt extends Component {
    //greensock
    constructor(props) {
        super(props);
        this.killspin = null;
        this.trumpy = null;
        this.trumpy2 = null;
        this.hilaryd = null;
        this.hilaryd2 = null;
        this.trumpstring = null;
        // this.trumpstring2 = null;
        // this.hilstring = null;
        // this.hilstring2 = null;
    }


    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        // change css to display header and footer well
        var headerBorderNone = document.getElementById('header');
        headerBorderNone.style.borderBottom = "0px";
        headerBorderNone.style.position = "absolute";
        var FooterPlacement = document.getElementById('footer');
        FooterPlacement.style.marginTop = "-2px";

        this.killspiral = new TimelineLite({ paused: false })
            .to(this.spiral, 30, { rotation: "+=100", repeat: -1, ease: 0 });

        this.trumpy = new TimelineLite({ paused: false })
            .to(this.trump, 3, { repeat: -1, repeatDelay: 6, xPercent: -13, left: "-13%", yoyo: true });

        this.trumpy2 = new TimelineLite({ paused: false })
            .to(this.trump, 3, { repeat: -1, delay: 3, repeatDelay: 6, xPercent: 5, left: "5%", yoyo: true });

        this.hilary = new TimelineLite({ paused: false })
            .to(this.hilaryimg, 6, { repeat: -1, repeatDelay: 12, xPercent: 22, left: "22%", yoyo: true });

        this.hilaryd = new TimelineLite({ paused: false })
            .to(this.hilaryimg, 6, { repeat: -1, delay: 6, repeatDelay: 12, xPercent: 0, left: "0%", yoyo: true });


        this.trumpstring = new TimelineLite({ paused: false })
            .to(this.trumpstringimg, 3, { repeat: -1, repeatDelay: 6, xPercent: -14, left: "-14%", yoyo: true });

        this.trustringed2 = new TimelineLite({ paused: false })
            .to(this.trumpstringimg, 3, { repeat: -1, delay: 3, repeatDelay: 6, xPercent: 6, left: "6%", yoyo: true });

        this.hilstringed = new TimelineLite({ paused: false })
            .to(this.hilstringimg, 6, { repeat: -1, repeatDelay: 12, xPercent: 16.7, left: "16.7%", yoyo: true });

        this.hilstringed2 = new TimelineLite({ paused: false })
            .to(this.hilstringimg, 6, { repeat: -1, delay: 6, repeatDelay: 12, xPercent: 0, left: "0%", yoyo: true });


    }
    componentWillUnmount() {
        // change css back to display header and footer for internals
        var headerBorderNone = document.getElementById('header');
        headerBorderNone.style.borderBottom = "1px solid #000000";
        headerBorderNone.style.position = "relative";
        var FooterPlacement = document.getElementById('footer');
        FooterPlacement.style.marginTop = "0%";
    }



    render() {
        return (
            <div className="killuminati-content Crop-App-to-bg">
                <img id="background" alt="Archibald Butler: web design, surfing and travel blog" src={bg} />
                <Route />
                <div id="navi-arrows">
                    <Link
                        className="navi-button"
                        to="/political-cartoon-animations-africas-diamonds/"
                    >
                        <img id="arrow-up" className="img-fluid" alt="Archibald Butler: web design, surfing and travel blog" src={ArrowUp} />
                    </Link>
                    <Link
                        className="navi-button2"
                        to="/study-right/"
                    >
                        <img id="arrow-down" className="img-fluid" alt="Archibald Butler: web design, surfing and travel blog" src={ArrowDown} />
                    </Link>
                </div>
                <div id="killspiral">
                    <img id="spiralimg" ref={img => this.spiral = img} src={spiral} alt="Archibald Butler, a roaming alchemist of web dev and illustration.png" />
                </div>
                <div id="pyramid">
                    <img id="pyramidimg"
                        src={pyramid}
                        alt="Archibald Butler's bespoke hand drawn web animation of a illuminati pyramid." />
                </div>
                <div id="hilary">
                    <img id="hilaryimg"
                        ref={img => this.hilaryimg = img}
                        src={hilaryimg}
                        alt="Archibald Butler's bespoke hand drawn web animation of Hilary Clinton" />
                </div>
                <div id="trump">
                    <img id="trumpimg"
                        ref={img => this.trump = img}
                        src={trump}
                        alt="Archibald Butler's bespoke hand drawn web animation of Donald Trump" />
                </div>
                <div id="bottom">
                    <img id="bottomimg"
                        src={bottom}
                        alt="Archibald Butler's bespoke hand drawn web animation of an apocalypse" />
                </div>
                <div id="hilstring">
                    <img id="hilstringimg"
                        ref={img => this.hilstringimg = img}
                        src={hilstringimg}
                        alt="Archibald Butler's bespoke hand drawn web animation of a control string" />
                </div>
                <div id="trumpstring">
                    <img id="trumpstringimg"
                        ref={img => this.trumpstringimg = img}
                        src={trumpstringimg}
                        alt="Archibald Butler's bespoke hand drawn web animation of a control string" />
                </div>

                {/*
                    Comments
                    */}

            </div>
        );
    }
}
export default KilluminatiArt;