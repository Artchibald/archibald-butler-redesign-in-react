import React, { Component } from 'react';
import { HashLink as Link } from 'react-router-hash-link';
//seo
import { Helmet } from "react-helmet";
//import MailchimpSubscribe from "react-mailchimp-subscribe";
import '../../../css/f_about.scss';

import Archie1 from '../../../img/internals/about/Archibald-Butler.jpg';
import Archie2 from '../../../img/internals/about/Archibald-Butler1.jpg';
import Archie3 from '../../../img/internals/about/Archibald-Butler2.jpg';
import Archie4 from '../../../img/internals/about/Archibald-Butler3.jpg';

class About extends Component {
	componentDidMount() {
		//make sure we load at top of page
		window.scrollTo(0, 0);
	}
	render() {
		return (
			<main className="About-content">
				<Helmet>
					<meta charSet="utf-8" />
					<title>Web animation specialist Archibald Butler explains his work...</title>
					<meta name="description" content="A willing to travel, London based freelance web animation specialist in front end web, illustration and animation tools such as Javascript and Css3" />
				</Helmet>
				<div className="container-fluid">
					<div className="row no-pad">
						<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<div className="textbox">
								<h1>About Me.</h1>
								<p>I am a London raised freelance web animation specialist and web developer with 10 years of experience focusing on my main passion: <a href="https://archibaldbutler.com/work/">Full width html5 animation</a>. If you need a free quote or my day rate, best to <a href="https://archibaldbutler.com/contact/">get in touch</a>.</p>
								<a aria-label="Pdf documents" href="https://archibaldbutler.com/projects/CV.pdf" className="btn btn-secondary">PDF CV</a>
								<a aria-label="Pdf documents" href="https://archibaldbutler.com/projects/Portfolio.pdf" className="btn btn-secondary">PDF Portfolio</a>
								{/*<a href="https://archibaldbutler.com/availability/" className="btn btn-secondary">Availability calendar</a>*/}
								<br /><br />
								<p>You can also check out my <Link to="#stats"><strong>stats below</strong></Link>. </p>

								<a aria-label="read more" href="#collapseExample" className="btn btn-secondary" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample">
									Read More
					</a>
								<div className="collapse" id="collapseExample">
									<div className="well">
										<p>Illustration has always been an obsession of mine and I have decided to go back to using more pen and paper. I would ideally like to get commissioned to build <a href="https://archibaldbutler.com/work/">animated websites</a> similar to:</p>
										<ul>
											<li><a aria-label="My artwork" href="/">My homepage</a></li>
											<li><a aria-label="My artwork" href="/killuminati-first-political-cartoon/">Killuminati</a></li>
											<li><a aria-label="My artwork" href="/political-cartoon-animations-africas-diamonds/">African Diamonds</a></li>
											<li><a aria-label="My artwork" href="/study-right/">The naughty classroom</a></li>
										</ul>
										<p>My favorite websites are <a aria-label="My artwork" href="http://www.graffitigeneral.com/">Graffiti General</a> and the <a href="http://www.abramsbooks.com/wesandersoncollection/">Wes Anderson collection</a>. I am inspired to research and develop more animation styles compatible with most web browsers. As you can see in the list above, I have created four of these so far. I am aiming to create ten so they
						are part of a set about my freelance web animation specialist skills. Animation and illustration are my main passion, coding is a tool I have learnt over 10 years to support my main interest.</p>
										<h2>About web animation Specialist, Archibald Butler.</h2>
										<p>I like to be in a team of front end web developers and build cool stuff.</p>
										<br />
									</div>
								</div>
							</div>
						</div>
						<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<img alt="Web animation specialist: Archibald Butler" src={Archie1} />
						</div>
					</div>
					<div className="row no-pad">
						<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<img alt="Web animation specialist: A graphic of the illustrator Archibald Butler" src={Archie2} />
						</div>
						<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<div className="textbox">
								<h1>Skillset</h1>
								<br />
								<div className="chartwrap">
									<div id="stats"></div>
									<div id="html">HTML5 – 90%
					</div>
									<div id="css">CSS3 – 90%
					</div>
									<div id="psd">Adobe Photoshop – 90%
					</div>
									<div id="illu">Adobe Illustrator – 70%
					</div>
									<div id="javascript">Javascript – 90%
					</div>
									<div id="php">PHP – 80%
					</div>
								</div>
							</div>
						</div>
					</div>
					<div className="row no-pad">
						<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<div className="textbox">
								<h1>Skills Continued</h1>
								<br />
								<div className="chartwrap">
									<div id="sql">My SQL – 80%
					</div>
									<div id="cpan">CPanel – 80%
					</div>
									<div id="greensock">Greensock Animation – 80%
					</div>
									<div id="sass">SASS / LESS – 80%
					</div>
									<div id="jquery">React JS – 90%
					</div>
									<div id="seo">SEO Optimization – 80%
					</div>
								</div>
							</div>
						</div>
						<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<img alt="Web animation specialist: A web design illustration by Archibald Butler" src={Archie3} />
						</div>
					</div>
					<div className="row no-pad">
						<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<img alt="Web animation specialist: A psd design for a website by Archibald Butler" src={Archie4} />
						</div>
						<div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
							<div className="textbox">
								<h1>Skills Last</h1>
								<br />
								<div className="chartwrap">
									<div id="wordpress">AWS  – 80%
					</div>
									<div id="ux">UX Design – 90%
					</div>
									<div id="ui">UI Design – 90%
					</div>
									<div id="animation">HTML5  Responsive Web Animation – 90%
					</div>
									<div id="illustration"> Illustration – 60%
					</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		);
	}
}
export default About;