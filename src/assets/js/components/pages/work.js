import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
//seo
import { Helmet } from "react-helmet";
import "../../../css/h_thumbs.scss";


import Scubapoint from "../../../img/video/scubapoint.mp4";
// import ripple from "../../../img/video/ripple_2.mp4";
// import yat from "../../../img/video/yat.mp4";
// import illustrator from "../../../img/video/typescript_illustrator.mp4";
import golfJunkiesVid from "../../../img/video/subliminal_square.mp4";
import screwFixVid from "../../../img/video/screwfix.mp4";

import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";
import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";

class Work extends Component {
 componentDidMount() {
  //make sure we load at top of page
  window.scrollTo(0, 0);
 }
 render() {
  return (
   <div className="container-fluid work">
    <Helmet>
     <meta charSet="utf-8" />
     <title>
      Web animation work by Archibald Butler, a specialist based in London.
     </title>
     <meta
      name="description"
      content="Checkout my work! I also have web projects for download too! Have a browse and let me know what you think!"
     />
    </Helmet>
    <Route />
    <div className="row no-pad">
     {/* <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <video autoPlay muted loop>
        <source src={illustrator} type="video/mp4" />
       </video>
       <figcaption>
        <h2>
         Typescript for <span>Illustrator</span>
        </h2>
        <p>Scripting for Adobe Illustrator to automate exports</p>
        <Link to="/illustrator/">View more</Link>
       </figcaption>
      </figure>
     </div> */}
     {/* <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <video autoPlay muted loop>
        <source src={yat} type="video/mp4" />
       </video>
       <figcaption>
        <h2>
         Y<span>.AT</span>
        </h2>
        <p>An emoji dream for crypto enthusiasts.</p>
        <Link to="/yat/">View more</Link>
       </figcaption>
      </figure>
     </div> */}
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       {/*// width="750" height="750"   */}
       <video autoPlay muted loop>
        <source src={screwFixVid} type="video/mp4" />
       </video>
       <figcaption>
        <h2>
         Banner <span>Hell!</span>
        </h2>
        <p>Check out some banners I made!</p>
        <Link to="/banners/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       {/*// width="750" height="750"   */}
       <video autoPlay muted loop>
        <source src={golfJunkiesVid} type="video/mp4" />
       </video>
       <figcaption>
        <h2>
         Golf <span>Junkies</span>
        </h2>
        <p>
         Partnered up with a close friend to prepare a Golf Membership NFT!
        </p>
        <Link to="/golfjunkies/">View more</Link>
       </figcaption>
      </figure>
     </div>
     {/* <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <video autoPlay muted loop>
        <source src={ripple} type="video/mp4" />
       </video>
       <figcaption>
        <h2>
         Ripping for <span>Ripple</span>
        </h2>
        <p>Another fine jewel to the viking crown!</p>
        <Link to="/ripple/">View more</Link>
       </figcaption>
      </figure>
     </div> */}
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <video autoPlay muted loop>
        <source src={Scubapoint} type="video/mp4" />
       </video>
       <figcaption>
        <h2>
         More under water <span>art!</span>
        </h2>
        <p>Got creative reign for scubapoint.com</p>
        <Link to="/scubapoint/">View more</Link>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-11/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-1/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default Work;
