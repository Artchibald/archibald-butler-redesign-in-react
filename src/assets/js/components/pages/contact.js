import React from 'react';
//seo
import { Helmet } from 'react-helmet';
import SimpleMap from '../../../js/scripts/googlemaps.js';
import '../../../css/t_contact.scss';
import contactStamp from '../../../img/contact.png';

const Contact = (props) => {
  return (
    <main className="contact-content">
      {' '}
      <Helmet>
        <meta charSet="utf-8" />
        <title>
          Contact | Archibald Butler | A digital illustrator, web animator, and
          front end developer.
        </title>
        <meta
          name="description"
          content="Please get in touch, I will be happy to answer any questions."
        />
      </Helmet>
      <div className="container-fluid">
        <div className="row no-pad">
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Archibald Butler</h1>
              <p>
                +44(0)7 873 254 312
                <br />
                <br />
                Address:
                <br />
                Suite 302, 43 Bedford street.
                <br />
                Covent Garden.
                <br />
                WC2E 9HA
                <br />
                United Kingdom
                <br />
                <br />
                <a
                  aria-label="email me"
                  href="mailto:archie@archibaldbutler.com"
                  className="btn btn-secondary"
                >
                  Email
                </a>
              </p>
              <img
                src={contactStamp}
                className="floatright"
                alt="Become One Marketing interviewed Archie Butler!"
              />
            </div>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <SimpleMap></SimpleMap>
          </div>
        </div>
      </div>
    </main>
  );
};
export default Contact;
