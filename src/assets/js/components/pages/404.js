import React from 'react';
//seo
import { Helmet } from 'react-helmet';
import '../../../css/t_contact.scss';
import contactStamp from '../../../img/contact.png';

const Contact = (props) => {
 return (
  <main className="contact-content">
   {' '}
   <Helmet>
    <meta charSet="utf-8" />
    <title>
     Contact | Archibald Butler | A digital illustrator, web animator, and
     front end developer.
    </title>
    <meta
     name="description"
     content="Please get in touch, I will be happy to answer any questions."
    />
   </Helmet>
   <div className="container-fluid">
    <div className="row no-pad">
     <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div className="textbox">
       <h1 className='display-1 text-center'>404</h1>
       <p className='text-center'>These are not the pages you are looking for.</p>
       <img
        src={contactStamp}
        className="floatright"
        alt="Become One Marketing interviewed Archie Butler!"
       />
      </div>
     </div>
    </div>
   </div>
  </main>
 );
};
export default Contact;
