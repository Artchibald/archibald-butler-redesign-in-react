import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import "../../../css/h_thumbs.scss";

import News4Img1 from "../../../img/internals/news-4/news-4-img-1.jpg";
import News4Img2 from "../../../img/internals/news-4/news-4-img-2.jpg";
import AfricanDiamond from "../../../img/internals/work/animated-gif-moving-illustration-animation-thumbnail.gif";
import tplImg from "../../../img/internals/work/the-pixel-lottery-hero.jpg";
import kinesisImg from "../../../img/internals/work/kinesis-money.jpg";
import techCircus from "../../../img/internals/work/techcircus-Archibald-Butler-Web-Development.jpg";

import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News3 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="illustration of World poverty" src={AfricanDiamond} />
       <figcaption>
        <h2>
         Inequality… <span>Explained!</span>
        </h2>
        <p>How the West is held up by the East…</p>

        <Link to="/diamonds/">See it</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="An illustration of a surfers work dilemma" src={News4Img1} />
       <figcaption>
        <h2>
         Coding at <span>New Look</span>
        </h2>
        <p>An interactive landing page for the New Milk Models </p>
        <Link to="/model-squad/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="Geek Award" src={News4Img2} />
       <figcaption>
        <h2>
         Stella <span>Mc Cartney</span>
        </h2>
        <p>Let it snow! Some GIF snow demos done in house in 2013.</p>
        <a href="https://archibaldbutler.com/projects/stella-mc-cartney/index.html">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An animated website design by Archibald Butler"
        src={kinesisImg}
       />
       <figcaption>
        <h2>
         Working in <span>Crypto</span>
        </h2>
        <p>A full ICO website revamp I coded solo!</p>

        <Link to="/kinesis-money-a-crypto-currency-ico-website/">See it</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="A UX event website by Archibald Butler" src={techCircus} />
       <figcaption>
        <h2>
         UX <span>Development</span>
        </h2>
        <p>A User experience event website</p>
        <Link to="/techcircus-io/">See it</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A Gambling website designed by Archibald Butler"
        src={tplImg}
       />
       <figcaption>
        <h2>
         My Exciting <span>Pixel Lottery</span>
        </h2>
        <p>An online lottery project!</p>
        <a aria-label="See work" href="/the-pixel-lottery/">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-2/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-4/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News3;
