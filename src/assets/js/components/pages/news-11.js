import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import "../../../css/h_thumbs.scss";

import News4Img17 from "../../../img/internals/news-4/news-4-img-17.jpg";
import News4Img18 from "../../../img/internals/news-4/news-4-img-18.jpg";
import News2Img4 from "../../../img/internals/news-2/news-2-img-4.jpg";
// import howToBe from "../../../img/internals/how-to-be/how-to-be1.jpg";
import News4Img12 from "../../../img/internals/news-4/news-4-img-12.png";
import thePixelLotteryImg from "../../../img/internals/news/7-the-pixel-lottery-website-project.jpg";
import skateboardImg from "../../../img/internals/news/8-skateboard-design-spray-paint-posca-pens.jpg";
import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News11 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="Freddie to get fit a wordpress template designed and developed by Archibald Butler"
        src={News4Img17}
       />
       <figcaption>
        <h2>
         Messing with <span>wood textures from above</span>
        </h2>
        <p>A design I created which got a little attention back in the day!</p>
        <Link className="btn btn-primary" to="/archies-web/">
         View more
        </Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An illustration of a family on the beach with dogs in california"
        src={News4Img18}
       />
       <figcaption>
        <h2>
         Download a free PSD: Corporate <span>Slick</span>
        </h2>
        <p>A bit of spare time fun.</p>
        <Link className="btn btn-primary" to="/corporate-psd/">
         View more
        </Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A website illustration for production that never completed. An illustration by Archibald Butler"
        src={thePixelLotteryImg}
       />
       <figcaption>
        <h2>
         My Exciting <span>Pixel Lottery</span>
        </h2>
        <p>A very exciting concept I pushed to LIVE!</p>
        <Link to="/the-pixel-lottery/">See it</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A Gambling website designed by Archibald Butler"
        src={skateboardImg}
       />
       <figcaption>
        <h2>
         Deck <span>Design</span>
        </h2>
        <p>I ordered a skate deck from Ebay and illustrated it</p>
        <Link to="/skateboard-spray-design-archies-world/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An illustration of a family on the beach with dogs in california"
        src={News4Img12}
       />
       <figcaption>
        <h2>
         An interactive <span>Illustration</span> from 2005
        </h2>
        <p>
         When I was still at Uni I produced this. One of my first pieces of
         interaction design.
        </p>
        <Link to="/interactive-map/">See</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A bespoke web dev job on Tumblr for ASOS by Archibald Butler"
        src={News2Img4}
       />
       <figcaption>
        <h2>
         New Look <span>Extended sizes</span>
        </h2>
        <p>A cool help page for finding your size at New Look</p>
        <a href="https://www.archibaldbutler.com/projects/new-look-extended-sizes/index.html">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     {/* <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A business website design for Roaming Giraffe by illustrator Archibald Butler"
        src={howToBe}
       />
       <figcaption>
        <h2>
         Become a <span>freelancer</span>
        </h2>
        <p>A course I was thiking about doing in France</p>
        <Link to="/how-to-be/">View more</Link>
       </figcaption>
      </figure>
     </div> */}
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-10/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/work/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News11;
