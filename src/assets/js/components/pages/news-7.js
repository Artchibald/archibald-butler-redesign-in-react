import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import "../../../css/h_thumbs.scss";

import News3Img2 from "../../../img/internals/news-3/news-3-img-2.jpg";
import News3Img3 from "../../../img/internals/news-3/news-3-img-3.gif";
import News3Img4 from "../../../img/internals/news-3/news-3-img-4.jpg";
import News3Img5 from "../../../img/internals/news-3/news-3-img-5.jpg";
import News3Img6 from "../../../img/internals/news-3/news-3-img-6.jpg";
import News3Img1 from "../../../img/internals/news-3/news-3-img-1.jpg";

import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News7 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="An illustration of a surfers work dilemma" src={News3Img1} />
       <figcaption>
        <h2>
         A Javascript <span>Kaleidoscope</span>
        </h2>
        <p>
         I have been enjoying messing around in javascript to create this from a
         div background image.
        </p>
        <Link to="/kaleidoscope/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="Geek Award" src={News3Img2} />
       <figcaption>
        <h2>
         Best use of mobile: <span>My team at ASOS</span>
        </h2>
        <p>
         Seven or eight developers including me were very pleased to win an
         award for innovative use of CMS for mobile.
        </p>
        <Link to="/geek-award/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A website development into bespoke WordPress by Archibald Butler"
        src={News3Img3}
       />
       <figcaption>
        <h2>
         Animated <span>Gifs</span>
        </h2>
        <p>Do they work in web design?</p>
        <Link to="/animated-gif/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A bespoke web dev job on Tumblr for ASOS by Archibald Butler"
        src={News3Img4}
       />
       <figcaption>
        <h2>
         Green <span>Yurts</span>
        </h2>
        <p>
         A Bootstrap css custom WordPress theme I coded from the ground up.
        </p>
        <Link to="/green-yurts/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="RELLIK Fashion" src={News3Img5} />
       <figcaption>
        <h2>
         London Fashion: <span>RELLIK</span>
        </h2>
        <p>A full blown solo des/dev project for a famous Fashion boutique.</p>
        <Link to="/rellik/">See the art</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A business website design for Roaming Giraffe by illustrator Archibald Butler"
        src={News3Img6}
       />
       <figcaption>
        <h2>
         How to be a freelance <span>web designer</span>
        </h2>
        <p>A course I will be doing soon hopefully.</p>
        <Link to="/how-to-be/">View more</Link>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-6/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-8/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News7;
