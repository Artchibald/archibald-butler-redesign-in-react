import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import "../../../css/h_thumbs.scss";

import News4Img5 from "../../../img/internals/news-4/news-4-img-5.jpg";
// import News4Img6 from '../../../img/internals/news-4/news-4-img-6.jpg';
import News4Img7 from "../../../img/internals/news-4/news-4-img-7.jpg";
import News4Img8 from "../../../img/internals/news-4/news-4-img-8.jpg";
import News4Img9 from "../../../img/internals/news-4/news-4-img-9.jpg";
import News4Img10 from "../../../img/internals/news-4/news-4-img-10.jpg";
import News4Img4 from "../../../img/internals/news-4/news-4-img-4.jpg";

import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News9 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A bespoke web dev job on Tumblr for ASOS by Archibald Butler"
        src={News4Img4}
       />
       <figcaption>
        <h2>
         HM <span>Locksmiths</span>
        </h2>
        <p>A full blown des/dev project I undertook solo.</p>
        <Link to="/hm-locksmiths/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="RELLIK Fashion" src={News4Img5} />
       <figcaption>
        <h2>
         Download a free <span>business card mockup!</span>
        </h2>
        <p>These things are Photoshop awesomeness!</p>
        <Link to="/business-cards/">See the art</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A fishing lake website design illustration by Archibald Butler"
        src={News4Img7}
       />
       <figcaption>
        <h2>
         Hotel <span>Les Terrasses</span>
        </h2>
        <p>A mag style site I created alone.</p>
        <a
         target="_blank"
         rel="noopener noreferrer"
         href="https://hotel-des-terrasses.com/"
        >
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="Freddie to get fit a wordpress template designed and developed by Archibald Butler"
        src={News4Img8}
       />
       <figcaption>
        <h2>
         Les Vignobles <span>Bessineau</span>
        </h2>
        <p>A wine chateaux in France. I did their website alone.</p>
        <a
         target="_blank"
         rel="noopener noreferrer"
         className="btn btn-primary"
         href="http://www.cote-montpezat.com/"
        >
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="An illustration of a surfers work dilemma" src={News4Img9} />
       <figcaption>
        <h2>
         Multi<span>bikes</span>
        </h2>
        <p>A mag style site I did alone in France.</p>
        <a
         target="_blank"
         rel="noopener noreferrer"
         href="http://multibikes.fr/"
        >
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="Geek Award" src={News4Img10} />
       <figcaption>
        <h2>
         An old <span>Flash Design from 2004</span>
        </h2>
        <p>Something I did at Uni.</p>
        <a href="https://archibaldbutler.com/web-design-templates/flashintro/intro.html">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
    </div>

    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-8/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-10/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News9;
