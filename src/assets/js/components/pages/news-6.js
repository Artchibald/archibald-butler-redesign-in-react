import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import "../../../css/h_thumbs.scss";

import News2Img5 from "../../../img/internals/news-2/news-2-img-5.jpg";
import News2Img6 from "../../../img/internals/news-2/news-2-img-6.jpg";
import News2Img7 from "../../../img/internals/news-2/news-2-img-7.jpg";
import News2Img8 from "../../../img/internals/news-2/news-2-img-8.jpg";
import News2Img9 from "../../../img/internals/news-2/news-2-img-9.jpg";
import oneMegaImg from "../../../img/internals/news/5-one-mega-management.jpg";

import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News6 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="illustration of World poverty" src={oneMegaImg} />
       <figcaption>
        <h2>
         <span>One</span> Mega Management
        </h2>
        <p>A full des/dev website project I worked on solo.</p>
        <Link to="/one-mega-management/">See it</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An illustration of a monk by Archibald Butler"
        src={News2Img5}
       />
       <figcaption>
        <h2>
         3 hours <span>In Photoshop</span>
        </h2>
        <p>
         When I got my new Macbook, I delved deep into its processing speeds
         with Photoshop. I was very impressed.
        </p>
        <Link to="/cosmic-planet/">See the art</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A business website design for Roaming Giraffe by illustrator Archibald Butler"
        src={News2Img6}
       />
       <figcaption>
        <h2>
         A full <span>Des/Dev website project</span>
        </h2>
        <p>
         WordPress, SEO, and a lot more for a catering business, sole developer.
        </p>
        <Link to="/roaming-giraffe/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A fishing lake website design illustration by Archibald Butler"
        src={News2Img7}
       />
       <figcaption>
        <h2>
         My Animated <span>Fish from 2012</span>
        </h2>
        <p>
         This is when I started seriously messing around with JS animation
         before Greensock came out.
        </p>
        <a href="/edwinsford/">View more</a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="Freddie to get fit a wordpress template designed and developed by Archibald Butler"
        src={News2Img8}
       />
       <figcaption>
        <h2>
         A full des/dev website for a <span>fitness instructor</span>
        </h2>
        <p>
         I had fun with the motivational style designs on this one. I worked
         alone on this project.
        </p>
        <Link to="/freddie-2-get-fit/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An illustration of a family on the beach with dogs in california"
        src={News2Img9}
       />
       <figcaption>
        <h2>
         Timed <span>Sketch</span>
        </h2>
        <p>2 hours tops from blank paper to frame.</p>
        <Link to="/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction/">
         See
        </Link>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-5/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-7/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News6;
