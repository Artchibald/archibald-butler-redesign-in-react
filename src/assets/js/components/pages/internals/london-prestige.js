import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';
//icons
import BootstrapIcon from '../../icons/bootstrap-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import NativeIcon from '../../icons/native-icon.js';

// img
import LondonPrestigeImg1 from '../../../../img/internals/london-prestige/london-prestige1.jpg';
import LondonPrestigeImg2 from '../../../../img/internals/london-prestige/london-prestige2.jpg';
import LondonPrestigeImg3 from '../../../../img/internals/london-prestige/london-prestige3.jpg';

class LondonPrestige extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }



    render() {
        return (
            <div className="container-fluid portfolio">
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">The London Prestige Awards winning category: Web developer of the year.</h1>
                            <h3 className="projHeaderP">A great kick off to the decade.</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <BootstrapIcon />
                                <ReactiveIcon />
                                <NativeIcon />                            </div>
                            <div className="clearfix">

                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={LondonPrestigeImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>ALWAYS A PLEASURE TO SWIPE AN AWARD!</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">I had the absolute pleasure of meeting the awesome team at The London Prestige Awards & Corporate Live Wire.
                    </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        They contacted me during nomination with a small email questionnaire.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        One month later I was informed I had one first place for web developer of the year!
                        </p>

                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">

                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <BootstrapIcon />
                                    <ReactiveIcon />
                                    <NativeIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid mt-5" alt="Kinesis Money: Archibald Butler Web Development" src={LondonPrestigeImg2} />
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the event</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">About the London Prestige Awards Event
                    </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Dress to impress!</h3>
                                        <p>
                                            Guests had really gone to fashion town, there were even tuxedos in the mix!
                        </p>

                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>I was greeted by the team.</h3>
                                        <p>
                                            They introduced themselves and walked me through to the main podium to receive my award.
                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">

                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={LondonPrestigeImg3} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Hard work pays off!</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            In my defence I am a web dev fanatic. It feels great  to get awarded though.
                        </p>

                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            It's been an awesome kick off to the year and I am grateful for the invite to their fantastic event.
                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LondonPrestige; 