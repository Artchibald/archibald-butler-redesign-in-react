import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";

import OurDesignAgencyImg1 from "../../../../img/internals/our-design-agency/our-design-agency1.jpg";
import OurDesignAgencyImg2 from "../../../../img/internals/our-design-agency/our-design-agency2.jpg";
import OurDesignAgencyImg3 from "../../../../img/internals/our-design-agency/our-design-agency3.jpg";
//icons
import HtmlIcon from "../../icons/html-icon.js";
import JqueryIcon from "../../icons/jquery-icon.js";
import WireframeIcon from "../../icons/wireframe-icon.js";
import WordpressIcon from "../../icons/css3-icon.js";
import BootstrapIcon from "../../icons/bootstrap-icon.js";
import ReactiveIcon from "../../icons/reactive-icon.js";
import NativeIcon from "../../icons/native-icon";
class OurDesignAgency extends Component {
 componentDidMount() {
  //make sure we load at top of page
  window.scrollTo(0, 0);
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid">
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-6 col-lg-6">
      <div className="textbox">
       <h1>Our Design Agency</h1>
       <p>
        A new London marketing agency set up by my dear friend Grant Willis. The
        design team presented me with a psd / ai which I transformed into a
        fully interactive website.
       </p>
       <br />
       <br />
       <h5 className="technoHeaderTitle">technologies used</h5>
       <div className="lineOnProject"></div>
       <div className="clearfix">
        <HtmlIcon />
        <WordpressIcon />
        <JqueryIcon />
        <WireframeIcon />
        <BootstrapIcon />
        <ReactiveIcon />
        <NativeIcon />
       </div>
       <div className="clearfix"></div>
       <a
        aria-label="See work"
        className="btn btn-secondary"
        href="http://our-design-agency.com/"
       >
        View the website
       </a>
      </div>
     </div>
     <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
      <figure className="effect-roxy">
       <img alt="Illustrator Archibald Butler" src={OurDesignAgencyImg1} />
       <figcaption>
        <h2>
         Our <span>Design Agency</span>
        </h2>
        <p> Web Design and development</p>
        <a aria-label="See work" href="http://our-design-agency.com/">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
      <figure className="effect-roxy">
       <img alt="Illustrator Archibald Butler" src={OurDesignAgencyImg2} />
       <figcaption>
        <h2>
         Our <span>Design Agency</span>
        </h2>
        <p> Web Design and development</p>
        <br />
        <a href="http://our-design-agency.com/">View the website</a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-6 col-lg-6">
      <div className="textbox">
       <h1>A lengthy build</h1>
       <p>
        The custom theme is using both Bootstrap media queries and Masonry JS.
        Therefore I had to declare a lot of widths and heights for the synergy
        to work.
       </p>
       <br />
      </div>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-6 col-lg-6">
      <div className="textbox">
       <h1>Conclusion</h1>
       <p>
        I am pretty sure they love it though I put it live remotely. Will check
        when I get back but they mentionned they want to expand the sections…
       </p>
      </div>
     </div>
     <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A web design illustration by Archibald Butler"
        src={OurDesignAgencyImg3}
       />
       <figcaption>
        <h2>
         Our <span>Design Agency</span>
        </h2>
        <p> Web Design and development</p>
        <br />
        <a href="http://our-design-agency.com/">View the website</a>
       </figcaption>
      </figure>
     </div>
    </div>
   </div>
  );
 }
}

export default OurDesignAgency;
