import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import HmLocksmithsImg1 from '../../../../img/internals/hm-locksmiths/hm-locksmiths1.jpg';
import JqueryIcon from '../../icons/jquery-icon.js';
import BitbucketIcon from '../../icons/bitbucket-icon.js';
import Css3Icon from '../../icons/css3-icon.js';
import GulpIcon from '../../icons/gulp-icon.js';
// import AngularIcon from '../../../../js/components/icons/angular-icon.js';
// import ApiIcon from '../../../../js/components/icons/api-icon.js';
// import BitbucketIcon from '../../../../js/components/icons/bitbucket-icon.js';
// import BootstrapIcon from '../../../../js/components/icons/bootstrap-icon.js';
// import CmsIcon from '../../../../js/components/icons/cms-icon.js';
// import CpanelIcon from '../../../../js/components/icons/cpanel-icon.js';
// import Css3Icon from '../../../../js/components/icons/css3-icon.js';
// import GifIcon from '../../../../js/components/icons/gif-icon.js';
// import GitIcon from '../../../../js/components/icons/git-icon.js';
// import GreensockIcon from '../../../../js/components/icons/greensock-icon.js';
// import GruntIcon from '../../../../js/components/icons/grunt-icon.js';
// import GulpIcon from '../../../../js/components/icons/gulp-icon.js';
// import HtmlIcon from '../../../../js/components/icons/html-icon.js';
// import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
// import LessIcon from '../../../../js/components/icons/less-icon.js';
// import NativeIcon from '../../../../js/components/icons/native-icon.js';
// import NodeIcon from '../../../../js/components/icons/node-icon.js';
// import PhpIcon from '../../../../js/components/icons/php-icon.js';
// import ReactiveIcon from '../../icons/reactive-icon.js';
// import SassIcon from '../../icons/sass-icon.js';
// import SeoIcon from '../../icons/seo-icon.js';
// import SqlIcon from '../../icons/sql-icon.js';
// import VideoIcon from '../../icons/video-icon.js';
// import WireframeIcon from '../../icons/wireframe-icon.js';
// import WordpressIcon from '../../icons/wordpress-icon.js';
class HmLocksmiths extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid portfolio">
                {/*
    <AngularIcon />
    <ApiIcon />
    <BitbucketIcon />
    <BootstrapIcon />
    <CmsIcon />
    <CpanelIcon />
    <Css3Icon />
    <GifIcon />
    <GitIcon />
    <GreensockIcon />
    <GruntIcon />
    <GulpIcon />
    <HtmlIcon />
    <JqueryIcon />
    <LessIcon />
    <NativeIcon />
    <NodeIcon />
    <PhpIcon />
    <ReactiveIcon />
    <SassIcon />
    <SeoIcon />
    <SqlIcon />
    <VideoIcon />
    <WireframeIcon />
    <WordpressIcon /> */}
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">Lost those keys?</h1>
                            <h3 className="projHeaderP">HM Locksmiths asked Archie to do their website.
                </h3>
                            <br />
                            <p>My old friend Jeremy wants a new design for his locksmith website. I built this .psd from scratch. I really liked the colors of his car: blue & yellow. I used the car as the basis for the homepage above the fold. I very much enjoyed building this free website template using Photoshop. I originally thought that the blue and yellow colours were very distinctive and would blend nicely together. I also wanted to incorporate some strong call-to-action functionality by using plenty of buttons that lead of to other relevant sections of the website. This free for download website template took me about 11 hours to draw up. I hope you like it and feel free to look at some of my other free website templates.
                </p>
                            <p>Websites like this should ideally be planned firstly on paper. They should then be added to the 996px wide recommended web grid, you can download some really nice ones with a quick search in case you are thinking of making your own templates instead of downloading my ones! If you do download this one of many free website templates, please recommended me using the Facebook like, Google + or tweet, all that seo jus will be much appreciated. Once again thank you so much for taking the time to look at my website, feel free to check the other pages to find out more about my work.</p>
                            <p>Below, you will find a few pictures helping you understand the “making of” this locksmith website template. Feel free to comment also using the Facebook plugin. I hope you have enjoyed reading this article. The image is their previous site.</p>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <JqueryIcon />
                                <Css3Icon />
                                <GulpIcon />
                                <BitbucketIcon />
                            </div>
                            <div className="clearfix">
                                <a className="btn btn-secondary readmoreBTN"
                                    href="http://hm-locksmiths.co.uk/">
                                    View the work
                    </a>
                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={HmLocksmithsImg1} />
                    </div>
                </div>

            </div>
        );
    }
}

export default HmLocksmiths;