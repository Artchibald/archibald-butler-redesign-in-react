import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import SmokeyImg2 from '../../../../img/internals/smokey/smokey2.jpg';
import SmokeyImg3 from '../../../../img/internals/smokey/smokey3.png';
import SmokeyImg4 from '../../../../img/internals/smokey/smokey4.jpg';


class Smokey extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Smokey: A Graffiti Project.</h1>
                            <p>There has been a large canvas hanging around taking up room in my flat. So I decided to do something with it.
                                <br /><br />
                                My housemate’s nickname is Smokee, and he wanted that in an old school graffiti style. Having not tried my hand at as manual tasks as of recently I decided to jump in at the deep end and build something quickly, as we all know how valuable time is nowadays, especially as a freelancer.
                                <br /><br />
                                Here is the speed up of the whole project:</p>

                            <br />

                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <iframe title="Archie painting" width="100%" height="515" src="https://www.youtube.com/embed/bljDzAIYjtM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler" src={SmokeyImg2} />
                            <figcaption>
                                <h2>Smokey, a graffiti project</h2>
                                <p>Design and idea creation</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">

                            <h1>About this art piece</h1>
                            <p>In Photoshop I created a series of shapes from dafont.com. Always making sure to check the creative licence even if it is just for personal work.</p>
                            <br />
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>#FAIL</h1>
                            <p>This needs more work, I consider it a fail. X(</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler" src={SmokeyImg3} />
                            <figcaption>
                                <h2>Smokey, a graffiti project</h2>
                                <p>Design and idea creation</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler" src={SmokeyImg4} />
                            <figcaption>
                                <h2>Smokey, a graffiti project</h2>
                                <p>Design and idea creation</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">

                            <h1>Writing on the wall</h1>
                            <p>Here it is hanging with a rather cheerful housemate!</p>
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Smokey;