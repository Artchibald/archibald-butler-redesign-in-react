import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
import csmImg120 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-120.jpg";
import csmImg121 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-121.jpg";
import csmImg122 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-122.jpg";
import csmImg123 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-123.jpg";
import csmImg124 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-124.jpg";
import csmImg125 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-125.jpg";
import csmImg126 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-126.jpg";
import csmImg127 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-127.jpg";
import csmImg128 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-128.jpg";
import csmImg129 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-129.jpg";
import csmImg130 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-130.jpg";
import csmImg131 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-131.jpg";
import csmImg132 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-132.jpg";
import csmImg133 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-133.jpg";
import csmImg134 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-134.jpg";
import csmImg135 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-135.jpg";
import csmImg136 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-136.jpg";
import csmImg137 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-137.jpg";
import csmImg138 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-138.jpg";
import csmImg139 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-139.jpg";
import csmImg140 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-140.jpg";
import csmImg141 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-141.jpg";
import csmImg142 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-142.jpg";
import csmImg143 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-143.jpg";
import csmImg144 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-144.jpg";
import csmImg145 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-145.jpg";
import csmImg146 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-146.jpg";
import csmImg147 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-147.jpg";
import csmImg148 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-148.jpg";
import csmImg149 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-149.jpg";
import csmImg150 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-150.jpg";

import chevronLeftImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class csmArtwork5 extends Component {
 componentDidMount() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid">
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-12 col-lg-12">
      <h1 className="text-center mt-5">May the horror continue!</h1>
      <p className="text-center mb-5">
       These drawings cost me a £25,000.00 student debt. Wow… Just wow.
      </p>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg120}
       />
       <figcaption>
        <h2>Oh look I drew some bones. Wow…</h2>
        <p>Someone’s been wandering around museums. ooouuhhh…</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-120.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg121}
       />
       <figcaption>
        <h2>Oh cercophages.</h2>
        <p>You must be so cultured, hipster art student.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-121.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg122}
       />
       <figcaption>
        <h2>A Pharao.</h2>
        <p>Looks about as old as my sadness.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-122.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg123}
       />
       <figcaption>
        <h2>Drawing Tombs and hieroglyphes</h2>
        <p>These drawings are so modern area. ZZZZZZZZZ.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-123.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg124}
       />
       <figcaption>
        <h2>OMG What is this horrible tiger rape b*ll*cks.</h2>
        <p>This should be banned from the internet.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-124.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg125}
       />
       <figcaption>
        <h2>“Wow you drew it while drinking it</h2>
        <p>That’s so 2008…”</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-125.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg126}
       />
       <figcaption>
        <h2>Illustrator I liked and copied</h2>
        <p>Wish I had drawn this from scratch</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-126.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg127}
       />
       <figcaption>
        <h2>I WILL KILL HIM!! I WILL KILL HIM!!</h2>
        <p>Sting in the movie Dune.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-127.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg128}
       />
       <figcaption>
        <h2>Drawing a friend</h2>
        <p>Oh wow you can draw photos. Get a life will you.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-128.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg129}
       />
       <figcaption>
        <h2>Oh you drew Superman.</h2>
        <p>You think this stank is good enough to work at Marvel?? P*ss off…</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-129.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg130}
       />
       <figcaption>
        <h2>Mogwai, drawn from photo.</h2>
        <p>Wow maybe you could work for National Geographic… NOT.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-130.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg131}
       />
       <figcaption>
        <h2>Ant and gun, refining lines.</h2>
        <p>So cool how your work combines great…. ZZZZ … ZZZZ</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-131.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg132}
       />
       <figcaption>
        <h2>Mirror in my room.</h2>
        <p>Mirror says : “Your drawings are the ugliest. Meh”.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-132.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg133}
       />
       <figcaption>
        <h2>Some real twisted s**t here</h2>
        <p>How I see my future self perhaps. Hehehe</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-133.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg134}
       />
       <figcaption>
        <h2>St Michael judging the demon</h2>
        <p>So deep, shame you copied it loser.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-134.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg135}
       />
       <figcaption>
        <h2>Random Mexican in my sketchbook</h2>
        <p>He looks drunk on margaritas…</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-135.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg136}
       />
       <figcaption>
        <h2>Weird imagination concepts</h2>
        <p>I can’t even be bothered to look at.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-136.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg137}
       />
       <figcaption>
        <h2>Old man is back!</h2>
        <p>Black and white this time.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-137.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg138}
       />
       <figcaption>
        <h2>Weird illustrations</h2>
        <p>Some things are best left unexplained.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-138.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg139}
       />
       <figcaption>
        <h2>Grotten child</h2>
        <p>Savagely brainwashed by the modern world. Standard.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-139.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg140}
       />
       <figcaption>
        <h2>Perfect me</h2>
        <p>A place only a disciplined Achie can attain…</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-140.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-12 col-md-12 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg141}
       />
       <figcaption>
        <h2>Djubai…</h2>
        <p>The strongest Samurai…. Meh.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-141.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg142}
       />
       <figcaption>
        <h2>A dream about buying your mum…</h2>
        <p>Before re-incarnating into your next life… Deep.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-142.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg143}
       />
       <figcaption>
        <h2>Drawing furniture near me</h2>
        <p>What a load of jizz jazz.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-143.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg144}
       />
       <figcaption>
        <h2>I gone and done it sir</h2>
        <p>Note behind his back.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-144.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg145}
       />
       <figcaption>
        <h2>Some twisted futuristic s**t</h2>
        <p>Random raggo visions.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-145.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg146}
       />
       <figcaption>
        <h2>Psycho….</h2>
        <p>Yes that’s definitely one way of putting it.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-146.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg147}
       />
       <figcaption>
        <h2>Wolfy</h2>
        <p>Part of a graphic novel thrown in here I haven’t assembled yet.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-147.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg148}
       />
       <figcaption>
        <h2>Graphic novel.</h2>
        <p>If you want teen anxiety, you are in the right place.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-148.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg149}
       />
       <figcaption>
        <h2>Graphic novel again.</h2>
        <p>A collection of 251 drawings for your enjoyment.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-149.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg150}
       />
       <figcaption>
        <h2>Graphic novel again.</h2>
        <p>Try and retrace the novel if you are bore:) </p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-150.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-1 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <a className="previous-link" href="/csm-artwork-4">
        <div className="animate1">
         <img
          className="img-fluid"
          src={chevronLeftImg}
          alt="Chevron left: Previous page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
     <div className="offset-col-10 col-1 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <a className="next-link" href="/csm-artwork-6">
        <div className="animate2">
         <img
          className="img-fluid"
          src={chevronRightImg}
          alt="Chevron right: Next page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default csmArtwork5;
