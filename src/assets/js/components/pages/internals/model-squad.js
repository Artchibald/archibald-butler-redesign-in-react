import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import ModelSquadImg1 from '../../../../img/internals/model-squad/model-squad1.jpg';
import ModelSquadImg2 from '../../../../img/internals/model-squad/model-squad2.jpg';
import ModelSquadImg3 from '../../../../img/internals/model-squad/model-squad3.jpg';
import JqueryIcon from '../../icons/jquery-icon.js';
import BitbucketIcon from '../../icons/bitbucket-icon.js';
import Css3Icon from '../../icons/css3-icon.js';
import GulpIcon from '../../icons/gulp-icon.js';
// import AngularIcon from '../../../../js/components/icons/angular-icon.js';
// import ApiIcon from '../../../../js/components/icons/api-icon.js';
// import BitbucketIcon from '../../../../js/components/icons/bitbucket-icon.js';
// import BootstrapIcon from '../../../../js/components/icons/bootstrap-icon.js';
// import CmsIcon from '../../../../js/components/icons/cms-icon.js';
// import CpanelIcon from '../../../../js/components/icons/cpanel-icon.js';
// import Css3Icon from '../../../../js/components/icons/css3-icon.js';
// import GifIcon from '../../../../js/components/icons/gif-icon.js';
// import GitIcon from '../../../../js/components/icons/git-icon.js';
// import GreensockIcon from '../../../../js/components/icons/greensock-icon.js';
// import GruntIcon from '../../../../js/components/icons/grunt-icon.js';
// import GulpIcon from '../../../../js/components/icons/gulp-icon.js';
// import HtmlIcon from '../../../../js/components/icons/html-icon.js';
// import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
// import LessIcon from '../../../../js/components/icons/less-icon.js';
// import NativeIcon from '../../../../js/components/icons/native-icon.js';
// import NodeIcon from '../../../../js/components/icons/node-icon.js';
// import PhpIcon from '../../../../js/components/icons/php-icon.js';
// import ReactiveIcon from '../../icons/reactive-icon.js';
// import SassIcon from '../../icons/sass-icon.js';
// import SeoIcon from '../../icons/seo-icon.js';
// import SqlIcon from '../../icons/sql-icon.js';
// import VideoIcon from '../../icons/video-icon.js';
// import WireframeIcon from '../../icons/wireframe-icon.js';
// import WordpressIcon from '../../icons/wordpress-icon.js';
class ModelSquad extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid portfolio">
                {/*                                 <AngularIcon />
                                <ApiIcon />
                                <BitbucketIcon />
                                <BootstrapIcon />
                                <CmsIcon />
                                <CpanelIcon />
                                <Css3Icon />
                                <GifIcon />
                                <GitIcon />
                                <GreensockIcon />
                                <GruntIcon />
                                <GulpIcon />
                                <HtmlIcon />
                                <JqueryIcon />
                                <LessIcon />
                                <NativeIcon />
                                <NodeIcon />
                                <PhpIcon />
                                <ReactiveIcon />
                                <SassIcon />
                                <SeoIcon />
                                <SqlIcon />
                                <VideoIcon />
                                <WireframeIcon />
                                <WordpressIcon /> */}
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">MODEL SQUAD: NEW LOOK JOBS</h1>
                            <h3 className="projHeaderP">A freelance web developer project created at the New Look London offices.</h3>
                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <JqueryIcon />
                                <Css3Icon />
                                <GulpIcon />
                                <BitbucketIcon />
                            </div>
                            <div className="clearfix">
                                <a className="btn btn-secondary readmoreBTN" href="https://www.archibaldbutler.com/projects/model-squad/">
                                    View the work
                                </a>
                                <p>(Not fully functioning due to the client removing files from their server)</p>
                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={ModelSquadImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>BEEN DOING SOME GREAT DESIGN WORK AT THE NEW LOOK HEAD OFFICE IN LONDON!</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Working as a freelancer At New Look</h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        Model squad is a project that I created while I was working at New Look for over five months. I worked with them creating all the new promotions for the big Christmas sale but also the January sale. I really enjoyed my time there and got to work in a great team with two other very talented developers. We worked on a lot of exciting different projects.
                                    </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        Some of the languages we worked with included gulp, CSS, JavaScript animation, and Bitbucket repositories. The atmosphere in the office is absolutely buzzing. There is always a lot of work on and it is a great environment to flourish in. It is mostly a female environment as it’s Fashion Retail after all. I worked with a team of 12 designers of which all were women except one.</p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://www.archibaldbutler.com/projects/new-look-model-squad/index.html">
                                        View the work
                        </a>
                                    <p>(Not fully functioning due to the client removing files from their server)</p>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <JqueryIcon />
                                    <Css3Icon />
                                    <GulpIcon />
                                    <BitbucketIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="paddedPortfolioImg img-fluid"
                                    alt="Earthgang illustrations and animations in React JS" src={ModelSquadImg2} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">New Look Jobs: About this project</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Theory:</h3>
                                        <p>
                                            Model squad is a partnership between the three freelance developers that worked at New Look. Another exciting aspect of the project is that it was sponsored by Milk. They are a very famous company for model recruiting in London. We pulled our heads together to work out the best way of creating all the different animations and Modal effects that our project manager wanted included.
                            </p>
                                        <p>
                                            This work has a newly arisen bug issues since New Look moved their files.
                            </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Javascript Frameworks:</h3>
                                        <p>
                                            <h3>Working on it:</h3>
                                            <p>
                                                The technologies on this particular project were a mix of bootstrap JavaScript, bootstrap CSS but also Jquery for custom functions that were included. It’s important to have good communication skills, be able to relay and interpret information from the different team members. Also from your project manager who is usually an experienced designer. So don’t forget to force yourself to be as communicative as possible. Being pro active and productive in the work environment will make you shine and get recommended.
                            </p>
                                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://www.archibaldbutler.com/projects/new-look-model-squad/index.html">
                                        View the work
                        </a>
                                    <p>(Not fully functioning due to the client removing files from their server)</p>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="paddedPortfolioImg img-fluid"
                                    alt="Earthgang illustrations and animations in React JS" src={ModelSquadImg3} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">It’s fun to get free reign from the client</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Conclusion about how you can get involved</h3>
                                        <p>
                                            Team work is also a great environment to thrive in as a developer. This is because if you are stuck with a certain coding or design problem, you can always go and walk over to another developer and ask his opinion which will help you solve complicated algebra issues. The project overall must’ve taken about 80 hours divided by three developers.
                                        </p>

                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            It was an absolute privilege to work with such a talented team and I hope I can go back there soon. I very much like the design and animation effects that were integrated into this project. It was also exciting to have a deadline at which the page had to go live. Which was at the same time as New Look press released the new models.
                                      </p>

                                    </div>
                                    <div className="col-12">
                                        <h3>HTML5:</h3>
                                        <p>
                                            If you are looking to get work at New Look as a freelancer, I would highly recommend the Venn group. They are a recruiting agency that specialises in finding placements for freelancers. They are based in London near Trafalgar Square.
                                       </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://www.archibaldbutler.com/projects/new-look-model-squad/index.html">
                                        View the work
                                       </a>
                                    <p>(Not fully functioning due to the client removing files from their server)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModelSquad; 