import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
//icons
import NodeIcon from "../../icons/node-icon.js";
import Css3Icon from "../../icons/css3-icon";
import SassIcon from "../../icons/sass-icon";
import ReactiveIcon from "../../icons/reactive-icon";
// img
import intro from "../../../../img/internals/scuba-point/intro.png";
import scubapoint1 from "../../../../img/internals/scuba-point/scubapoint-1.png";
import scubapoint2 from "../../../../img/internals/scuba-point/scubapoint-2.png";
import scubapoint3 from "../../../../img/internals/scuba-point/scubapoint-3.png";
import scubapoint4 from "../../../../img/internals/scuba-point/scubapoint-4.png";
import scubapoint5 from "../../../../img/internals/scuba-point/scubapoint-5.png";
import scubapoint6 from "../../../../img/internals/scuba-point/scubapoint-6.png";
import scubapointRecord from "../../../../img/internals/scuba-point/scubapoint.mp4";
import scubapointRecord2 from "../../../../img/internals/scuba-point/scubapoint_2.mp4";

class Scubapoint extends Component {
    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid portfolio">
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">
                                Built a wicked animated website in Procreate recently
                            </h1>
                            <h3 className="projHeaderP">
                                The client liked my work and said go wild with it. So I indulged him and did!
                            </h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">

                                <NodeIcon />
                                <Css3Icon />
                                <SassIcon />
                                <ReactiveIcon />
                                {/*
    <AngularIcon />
    <ApiIcon />
    <BitbucketIcon />
    <BootstrapIcon />
    <CmsIcon />
    <CpanelIcon />
    <Css3Icon />
    <GifIcon />
    <GitIcon />
    <GreensockIcon />
    <GruntIcon />
    <GulpIcon />
    <HtmlIcon />
    <JqueryIcon />
    <LessIcon />
    <NativeIcon />
    <NodeIcon />
    <PhpIcon />
    <ReactiveIcon />
    <SassIcon />
    <SeoIcon />
    <SqlIcon />
    <VideoIcon />
    <WireframeIcon />
    <WordpressIcon /> */}
                            </div>
                            <div className="clearfix">
                                <a
                                    className="btn btn-secondary readmoreBTN inline"
                                    href="https://scuba-point.com/"
                                >
                                    View website
                                </a>

                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="cromwellplace.com" src={intro} />
                    </div>
                </div>
                <div className="video_wrapper screen_bordered container-fluid">
                    <div className="row no-pad">
                        <video className="w-100" autoPlay loop muted>
                            <source src={scubapointRecord} type="video/mp4" />
                        </video>
                    </div>
                </div>
                <div className="video_wrapper screen_bordered container-fluid">
                    <div className="row no-pad">
                        <video className="w-100" autoPlay loop muted>
                            <source src={scubapointRecord2} type="video/mp4" />
                        </video>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">1</div>
                                <div className="text">
                                    <p>Loving the Apple pen and Procreate!</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                                    Had so so much fun drawing, sketching, inking, presenting, reviewing and implementing this website. I worked with a scuba diving school  in Tenerife.
                                </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        The client was really laid back and loves the final live web app at scuba-point.com so this website was a win-win all round. I love having it in my portfolio too.
                                    </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        This typoe of creative freedom really reflects my work, but also creates income for the client! I love it when my work is generating dem fresh dollar dollar bills y'all.
                                    </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a
                                        className="btn btn-secondary clearfix"
                                        href="https://scuba-point.com/"
                                    >
                                        View the work
                                    </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <NodeIcon />
                                    <Css3Icon />
                                    <SassIcon />
                                    <ReactiveIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img
                                    className="img-fluid mt-5"
                                    alt="cromwellplace.com"
                                    src={scubapoint1}
                                />
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">2</div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                                    This is a premium blend of React, Vanilla Javascript, Node, Sass animations. I added in some clean cut latest updated node and react versions too, so this guy has all the latest bells and whistles.
                                </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Theory:</h3>
                                        <p>
                                            Putting the design aspects to one side, I spent a lot of time cutting and implementing imagery then animating it with css only. No Greensock JS required here.
                                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Working on it:</h3>
                                        <p>
                                            It was a time consuming process, probably 70 hours for the whole project end to end. I kept on going though that code until launch though, as I was clearly inspired and eager to go live with this!
                                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a
                                        className="btn btn-secondary clearfix"
                                        href="https://scuba-point.com/"
                                    >
                                        View the work
                                    </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid" alt="cromwellplace.com" src={scubapoint2} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">3</div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                                    Really enjoyed the art part and really enjoyed the coding part on this!
                                </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            I really like my inclusion of the Fibunacci spiral or golden ratio on the original draft. It looks cool and I think it's a nice touch.
                                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">

                                        <p>
                                            The team at Scuba Point Tenerife were awesome to work with and the work was totally fun end to end.
                                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a
                                        className="btn btn-secondary clearfix"
                                        href="https://scuba-point.com/"
                                    >
                                        View the work
                                    </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid" alt="cromwellplace.com" src={scubapoint4} />
                                <img className="img-fluid" alt="cromwellplace.com" src={scubapoint5} />
                                <img className="img-fluid" alt="cromwellplace.com" src={scubapoint6} />
                                <img className="img-fluid" alt="cromwellplace.com" src={scubapoint3} />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Scubapoint;
