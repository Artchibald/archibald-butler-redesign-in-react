import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';
//icons
import BootstrapIcon from '../../icons/bootstrap-icon.js';
import CmsIcon from '../../icons/cms-icon.js';
import GulpIcon from '../../icons/gulp-icon.js';
import NativeIcon from '../../icons/native-icon.js';
import PhpIcon from '../../icons/php-icon.js';
import WordpressIcon from '../../icons/wordpress-icon.js';

//img
import deliverooImg1 from '../../../../img/internals/deliveroo/Deliveroo-freelancing-london-web-developer-1.jpg';
import deliverooImg2 from '../../../../img/internals/deliveroo/Deliveroo-freelancing-london-web-developer-2.jpg';
import deliverooImg3 from '../../../../img/internals/deliveroo/Deliveroo-freelancing-london-web-developer-3.jpg';

class Deliveroo extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }



    render() {
        return (
            <div className="container-fluid portfolio">
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle"><a href="https://www.google.co.uk/maps/place/Deliveroo+HQ/@51.5101095,-0.091093,15z/data=!4m2!3m1!1s0x0:0x5951dd455dd060c7?sa=X&amp;ved=0ahUKEwjgq66_qf_WAhVlKsAKHQwmBQgQ_BIIwgEwHA">Deliveroo</a> Freelancing in London:</h1>
                            <h3 className="projHeaderP">Two weeks of WordPress mayhem.</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <BootstrapIcon />
                                <CmsIcon />
                                <GulpIcon />
                                <NativeIcon />
                                <PhpIcon />
                                <WordpressIcon />
                            </div>
                            <div className="clearfix">
                                <a className="btn btn-secondary readmoreBTN" href="https://archibaldbutler.com/projects/Deliveroo/index.html">
                                    View the work
                    </a>
                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={deliverooImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>DELIVEROO FREELANCING</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Deliveroo ordered me Deliveroo at the Deliveroo HQ!
                    </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        Working for Deliveroo was an absolutely amazing experience. The job was a really complex multi-site, multi language, custom theme built in .twig.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        I cannot thank <a href="https://www.linkedin.com/in/rory-russell-b2480131/">Rory Russell</a> enough for this amazing experience. And I haven’t even started talking about the work yet.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        Their offices are the most amazing ones I have ever seen in my life. In the middle of the office is a full scale basketball court. The meeting rooms contain swinging chairs and incased digital touch screens.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        The WordPress for their <a href="http://www.roocommunity.com">roocommunity.com</a> website was outsourced to a professional WordPress development company.
                        </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://archibaldbutler.com/projects/Deliveroo/index.html">
                                        View the work
                        </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <BootstrapIcon />
                                    <CmsIcon />
                                    <GulpIcon />
                                    <NativeIcon />
                                    <PhpIcon />
                                    <WordpressIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid mt-5" alt="Kinesis Money: Archibald Butler Web Development" src={deliverooImg2} />
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">About this Deliveroo web project
                    </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Theory:</h3>
                                        <p>
                                            The template wasn’t built in PHP but in Timber .twig files. Due to the tight timescale I only had a couple of days to learn how to write this new language.
                        </p>
                                        <p>
                                            Finding the right PHP files to edit within the custom theme was complicated. I had to use the search features from sublime text editor to retrieve functions and arrays hidden in particular files.
                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Working on it:</h3>
                                        <p>
                                            On top of that they use the advanced custom fields plug-in, yet there was absolutely no mention of the modules that were appearing in the WordPress dashboard in the plug-in section.
                        </p>
                                        <p>
                                            I came to realise that the options of advanced custom fields were written completely inside of arrays.
                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://archibaldbutler.com/projects/Deliveroo/index.html">
                                        View the work
                        </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={deliverooImg3} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Advanced WordPress development</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            I very much enjoy working with the boys at ASOS. As a freelancer, I have worked almost three years there full time.
                        </p>
                                        <p>
                                            I also used a ring fence div to stop my CSS code conflicting with theirs. I noticed Bootstrap CSS was integrated onto the WordPress platform they were using so I used that to create the responsive elements contained within the design.
                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            I wasn’t given a responsive version of the Photoshop design so I had to make it up on the fly using my knowledge of bootstrap CSS.
                        </p>
                                        <h3>Best of the project:</h3>
                                        <p>
                                            At their request I did manage to integrate a WYSIWYG editor so that the delivery staff could add custom HTML to the pages.
                        </p>
                                        <p>
                                            I hope you enjoy looking at the work as much as I enjoyed making it. Archie Butler.
                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://archibaldbutler.com/projects/Deliveroo/index.html">
                                        View the work
                        </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Deliveroo; 