import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
import csmImg150 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-150.jpg";
import csmImg151 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-151.jpg";
import csmImg152 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-152.jpg";
import csmImg154 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-154.jpg";
import csmImg155 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-155.jpg";
import csmImg156 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-156.jpg";
import csmImg157 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-157.jpg";
import csmImg158 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-158.jpg";
import csmImg159 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-159.jpg";
import csmImg160 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-160.jpg";
import csmImg161 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-161.jpg";
import csmImg162 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-162.jpg";
import csmImg163 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-163.jpg";
import csmImg164 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-164.jpg";
import csmImg165 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-165.jpg";
import csmImg166 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-166.jpg";
import csmImg167 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-167.jpg";
import csmImg168 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-168.jpg";
import csmImg169 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-169.jpg";
import csmImg170 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-170.jpg";
import csmImg171 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-171.jpg";
import csmImg172 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-172.jpg";
import csmImg173 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-173.jpg";
import csmImg174 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-174.jpg";
import csmImg175 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-175.jpg";
import csmImg176 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-176.jpg";

import chevronLeftImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";

class csmArtwork6 extends Component {
 componentDidMount() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid">
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-12 col-lg-12">
      <h1 className="text-center mt-5">May the horror continue!</h1>
      <p className="text-center mb-5">
       These drawings cost me a £25,000.00 student debt. Wow… Just wow.
      </p>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg154}
       />
       <figcaption>
        <h2>Some more of the same…</h2>
        <p>But boringly slightly different.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-154.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg155}
       />
       <figcaption>
        <h2>Oh wow you drew an amp</h2>
        <p>Then added squiggles on it. BRAVO BRAVO.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-155.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg156}
       />
       <figcaption>
        <h2>At last: The demonic drawing.</h2>
        <p>
         This is when an art student wants to look challenging and cool. Fail.
        </p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-156.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg157}
       />
       <figcaption>
        <h2>Silver surfer</h2>
        <p>One of my childhood heroes.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-157.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg158}
       />
       <figcaption>
        <h2>Some grotty fifth element type stuff</h2>
        <p>Not the finest is art skills ey…</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-158.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg159}
       />
       <figcaption>
        <h2>More rough lines</h2>
        <p>Little squiggles of “fall asleeps”.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-159.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg160}
       />
       <figcaption>
        <h2>
         Wow, <span>keep a lid on it….</span>
        </h2>
        <p>You filthy little violent boy.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-160.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg161}
       />
       <figcaption>
        <h2>Another room in London</h2>
        <p>Another quick squiggle, we are almost done here, thank god.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-161.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg162}
       />
       <figcaption>
        <h2>Illegal vandalism</h2>
        <p>Im my sketch books. Ridiculous…</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-162.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg163}
       />
       <figcaption>
        <h2>Crocodile from underneath</h2>
        <p>Part of an African graphic novel I was doing.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-163.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg164}
       />
       <figcaption>
        <h2>My Papa!</h2>
        <p>
         Found his ID pictures and stuck them together. Haha. Most handsome if I
         may.
        </p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-164.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg165}
       />
       <figcaption>
        <h2>A strip club.</h2>
        <p>I was messing around with Photoshop colours.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-165.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg166}
       />
       <figcaption>
        <h2>Kill Bill?</h2>
        <p>Silly Billy if ypou ask me.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-166.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg167}
       />
       <figcaption>
        <h2>Some hipster twit</h2>
        <p>I was trying out colour in Photoshop.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-167.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg168}
       />
       <figcaption>
        <h2>Some weird new style</h2>
        <p>Inspired by something I saw.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-168.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg169}
       />
       <figcaption>
        <h2>More museum drawings</h2>
        <p>oouuuuh. So sophisticated.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-169.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg170}
       />
       <figcaption>
        <h2>Rudeboys:</h2>
        <p>A bit like the ones who mugged my young as* self.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-170.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-12 col-md-12 col-lg-4">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg171}
       />
       <figcaption>
        <h2>A stencil I used for my surfboard from scratch project</h2>
        <p>Will add it soon…</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-171.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg172}
       />
       <figcaption>
        <h2>Some fake manga</h2>
        <p>Oh why don’t you move to Japan, Archinata.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-172.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg173}
       />
       <figcaption>
        <h2>This is a bit like a good action movie.</h2>
        <p>Except, NO, It’s a load of crud.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-173.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg174}
       />
       <figcaption>
        <h2>The old Bailey</h2>
        <p>Drawn from Photo</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-174.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg175}
       />
       <figcaption>
        <h2>More furnishing sketches</h2>
        <p>Still a load of half a**ed crud.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-175.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg176}
       />
       <figcaption>
        <h2>Oh wow. You can use Photoshop.</h2>
        <p>Give it up cracker Jack.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-176.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg150}
       />
       <figcaption>
        <h2>More Graphic novel Shiznit</h2>
        <p>Some old ramblings about an ex girlfriend I recall…</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-150.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg151}
       />
       <figcaption>
        <h2>More Graphic novel Shiznit</h2>
        <p>Some old ramblings about an ex girlfriend I recall… </p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-151.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg152}
       />
       <figcaption>
        <h2>More Graphic novel Shiznit</h2>
        <p>Goodbye and thank you for wasting time with me!</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-152.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-1 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <a className="previous-link" href="/csm-artwork-5">
        <div className="animate1">
         <img
          className="img-fluid"
          src={chevronLeftImg}
          alt="Chevron left: Previous page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default csmArtwork6;
