import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";

import csmImg1 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-1.jpg";
import csmImg2 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-2.jpg";
import csmImg3 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-3.jpg";
import csmImg4 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-4.jpg";
import csmImg5 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-5.jpg";
import csmImg6 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-6.jpg";
import csmImg7 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-7.jpg";
import csmImg8 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-8.jpg";
import csmImg9 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-9.jpg";
import csmImg10 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-10.jpg";
import csmImg11 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-11.jpg";
import csmImg12 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-12.jpg";
import csmImg13 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-13.jpg";
import csmImg14 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-14.jpg";
import csmImg15 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-15.jpg";
import csmImg16 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-16.jpg";
import csmImg17 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-17.jpg";
import csmImg18 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-18.jpg";
import csmImg19 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-19.jpg";
import csmImg20 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-20.jpg";
import csmImg21 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-21.jpg";
import csmImg22 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-22.jpg";
import csmImg23 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-23.jpg";
import csmImg24 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-24.jpg";
import csmImg25 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-25.jpg";
import csmImg26 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-26.jpg";
import csmImg27 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-27.jpg";
import csmImg28 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-28.jpg";
import csmImg29 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-29.jpg";
import csmImg30 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-30.jpg";
import chevronRightImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class csmArtwork extends Component {
 componentDidMount() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid">
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-12 col-lg-12">
      <h1 className="text-center mt-5">
       My Sketchbooks from Art School! Most were created around 2008.
      </h1>
      <p className="text-center mb-5">
       An interesting journey from an anxious teen. Each with individual
       comment. Don’t forget the pagination at the bottom of the page for each
       set.
      </p>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg11}
       />
       <figcaption>
        <h2>
         You deserve <span>a slap</span>.
        </h2>
        <p>You’re def seeing double bru</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-30.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg1}
       />
       <figcaption>
        <h2>
         Samurai <span>meets goldfish</span>.
        </h2>
        <p>Instant attraction</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-1.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg3}
       />
       <figcaption>
        <h2>
         Warrior <span>drowned</span>.
        </h2>
        <p>Random different splatter</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-2.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg4}
       />
       <figcaption>
        <h2>
         Things inside <span>Things</span>.
        </h2>
        <p>Spiritual beings living in our everyday hipster lives</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-3.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg5}
       />
       <figcaption>
        <h2>
         Ballet dancer <span>handle handler</span>.
        </h2>
        <p>Those crooked asians…</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-4.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg6}
       />
       <figcaption>
        <h2>
         I cut <span>a tree</span>.
        </h2>
        <p>Then asked it about my sexuality</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-5.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg7}
       />
       <figcaption>
        <h2>
         Drunken <span>SAMURAI WHoa!</span>.
        </h2>
        <p>A bit like me but less chubby</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-6.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg8}
       />
       <figcaption>
        <h2>
         Who put <span>MAGIC MUSHROOMS</span>.
        </h2>
        <p>in my PG tips</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-7.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg9}
       />
       <figcaption>
        <h2>
         Angry <span>Friends</span>.
        </h2>
        <p>I drew at cos play com</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-8.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg10}
       />
       <figcaption>
        <h2>
         Hold <span>that pencil</span>.
        </h2>
        <p>Worth a 1000 swords and all that jazz..</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-9.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg2}
       />
       <figcaption>
        <h2>
         Kid <span>Trick</span>.
        </h2>
        <p>Entrance to high school, from memory.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-10.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg12}
       />
       <figcaption>
        <h2>
         Clint <span>Eastwod</span>.
        </h2>
        <p>And his hot lay lay.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-30.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg13}
       />
       <figcaption>
        <h2>
         Scan’em, <span>Cut’em up!</span>.
        </h2>
        <p>Re-using to impress art teachers.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-1.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg14}
       />
       <figcaption>
        <h2>
         Stick ’em <span>up!</span>.
        </h2>
        <p>This hipster can make you famous.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-2.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg15}
       />
       <figcaption>
        <h2>
         This one’s <span>messy…</span>.
        </h2>
        <p>If you take “Away” out.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-3.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg16}
       />
       <figcaption>
        <h2>
         Sketch <span>of the book cover:</span>.
        </h2>
        <p>The Dark Is Rising.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-4.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg17}
       />
       <figcaption>
        <h2>
         Human <span>Anatomy</span>.
        </h2>
        <p>Trying to get proportions from memory.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-5.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg18}
       />
       <figcaption>
        <h2>
         Some Jokes <span>About Bin men</span>.
        </h2>
        <p>I drew these for a holiday rental’s binning instructions</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-6.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg19}
       />
       <figcaption>
        <h2>
         BinMen, <span>SAS shakedown.</span>.
        </h2>
        <p>I drew these for a holiday rental’s binning instructions</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-7.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg20}
       />
       <figcaption>
        <h2>
         The Old Bailey, <span>London</span>.
        </h2>
        <p>Loving the caption</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-8.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-12 col-md-12 col-lg-4">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg21}
       />
       <figcaption>
        <h2>
         Inside the <span>Old Bailey</span>.
        </h2>
        <p>These were some graphic novel ideas.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-20.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg22}
       />
       <figcaption>
        <h2>
         A Village <span>from my mind</span>.
        </h2>
        <p>I wonder if there is a place like this?.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-21.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg23}
       />
       <figcaption>
        <h2>
         Marker Pen <span>Wings</span>.
        </h2>
        <p>On my grifriend.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-22.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg24}
       />
       <figcaption>
        <h2>
         A hand made <span>surfboard</span>.
        </h2>
        <p>And a fashion art project</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-23.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg25}
       />
       <figcaption>
        <h2>
         What was in that <span>Burger!</span>
        </h2>
        <p>Or could it be the future of headsets?</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-24.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg26}
       />
       <figcaption>
        <h2>
         Well i hope this is <span>true</span>!
        </h2>
        <p>I hope this was a vision of oil extinction!</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-25.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg27}
       />
       <figcaption>
        <h2>
         A favorite <span>Grimace</span>.
        </h2>
        <p>I drew this guy many times, love the eyes.</p>

        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-26.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg28}
       />
       <figcaption>
        <h2>
         Some next<span> Robo Firework</span>.
        </h2>
        <p>Would love to see this at a rave!</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-27.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg29}
       />
       <figcaption>
        <h2>
         The Hitch Hiker’s<span> Guide</span>.
        </h2>
        <p>To Skirtonomy.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-28.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Art school"
        src={csmImg30}
       />
       <figcaption>
        <h2>
         Vroom Vroom <span>Old Man</span>.
        </h2>
        <p>Wherever you came from.</p>
        <a
         aria-label="See work"
         href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-29.jpg"
        >
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="offset-col-11 col-1 offset-sm-11 col-sm-1 offset-md-11 col-md-1 offset-lg-11 col-lg-1 no-pad">
      <div className="crop2">
       <a aria-label="See work" className="next-link" href="/csm-artwork-2">
        <div className="animate2">
         <img
          className="img-fluid"
          src={chevronRightImg}
          alt="Chevron right: Next page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default csmArtwork;
