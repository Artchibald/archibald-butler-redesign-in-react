import React, { Component } from 'react';
import { BrowserRouter as Route, Link } from "react-router-dom";
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';
//icons
import GifIcon from '../../icons/gif-icon.js';
import GreensockIcon from '../../icons/greensock-icon.js';
import JqueryIcon from '../../icons/jquery-icon.js';
import WireframeIcon from '../../icons/wireframe-icon.js';

import diamondsImg1 from '../../../../img/internals/diamonds/political-cartoons-afriican-daimonds-web-animation-1.jpg';
import diamondsImg2 from '../../../../img/internals/diamonds/political-cartoons-african-diamonds-web-animation-2.jpg';
import diamondsImg3 from '../../../../img/internals/diamonds/political-cartoons-african-diamonds-web-animation-3.jpg';
import diamondsImg4 from '../../../../img/internals/diamonds/african-diamonds-sketch-1.jpg';
import diamondsImg5 from '../../../../img/internals/diamonds/african-diamonds-sketch-2.jpg';
import diamondsImg6 from '../../../../img/internals/diamonds/african-diamonds-sketch-3.jpg';
import diamondsImg7 from '../../../../img/internals/diamonds/african-diamonds-sketch-4.jpg';
import diamondsImg8 from '../../../../img/internals/diamonds/african-diamonds-sketch-5.jpg';

class Diamonds extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid portfolio">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">ANOTHER POLITICAL CARTOON:</h1>
                            <h3 className="projHeaderP">The notorious theme of inequality.</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <GifIcon />
                                <GreensockIcon />
                                <JqueryIcon />
                                <WireframeIcon />

                            </div>

                            <div className="clearfix">
                                <Link className="btn btn-secondary readmoreBTN"
                                    to="/political-cartoon-animations-africas-diamonds/">
                                    View the work
                    </Link>

                                <Link to="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </Link>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="HTML5 animated illustration" src={diamondsImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>ANOTHER ANIMATION PROJECT IN CSS AND JS</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">This one got sidelined for a
                        while...</h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        It did eventually get finished though! Another project similar to my <Link to="/">home
                                page</Link> and <Link
                                            to="/killuminati-first-political-cartoon/">Killuminati</Link>. This bizarre piece of political attack was my third illustration in this series.
                            <br className="c" />
                                    </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        I remember seeing this vision when I was just about to fall asleep in my room. I rushed out
                                        of bed and created the illustrations that you can see at the bottom of the page.
                        </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <Link className="btn btn-secondary clearfix"
                                        to="/political-cartoon-animations-africas-diamonds/">
                                        View the work
                        </Link>

                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <GifIcon />
                                    <GreensockIcon />
                                    <JqueryIcon />
                                    <WireframeIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="paddedPortfolioImg img-fluid"
                                    alt="Earthgang illustrations and animations in React JS" src={diamondsImg2} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">About this project</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            The coding aspect of this page didn’t take much time as this is my fourth piece in this
                                            collection of web animated illustrations.
                            </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            It did take a while to super impose the four layers of background that you can see,
                                            moving at different speeds horizontally.
                            </p>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <Link className="btn btn-secondary clearfix"
                                        to="/political-cartoon-animations-africas-diamonds/">
                                        View the work
                        </Link>

                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="paddedPortfolioImg img-fluid"
                                    alt="Earthgang illustrations and animations in React JS" src={diamondsImg3} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Doing your personal work is
                        most enjoyable at times.</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Best of the project:</h3>
                                        <p>
                                            I am happy with the background animation that gives the diamond the impression it is
                                            physically moving, and people said they liked the flashes of light in the diamond.
                            </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            On the other hand, I do feel that this project was a little bit rushed. The colours
                                            didn’t come out very well, I must remember to photograph them in natural light only as
                                            the homepage of this website was.
                            </p>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                                        <img className="img-fluid" alt="Animated illustrations by Archibald Butler"
                                            src={diamondsImg4} />
                                    </div>

                                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                                        <img className="img-fluid" alt="Animated illustrations by Archibald Butler"
                                            src={diamondsImg5} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                                        <img className="img-fluid" alt="Animated illustrations by Archibald Butler"
                                            src={diamondsImg6} />
                                    </div>

                                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                                        <img className="img-fluid" alt="Animated illustrations by Archibald Butler"
                                            src={diamondsImg7} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-12 col-lg-12 mb-4">
                                        <img className="img-fluid" alt="Animated illustrations by Archibald Butler"
                                            src={diamondsImg8} />
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <p>I hope you enjoyed this project, feel free to get in touch if you have any questions.</p>
                                    <Link className="btn btn-secondary clearfix"
                                        to="/political-cartoon-animations-africas-diamonds/">
                                        View the work
                        </Link>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Diamonds; 