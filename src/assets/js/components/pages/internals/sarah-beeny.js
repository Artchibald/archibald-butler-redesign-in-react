import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import SarahBeenyImg1 from '../../../../img/internals/sarah-beeny/sarah-beeny1.jpg';
import SarahBeenyImg2 from '../../../../img/internals/sarah-beeny/sarah-beeny2.jpg';
import SarahBeenyImg3 from '../../../../img/internals/sarah-beeny/sarah-beeny3.jpg';


class SarahBeeny extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Working for Sarah Beeny</h1>
                            <p>This was an intial sketch for this website I was thinking of creating a few years back.
                    <br /><br />
                                Ths is the first large scale design i created for a website.
                    <br /><br />
                                All the elements of this website template are hand drawn, photographed as sketches and scanned. I
                                then coloured them using some nice watercolor Photoshop brushes. I drew the elements separately and
                                then made a “collage” to achieve some even sizing and balance.
                    <br /><br />
                                The next step was choosing fonts and colours. It is always best to test things out by creating a
                                font pack in a separate file with a white background. Don’t forget to choose similar shaped letters
                                or they will look odd. I also added some nice bold buttons to create a little call-to-action
                                functionality.
                </p>

                            <br />
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/web-design-templates/archiesutopia/">View the work!</a>
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/free-photoshop-downloads/cartoon-website-illustration-template.zip">Download
                    it!</a>

                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler" src={SarahBeenyImg1} />
                            <figcaption>
                                <h2><span>Sarah Beeny</span></h2>
                                <p>Website illustration and animation techniques</p>

                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler" src={SarahBeenyImg2} />
                            <figcaption>
                                <h2><span>Sarah Beeny</span></h2>
                                <p>Website illustration and animation techniques</p>


                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>It was pure fun creating this</h1>
                            <p>
                                The brief was wild animation.
                                The first of its kind I had received.
                                I managed to get this together and was happy with it.



                            </p>
                            <br />
                            <a className="btn btn-secondary readmoreBTN d-inline mt-4"
                                href="https://archibaldbutler.com/web-design-templates/archiesutopia/">View the work!</a>
                            <a className="btn btn-secondary readmoreBTN d-inline mt-4"
                                href="https://archibaldbutler.com/free-photoshop-downloads/cartoon-website-illustration-template.zip">Download
                    it!</a>
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Conclusion</h1>
                            <p>
                                This design made me realize my potential and my incline for this specialisation. It was a turning
                    point for me! I hope one day I can bring it to life.

                            </p>
                            <br />
                            <a className="btn btn-secondary readmoreBTN d-inline mt-4"
                                href="https://archibaldbutler.com/web-design-templates/archiesutopia/">View the work!</a>
                            <a className="btn btn-secondary readmoreBTN d-inline mt-4"
                                href="https://archibaldbutler.com/free-photoshop-downloads/cartoon-website-illustration-template.zip">Download
                    it!</a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler" src={SarahBeenyImg3} />
                            <figcaption>
                                <h2><span>Sarah Beeny</span></h2>
                                <p>Website illustration and animation techniques</p>


                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default SarahBeeny;