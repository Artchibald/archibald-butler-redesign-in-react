import React, { Component } from 'react';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';
//icons
import BootstrapIcon from '../../icons/bootstrap-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import NativeIcon from '../../icons/native-icon.js';

// img
import brewedTop from '../../../../img/internals/brewed/brewed.jpg';
import BrewedImg1 from '../../../../img/internals/brewed/showcase_1.jpg';
import BrewedImg2 from '../../../../img/internals/brewed/showcase_2.jpg';
import BrewedImg3 from '../../../../img/internals/brewed/showcase_3.jpg';

class BrewedOnline extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid portfolio">
        <div className="row no-pad">
          <div className="col-12 col-sm-12 col-md-6 col-lg-6">
            <div className="textbox">
              <h1 className="projHeaderTitle">Brewed.online: Coffee Ecom.</h1>
              <h3 className="projHeaderP">
                An excellent example of bespoke design.
              </h3>
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>

              <div className="clearfix">
                <BootstrapIcon />
                <ReactiveIcon />
                <NativeIcon />{' '}
              </div>
              <div className="clearfix">
                <a
                  className="btn btn-secondary readmoreBTN inline"
                  href="https://brewed.online"
                >
                  View website
                </a>
                <a
                  className="btn btn-secondary readmoreBTN inline"
                  href="https://archibaldbutler.com/projects/brewed-v28/loading.html"
                >
                  Load anim
                </a>
                <a
                  className="btn btn-secondary readmoreBTN inline"
                  href="https://archibaldbutler.com/projects/brewed-v28/loading-vid.html"
                >
                  Load video
                </a>

                <a href="#mainPara">
                  <div className="scroll-down-dude"></div>
                </a>
                <br className="c" />
                <br className="c" />
                <br className="c" />
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
            <img className="img-fluid" alt="Brewed.online" src={brewedTop} />
          </div>
        </div>
        <div className="container-fluid">
          <div className="row no-pad" id="mainPara">
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">1</div>
                <div className="text">
                  <p>
                    A handcrafted bespoke ecom website for a coffee&nbsp;shop
                  </p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  One of my friends, Bob, got in touch with me about creating a
                  brand spanking new ecom for his coffee
                  dealership&nbsp;business.
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    It was during the pandemic when his shop was not open and he
                    wanted to move his business into the digital&nbsp;sphere.
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    I cannot thank them enough for giving me this opportunity,
                    this looks awesome in my&nbsp;portfolio!
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    className="btn btn-secondary clearfix"
                    href="https://brewed.online"
                  >
                    View the work
                  </a>
                </div>
              </div>
              <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                <h5 className="technoHeaderTitle">technologies used</h5>
                <div className="lineOnProject"></div>
                <div className="clearfix">
                  <BootstrapIcon />
                  <ReactiveIcon />
                  <NativeIcon />
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="img-fluid mt-5"
                  alt="Brewed.online"
                  src={BrewedImg1}
                />
              </div>
            </div>

            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">2</div>
                <div className="text">
                  <p>the code</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  About this Coffee Gig website&nbsp;project:
                </h2>
                <div className="row">
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <h3>Theory:</h3>
                    <p>
                      I am very keen to include animations into the work
                      nowadays. Clients often want to leave this because of
                      budget, but it really stands&nbsp;out.
                    </p>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <h3>Working on it:</h3>
                    <p>
                      This codebase was a partnership build between multiple
                      developers. We agreed on the tech stack and focused on our
                      individual chunks of work in an organised&nbsp;fashion.
                    </p>
                  </div>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    className="btn btn-secondary clearfix"
                    href="https://brewed.online"
                  >
                    View the work
                  </a>
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="img-fluid"
                  alt="Brewed.online"
                  src={BrewedImg2}
                />
                <img
                  className="img-fluid"
                  alt="Brewed.online"
                  src={BrewedImg3}
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">3</div>
                <div className="text">
                  <p>conclusion</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Built with great care
                </h2>
                <div className="row">
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <p>
                      I started the contract as an advisory role for Brewed.
                      They were quite stoked about all the different advice I
                      had given them for starting up.
                    </p>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <p>
                      They brought together a fantastic team composed of a
                      designer, a developer and quality control. Thus we created
                      this beast project!
                    </p>
                  </div>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    className="btn btn-secondary clearfix"
                    href="https://brewed.online"
                  >
                    View the work
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BrewedOnline;
