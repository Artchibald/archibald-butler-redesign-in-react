import React, { Component } from 'react';
import $ from 'jquery';
//icons
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';
//css
import '../../../../css/i_portfolio_internal_styles.scss';
import '../../../../css/c_icons.scss';
//img
import kinesisImg1 from '../../../../img/internals/kinesis/KINESIS-MONEY-Archibald-Butler-Web-Development.jpg';
import kinesisImg2 from '../../../../img/internals/kinesis/layout-kinesis-1.jpg';
import kinesisImg3 from '../../../../img/internals/kinesis/layout-kinesis-2.jpg';

class Kinesis extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid portfolio">
        <div className="row no-pad">
          <div className="col-12 col-sm-12 col-md-6 col-lg-6">
            <div className="textbox">
              <h1 className="projHeaderTitle">
                Kinesis.money: A crypto-currency website build
              </h1>
              <h3 className="projHeaderP">
                I had the pleasure of working in the finance industry lately,
                building an exciting cryptocurrency website.
              </h3>
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>

              <div className="clearfix">
                <Css3Icon />
                <ReactiveIcon />
                <SeoIcon />
                <NativeIcon />
              </div>
              <div className="clearfix">
                <a
                  aria-label="See work"
                  className="btn btn-secondary readmoreBTN"
                  href="https://kinesis.money/"
                >
                  View the work
                </a>
                <a aria-label="See work" href="#mainPara">
                  <div className="scroll-down-dude"></div>
                </a>
                <br className="c" />
                <br className="c" />
                <br className="c" />
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
            <img
              className="img-fluid"
              alt="Kinesis Money: Archibald Butler Web Development"
              src={kinesisImg1}
            />
          </div>
        </div>
        <div className="container-fluid">
          <div className="row no-pad" id="mainPara">
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">1</div>
                <div className="text">
                  <p>the brief</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Working as a freelancer at Kinesis
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <a aria-label="See work" href="https://kinesis.money/">
                      Kinesis
                    </a>{' '}
                    is a revolutionary cryptocurrency created by ABX (top
                    Australian gold bullion dealer). Their Initial Coin Offering
                    approach to world economy was fascinating. They are trying
                    to transform the banking industry.
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    I was hired to rebuild the website from scratch in
                    partnership with a designer over the course of a few months.
                    It was a very fast paced environment and learning Jai
                    Bifulco’s industry tips was a real insight.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary clearfix"
                    href="https://kinesis.money/"
                  >
                    View the work
                  </a>
                </div>
              </div>
              <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                <h5 className="technoHeaderTitle">technologies used</h5>
                <div className="lineOnProject"></div>
                <div className="clearfix">
                  <Css3Icon />
                  <ReactiveIcon />
                  <SeoIcon />
                  <NativeIcon />
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="img-fluid mt-5"
                  alt="Kinesis Money: Archibald Butler Web Development"
                  src={kinesisImg2}
                />
              </div>
            </div>

            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">2</div>
                <div className="text">
                  <p>the code</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Kinesis: About this project
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    The website is a fusion of multiple tech stacks, both back
                    and front end. HTML5, CSS3, Bootstrap, Php, MySql, AWS,
                    WordPress, ACF plus the incorporation of the external
                    Salesforce CRM. There was a lot of work to keep me busy:
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    Landing pages, automation setups, and later down the line, I
                    was later very pleased to discover their website/ICO has
                    raised over 60 million US dollars since launch mid 2018. I
                    was offered a full time job there and I regret not taking
                    it.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary clearfix"
                    href="https://kinesis.money/"
                  >
                    View the work
                  </a>
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="img-fluid"
                  alt="Kinesis Money: Archibald Butler Web Development"
                  src={kinesisImg3}
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">3</div>
                <div className="text">
                  <p>conclusion</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Conclusion about this cryptcurrency website
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    I learnt so much about the cryptocurrency/Bitcoin sphere
                    while working on this contract. Furthermore, the base staff
                    was from Australia and they are always very fun to work
                    with!
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    If anyone is considering joining I would actively encourage
                    it. To conclude, please not I have purchased one of their
                    tokens at 1000USD and I am actively following their ICO!
                  </p>
                  <p className="col-12 col-sm-12 col-md-12 col-lg-12">
                    If you are looking to get work at Kinesis as a freelancer, I
                    would highly recommend the Aquent group. They are a
                    recruiting agency that specialises in finding placements for
                    freelancers. They are based in London near Tottenham Court
                    rd.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary clearfix"
                    href="https://kinesis.money/"
                  >
                    View the work
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Kinesis;
