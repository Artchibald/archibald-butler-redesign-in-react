import React, { Component } from 'react';
import { BrowserRouter as Route, Link } from "react-router-dom";
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import Freddie2GetFitImg1 from '../../../../img/internals/freddie-2-get-fit/freddie-2-get-fit1.jpg';
import Freddie2GetFitImg2 from '../../../../img/internals/freddie-2-get-fit/freddie-2-get-fit2.jpg';
import Freddie2GetFitImg3 from '../../../../img/internals/freddie-2-get-fit/freddie-2-get-fit3.jpg';
//icons
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';


class Freddie2GetFit extends Component {

    componentDidMount() {
        //make sure we load at top of page 
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Working for Freddie 2 get fit</h1>
                            <p>Had a lot of fun developing this parallax website for Fred.</p>
                            <p>I designed and developed it using WordPress, .js, .php, and SEO plus some custom Photoshop work.</p>

                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <Css3Icon />
                                <ReactiveIcon />
                                <SeoIcon />
                                <NativeIcon />
                            </div>
                            <div className="clearfix" ></div>
                            <Link className="btn btn-secondary"
                                to="/news-2/">Site No Longer Exists  <i className="fa fa-ambulance"></i></Link>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={Freddie2GetFitImg1} />
                            <figcaption>
                                <h2>A Good Day of <span>Work</span></h2>
                                <p> Website illustration and animation techniques</p>

                                <Link to="/news-2/">View more</Link>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={Freddie2GetFitImg2} />
                            <figcaption>
                                <h2>The Freddie2GetFit <span>Estate</span></h2>
                                <p> Website illustration and animation techniques</p>
                                <br />
                                <Link
                                    to="/news-2/">Site No Longer Exists <i className="fa fa-ambulance"></i></Link>
                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Motivational design</h1>
                            <p>I had a go at designing some banners that would make people want to get toned and get in touch.</p>
                            <br />
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Conclusion</h1>
                            <p>I created this when Parallax was just starting. What fun :).</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={Freddie2GetFitImg3} />
                            <figcaption>
                                <h2>The Freddie2GetFit <span>Estate</span></h2>
                                <p> Website illustration and animation techniques</p>
                                <br />
                                <Link
                                    to="/news-2/">Site No Longer Exists <i className="fa fa-ambulance"></i></Link>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default Freddie2GetFit; 