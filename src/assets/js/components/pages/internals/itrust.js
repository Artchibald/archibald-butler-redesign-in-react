import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
//icons
import BootstrapIcon from "../../icons/bootstrap-icon.js";
import ReactiveIcon from "../../icons/reactive-icon.js";
import NativeIcon from "../../icons/native-icon.js";

// img
import iTrustTop from "../../../../img/internals/itrust/itrust-compressed.png";
import iTrustImg1 from "../../../../img/internals/itrust/showcase_1.png";
import iTrustImg2 from "../../../../img/internals/itrust/showcase_2.png";
import iTrustImg3 from "../../../../img/internals/itrust/showcase_3.png";
import iTrustRecording from "../../../../img/internals/itrust/recording.mp4";
class iTrust extends Component {
 componentDidMount() {
  //make sure we load at top of page
  window.scrollTo(0, 0);
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid portfolio">
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-6 col-lg-6">
      <div className="textbox">
       <h1 className="projHeaderTitle">
        Itrust.finance: A submersive Crypto&nbsp;ICO
       </h1>
       <h3 className="projHeaderP">Bitcoin is taking&nbsp;over!</h3>
       <br />
       <h5 className="technoHeaderTitle">technologies used</h5>
       <div className="lineOnProject"></div>

       <div className="clearfix">
        <BootstrapIcon />
        <ReactiveIcon />
        <NativeIcon />
       </div>
       <div className="clearfix">
        <a
         className="btn btn-secondary readmoreBTN inline"
         href="https://itrust.finance"
        >
         View website
        </a>

        <a href="#mainPara">
         <div className="scroll-down-dude"></div>
        </a>
        <br className="c" />
        <br className="c" />
        <br className="c" />
       </div>
      </div>
     </div>
     <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
      <img className="img-fluid" alt="itrust.finance" src={iTrustTop} />
     </div>
    </div>
    <div className="video_wrapper screen_bordered container-fluid">
     <div className="row no-pad">
      <video className="w-100" autoPlay loop muted>
       <source src={iTrustRecording} type="video/mp4" />
      </video>
     </div>
    </div>
    <div className="container-fluid">
     <div className="row no-pad" id="mainPara">
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">1</div>
        <div className="text">
         <p>I designed and built a website for the DEFI crypto&nbsp;sector</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         Rob Cooke is a major player in the crypto scene. He got in touch with
         me because we had worked together in the past. He wanted me to design
         and build a new website for their brand&nbsp;iTrust.
        </h2>
        <div className="row">
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          It was during the pandemic so all the work was done remotely. He kept
          me engaged with lots of calls. The brief was not an easy one, I had to
          do a lot of research and concepting to achieve the
          required&nbsp;result.
         </p>
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          I cannot thank them enough for giving me this opportunity, this web
          page I designed looks awesome in my&nbsp;portfolio!
         </p>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a
          className="btn btn-secondary clearfix"
          href="https://itrust.finance"
         >
          View the work
         </a>
        </div>
       </div>
       <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
        <h5 className="technoHeaderTitle">technologies used</h5>
        <div className="lineOnProject"></div>
        <div className="clearfix">
         <BootstrapIcon />
         <ReactiveIcon />
         <NativeIcon />
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img className="img-fluid mt-5" alt="itrust.finance" src={iTrustImg1} />
       </div>
      </div>

      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">2</div>
        <div className="text">
         <p>the code</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         This is a fusion of React and Material UI which is the new grid go to.
         It is in fact better than Bootstrap or the foundation grid because it
         strips all unnecessary code from the production&nbsp;files.
        </h2>
        <div className="row">
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Theory:</h3>
          <p>
           I am very keen to include animations into the work nowadays. Clients
           often want to leave this because of budget, but it really
           stands&nbsp;out.
          </p>
         </div>
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Working on it:</h3>
          <p>
           This codebase was entirely designed and built by myself. It was then
           published through Azure by the backend dev team. They work a lot with
           Solidity in the crypto sphere. DeFi is the new ICO! This is why they
           talk about the summer of DeFi. A lot of people got&nbsp;rich!
          </p>
         </div>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a
          className="btn btn-secondary clearfix"
          href="https://itrust.finance"
         >
          View the work
         </a>
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img className="img-fluid" alt="itrust.finance" src={iTrustImg2} />
       </div>
      </div>
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">3</div>
        <div className="text">
         <p>conclusion</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         Built with great care
        </h2>
        <div className="row">
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <p>
           A lot of attention to detail and time went into this&nbsp;build.
          </p>
         </div>
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <p>
           I decided to use a carousel (Flickity NPM) for the content as it
           would simplify the build of all the responsive versions of
           the&nbsp;page.
          </p>
         </div>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a
          className="btn btn-secondary clearfix"
          href="https://itrust.finance"
         >
          View the work
         </a>
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img className="img-fluid" alt="itrust.finance" src={iTrustImg3} />
       </div>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default iTrust;
