import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import CorporatePsdImg1 from '../../../../img/internals/corporate-psd/corporate-psd.jpg';

class CorporatePsd extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }
    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }
    render() {
        return (
            <div className="container-fluid">

                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>A corporate type illustration for free download.</h1>
                            <p>
                                Thank you for coming to this page, you can dowload the free web design template Photoshop file free below . This is a quick design I have knocked up for a potential client. The psd has all the layers and is nicely organized. If you are going to download all of my free web design templates, please have te decency to share me on Google+ or Facebook. After all, I am giving you free stuff. Cheers and happy designing!
                             </p>
                            <br />
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/web-design-templates/starburst/">View the work!</a>
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/free-photoshop-downloads/corporate-slick-web-template-photoshop-file-free-download.zip">Download
                    it!</a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler" src={CorporatePsdImg1} />
                            <figcaption>
                                <h2>A  <span>Corporate Slick Theme</span></h2>
                                <p>designed by Archie</p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}
export default CorporatePsd;