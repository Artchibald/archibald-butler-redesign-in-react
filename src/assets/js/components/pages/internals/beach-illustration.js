import React, { Component } from 'react';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';

import BeachIllustrationImg1 from '../../../../img/internals/beach-illustration/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction1.jpg';
import BeachIllustrationImg2 from '../../../../img/internals/beach-illustration/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction2.jpg';
import BeachIllustrationImg3 from '../../../../img/internals/beach-illustration/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction3.jpg';
import BeachIllustrationImg4 from '../../../../img/internals/beach-illustration/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction4.jpg';
//icons
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';

class BeachIllustration extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Needed to thank a friend…</h1>
              <p>
                I took a great picture of my friend and her family on the beach…
                See Emma Ridley. Or scroll down.
              </p>

              <br />
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>
              <div className="clearfix">
                <Css3Icon />
                <ReactiveIcon />
                <SeoIcon />
                <NativeIcon />
              </div>
              <div className="clearfix"></div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="Illustrator Archibald Butler"
                src={BeachIllustrationImg1}
              />
              <figcaption>
                <h2>
                  A Good Day of <span>Work</span>
                </h2>
                <p> Website illustration and animation techniques</p>

                <a
                  aria-label="See work"
                  href="https://archibaldbutler.com/projects/beach-illustrations/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction1.jpg"
                >
                  View more
                </a>
              </figcaption>
            </figure>
          </div>
        </div>
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="Illustrator Archibald Butler"
                src={BeachIllustrationImg2}
              />
              <figcaption>
                <h2>
                  Illustrations from the <span>Beach!</span>
                </h2>
                <p> Website illustration and animation techniques</p>
                <br />
                <a href="https://archibaldbutler.com/projects/beach-illustrations/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction2.jpg">
                  ...
                </a>
              </figcaption>
            </figure>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Emma Ridley: thanks sketch</h1>
              <p>
                I wanted to thank her for putting me up for 45 nights in her
                delightful house while I was travelling around the world for
                surfing.
              </p>
              <br />
            </div>
          </div>
        </div>
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>A nice person…</h1>
              <p>
                Bless her, she has been so nice to me, I am very grateful so I
                produced this piece for her in about 2 hours I think.
              </p>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="A web design illustration by Archibald Butler"
                src={BeachIllustrationImg3}
              />
              <figcaption>
                <h2>
                  Illustrations from the <span>Beach!</span>
                </h2>
                <p> Website illustration and animation techniques</p>
                <br />
                <a
                  aria-label="See work"
                  href="https://archibaldbutler.com/projects/beach-illustrations/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction3.jpg"
                >
                  ...
                </a>
              </figcaption>
            </figure>
          </div>
        </div>
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="Illustrator Archibald Butler"
                src={BeachIllustrationImg4}
              />
              <figcaption>
                <h2>
                  A Good Day of <span>Work</span>
                </h2>
                <p> Website illustration and animation techniques</p>

                <a
                  aria-label="See work"
                  href="https://archibaldbutler.com/projects/beach-illustrations/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction4.jpg"
                >
                  View more
                </a>
              </figcaption>
            </figure>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>All this work…</h1>
              <p>
                I spent about 2 hours all together on this project. Uploading to
                the blog took another hour <i className="far fa-smile-beam"></i>
              </p>

              <div className="clearfix"></div>
              <a
                aria-label="See work"
                className="btn btn-secondary"
                href="https://archibaldbutler.com/projects/beach-illustrations/fast-aquarelle-and-ink-drawing-illustration-beach-family-photo-reproduction1.jpg"
              >
                See work
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BeachIllustration;
