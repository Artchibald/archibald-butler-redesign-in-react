import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
//icons
import ApiIcon from "../../icons/api-icon.js";
import NodeIcon from "../../icons/node-icon.js";
import Css3Icon from "../../icons/css3-icon";
import SassIcon from "../../icons/sass-icon";
import CmsIcon from "../../icons/cms-icon";
// img
import iTrustTop from "../../../../img/internals/itrust/itrust-compressed.png";
import iTrustImg1 from "../../../../img/internals/itrust/showcase_1.png";
import iTrustImg2 from "../../../../img/internals/itrust/showcase_2.png";
import iTrustImg3 from "../../../../img/internals/itrust/showcase_3.png";
import iTrustRecording from "../../../../img/internals/itrust/recording.mp4";

class Ripple extends Component {
 componentDidMount() {
  //make sure we load at top of page
  window.scrollTo(0, 0);
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid portfolio">
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-6 col-lg-6">
      <div className="textbox">
       <h1 className="projHeaderTitle">Worked for crypto giant Ripple.</h1>
       <h3 className="projHeaderP">A rather top secret project.</h3>
       <br />
       <h5 className="technoHeaderTitle">technologies used</h5>
       <div className="lineOnProject"></div>

       <div className="clearfix">
        <ApiIcon />
        <NodeIcon />
        <Css3Icon />
        <SassIcon />
        <CmsIcon />
        {/*
    <AngularIcon />
    <ApiIcon />
    <BitbucketIcon />
    <BootstrapIcon />
    <CmsIcon />
    <CpanelIcon />
    <Css3Icon />
    <GifIcon />
    <GitIcon />
    <GreensockIcon />
    <GruntIcon />
    <GulpIcon />
    <HtmlIcon />
    <JqueryIcon />
    <LessIcon />
    <NativeIcon />
    <NodeIcon />
    <PhpIcon />
    <ReactiveIcon />
    <SassIcon />
    <SeoIcon />
    <SqlIcon />
    <VideoIcon />
    <WireframeIcon />
    <WordpressIcon /> */}
       </div>
       <div className="clearfix">
        <a
         className="btn btn-secondary readmoreBTN inline"
         href="https://ripple.com"
        >
         View website
        </a>

        <a href="#mainPara">
         <div className="scroll-down-dude"></div>
        </a>
        <br className="c" />
        <br className="c" />
        <br className="c" />
       </div>
      </div>
     </div>
     <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
      <img className="img-fluid" alt="ripple.com" src={iTrustTop} />
     </div>
    </div>
    <div className="video_wrapper screen_bordered container-fluid">
     <div className="row no-pad">
      <video className="w-100" autoPlay loop muted>
       <source src={iTrustRecording} type="video/mp4" />
      </video>
     </div>
    </div>
    <div className="container-fluid">
     <div className="row no-pad" id="mainPara">
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">1</div>
        <div className="text">
         <p>A free reign art project!</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         By the way, before you bother hacking me, they didn't give me any, and
         I do not hold any client data EVER!
        </h2>
        <div className="row">
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          This was an insanely sick animated art project I pulled off at Ripple!
         </p>
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          The work was absolutely dope and I am pretty sure I smashed it!
         </p>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         {/* <a
          className="btn btn-secondary clearfix"
          href="https://ripple.com"
         >
          View the work
         </a> */}
        </div>
       </div>
       <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
        <h5 className="technoHeaderTitle">technologies used</h5>
        <div className="lineOnProject"></div>
        <div className="clearfix">
         <ApiIcon />
         <NodeIcon />
         <Css3Icon />
         <SassIcon />
         <CmsIcon />
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img className="img-fluid mt-5" alt="ripple.com" src={iTrustImg1} />
       </div>
      </div>

      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">2</div>
        <div className="text">
         <p>the code</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         It was a custom web build in React, using a variety of amazing
         transitions and art.
        </h2>
        <div className="row">
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Theory:</h3>
          <p>
           I managed to tag on some pretty wild blue orb animation for them!
          </p>
         </div>
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Working on it:</h3>
          <p>
           It is such a privilege to work for such high end clients. I am very
           grateful.
          </p>
         </div>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a className="btn btn-secondary clearfix" href="https://ripple.com">
          View the work
         </a>
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img className="img-fluid" alt="ripple.com" src={iTrustImg2} />
       </div>
      </div>
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">3</div>
        <div className="text">
         <p>conclusion</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         I would never have thought I would code a project for these guys.
        </h2>
        <div className="row">
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <p>Really enjoyed this remote job at the start of Covid.</p>
         </div>
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <p>A lotta peeps got rich buying this stuff!</p>
         </div>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a className="btn btn-secondary clearfix" href="https://ripple.com">
          View the work
         </a>
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img className="img-fluid" alt="ripple.com" src={iTrustImg3} />
       </div>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default Ripple;
