import React, { Component } from 'react';
import { BrowserRouter as Route, a } from "react-router-dom";
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import GeekAwardImg1 from '../../../../img/internals/geek-award/geek-award-claimed1.jpg';
import GeekAwardImg2 from '../../../../img/internals/geek-award/geek-award-claimed2.jpg';
import GeekAwardImg3 from '../../../../img/internals/geek-award/geek-award-claimed3.jpg';
//icons
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';


class GeekAward extends Component {

    componentDidMount() {
        //make sure we load at top of page 
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Geek award claimed for best use of mobile in 2014.</h1>
                            <p>I was surprised and delighted to receive an award for coding innovation in 2014. I feel very proud that my team at ASOS received an award at The Sitecore Experiences Awards Ceremony in KOKO Camden, London. It was also nice to be on the other side of the event having worked in private events catering during my student years.</p>

                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <Css3Icon />
                                <ReactiveIcon />
                                <SeoIcon />
                                <NativeIcon />
                            </div>
                            <div className="clearfix" ></div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={GeekAwardImg1} />
                            <figcaption>
                                <h2>Best use of <span>Mobile</span></h2>
                                <p>Development and coding skills</p>

                                <a href="https://archibaldbutler.com/projects/GeekAward/">View more</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={GeekAwardImg2} />
                            <figcaption>
                                <h2>Best use of <span>Mobile</span></h2>
                                <p>Development and coding skills</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">

                            <h1>About the build</h1>
                            <p><strong>ASOS</strong><br />
                                -Category<br />
                                –Best use of mobile<br />
                                -Region<br />
                                –United Kingdom<br />
                                -Sitecore Partner<br />
                                –True Clarity<br />
                                -Customer website<br />
                                –asos.com<br />
                                <br /><br />
                                ASOS is a global online fashion and beauty retailer that sells more than 65,000 branded and own-label products to fashion-forward 20-somethings through its website, asos.com. The business delivers to 234 countries and territories from its global distribution center, in the UK. Its revenue was £975 million for its 2014 financial year.<br /><br />

                                ASOS’s nine local-language websites in the UK, US, France, Germany, Spain, Italy, Australia, Russia, and China attract 29.5 million unique visitors a month and had 14.8 million registered users at the end of 2013.</p>
                            <br />
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Conclusion</h1>
                            <p>ASOS used the Sitecore® Experience Platform™ as part of a wider solution to address this issue. Mobile pages are now hosted in Sitecore®, using a hybrid responsive/adaptive solution in which every element of the page, where possible, is built in a responsive way. Speed has increased dramatically—for example, page load time in Australia has decreased by well over one second.<br /><br />
                                Taken from http://www.sitecore.net/Customers/Experience-Awards/2014-Finalists-UK.aspx</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={GeekAwardImg3} />
                            <figcaption>
                                <h2>Best use of <span>Mobile</span></h2>
                                <p>Development and coding skills</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default GeekAward; 