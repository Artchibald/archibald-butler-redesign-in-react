import React, { Component } from 'react';
import { BrowserRouter as Route, a } from "react-router-dom";
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import AnimatedGifImg1 from '../../../../img/internals/animated-gif/animated-gif1.jpg';
import AnimatedGifImg2 from '../../../../img/internals/animated-gif/animated-gif2.jpg';
import AnimatedGifImg3 from '../../../../img/internals/animated-gif/animated-gif3.jpg';
//icons
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';


class AnimatedGif extends Component {

    componentDidMount() {
        //make sure we load at top of page 
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Animated Gifs: Do they Work in Web Design?</h1>
                            <p>Here is a little trial run at gifs I did a while back. I did this recently to try them out. I will be posting up a tutorial soon. Gifs are cool but they are very large files. Not really suitable for a big background image.</p>

                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <Css3Icon />
                                <ReactiveIcon />
                                <SeoIcon />
                                <NativeIcon />
                            </div>
                            <div className="clearfix" ></div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={AnimatedGifImg1} />
                            <figcaption>
                                <h2>Animated <span>GIFs</span></h2>
                                <p>Do they work in Web Design</p>


                                <a href="https://archibaldbutler.com/projects/AnimatedGif/">View more</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={AnimatedGifImg2} />
                            <figcaption>
                                <h2>Animated <span>GIFs</span></h2>
                                <p>Do they work in Web Design</p>

                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">

                            <h1>About the build</h1>
                            <p>Hand drawn stop frame animation takes so much time to complete!</p>
                            <br />
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Conclusion</h1>
                            <p>The animation is a bit jittery, I could have done better!</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={AnimatedGifImg3} />
                            <figcaption>
                                <h2>Animated <span>GIFs</span></h2>
                                <p>Do they work in Web Design</p>

                                <br />

                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default AnimatedGif; 