import React, { Component } from 'react';
import { BrowserRouter as Route, a } from "react-router-dom";
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import RellikImg1 from '../../../../img/internals/rellik/rellik.jpg';
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import CpanelIcon from '../../icons/cpanel-icon.js';
import SeoIcon from '../../icons/seo-icon.js';


class Rellik extends Component {

    componentDidMount() {
        //make sure we load at top of page 
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>RELLIK, A London Fashion Boutique asked Archie to do their website.</h1>
                            <p>I got put in touch with them through friends. Ultra minimalist, with some extra simplicity thrown in, no frills, no pointless graphics, just uber clean. This was the brief from Steven Philips. To start off, I threw some minimalist designs at them, but to no effect. They then sent me a wire frame, which really enabled me to discernate what the Rellik team was after:
                            </p>
                            <p>
                                Eventhough this was asked of me, I still had to thrown in something special: So I made the whole website mega responsive. Give it a try it is all there: IPad Landscape, Portrait, and Iphone P and L. I also spent numerous hours uploading all their press articles and I really like the layout of the press page. Here is the final result, It has a bit of a style of its own that’s for sure. Well the design is one hundred percent bespoke. <span aria-label="happy face" role="img">😉</span>
                            </p>
                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <Css3Icon />
                                <CpanelIcon />
                                <SeoIcon />
                                <NativeIcon />
                            </div>
                            <div className="clearfix" ></div>
                            <a className="btn btn-secondary"
                                href="https://relliklondon.co.uk">View the composition</a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={RellikImg1} />
                            <figcaption>
                                <h2>Fashion with  <span>Rellik</span></h2>
                                <p>developed by Archie</p>
                                <a href="https://relliklondon.co.uk">View more</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default Rellik; 