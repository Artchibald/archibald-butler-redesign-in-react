import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
//icons
import ApiIcon from "../../icons/api-icon.js";
import NodeIcon from "../../icons/node-icon.js";
import Css3Icon from "../../icons/css3-icon";
import SassIcon from "../../icons/sass-icon";
import CmsIcon from "../../icons/cms-icon";
// img

import golfJunkiesHero from "../../../.../../../img/internals/golfjunkies/golf-junkies-hero.png";
import golfJunkiesVideo from "../../../.../../../img/internals/golfjunkies/golf_junkies_video.mp4";
import golfJunkiesArt1 from "../../../.../../../img/internals/golfjunkies/golf_junkies_art_1.png";
import golfJunkiesArt2 from "../../../.../../../img/internals/golfjunkies/golf_junkies_art_2.png";
import golfJunkiesArt3 from "../../../.../../../img/internals/golfjunkies/golf_junkies_art_3.png";



class GolfJunkies extends Component {
    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid portfolio">
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">The Golf Junkies NFT!</h1>
                            <h3 className="projHeaderP">A partnership for NFT utility!</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <ApiIcon />
                                <NodeIcon />
                                <Css3Icon />
                                <SassIcon />
                                <CmsIcon />
                                {/*
    <AngularIcon />
    <ApiIcon />
    <BitbucketIcon />
    <BootstrapIcon />
    <CmsIcon />
    <CpanelIcon />
    <Css3Icon />
    <GifIcon />
    <GitIcon />
    <GreensockIcon />
    <GruntIcon />
    <GulpIcon />
    <HtmlIcon />
    <JqueryIcon />
    <LessIcon />
    <NativeIcon />
    <NodeIcon />
    <PhpIcon />
    <ReactiveIcon />
    <SassIcon />
    <SeoIcon />
    <SqlIcon />
    <VideoIcon />
    <WireframeIcon />
    <WordpressIcon /> */}
                            </div>
                            <div className="clearfix">
                                <a
                                    className="btn btn-secondary readmoreBTN inline"
                                    href="https://golfjunkies.com"
                                >
                                    View website
                                </a>

                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="golfjunkies.com" src={golfJunkiesHero} />
                    </div>
                </div>
                <div className="video_wrapper screen_bordered container-fluid">
                    <div className="row no-pad">
                        <video className="w-100" autoPlay loop muted>
                            <source src={golfJunkiesVideo} type="video/mp4" />
                        </video>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">1</div>
                                <div className="text">
                                    <p>A free reign art project!</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                                    Tried to tone down the color palette on this one. We sold loads too!
                                </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        It is important to not create rainbow art. Really enjoyed using my
                                        Apple pen for this work as well on this project.
                                    </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        We were trying to create an NFT with a Golf membership attached to it.
                                        We semi succeeded. Join our Discord!
                                    </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    {/* <a
          className="btn btn-secondary clearfix"
          href="https://golfjunkies.com"
         >
          View the work
         </a> */}
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <ApiIcon />
                                    <NodeIcon />
                                    <Css3Icon />
                                    <SassIcon />
                                    <CmsIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img
                                    className="img-fluid mt-5"
                                    alt="golfjunkies.com"
                                    src={golfJunkiesArt1}
                                />
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">2</div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                                    I used Bootstrap, Node, Css3 animation and Flickity NPM.
                                </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Theory:</h3>
                                        <p>
                                            Really enjoyed creating the art for all this, I generated 10,000
                                            unique NFTs!
                                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Working on it:</h3>
                                        <p>
                                            I love working on this type of art, and want to send a big shout out
                                            to Rob Cooke for hooking me up!
                                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a
                                        className="btn btn-secondary clearfix"
                                        href="https://golfjunkies.com"
                                    >
                                        View the work
                                    </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid" alt="golfjunkies.com" src={golfJunkiesArt2} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">3</div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                                    NFTs are sick!
                                </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>Wish we had hoped on this train a bit earlier, NFTs are sick.</p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>A lotta peeps got rich doing this stuff!</p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a
                                        className="btn btn-secondary clearfix"
                                        href="https://golfjunkies.com"
                                    >
                                        View the work
                                    </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid" alt="golfjunkies.com" src={golfJunkiesArt3} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default GolfJunkies;
