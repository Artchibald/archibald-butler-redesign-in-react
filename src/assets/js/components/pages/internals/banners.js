import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
//icons
import ApiIcon from "../../icons/api-icon.js";
import NodeIcon from "../../icons/node-icon.js";
import Css3Icon from "../../icons/css3-icon";
import GreensockIcon from "../../icons/greensock-icon";
import SassIcon from "../../icons/sass-icon";
// img
// import iTrustTop from "../../../../img/internals/itrust/itrust-compressed.png";
import TopImg from "../../../../img/internals/banners/top.png";

class Banners extends Component {
    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid portfolio">
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">Banner Hell!</h1>
                            <h3 className="projHeaderP">Get your screwed up banners here!</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <ApiIcon />
                                <NodeIcon />
                                <Css3Icon />
                                <GreensockIcon />
                                <SassIcon />
                                {/*
    <AngularIcon />
    <ApiIcon />
    <BitbucketIcon />
    <BootstrapIcon />
    <CmsIcon />
    <CpanelIcon />
    <Css3Icon />
    <GifIcon />
    <GitIcon />
    <GreensockIcon />
    <GruntIcon />
    <GulpIcon />
    <HtmlIcon />
    <JqueryIcon />
    <LessIcon />
    <NativeIcon />
    <NodeIcon />
    <PhpIcon />
    <ReactiveIcon />
    <SassIcon />
    <SeoIcon />
    <SqlIcon />
    <VideoIcon />
    <WireframeIcon />
    <WordpressIcon /> */}
                            </div>
                            <div className="clearfix">
                                {/* <a
         className="btn btn-secondary readmoreBTN inline"
         href="https://itrust.finance"
        >
         View website
        </a> */}

                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="itrust.finance" src={TopImg} />
                    </div>
                </div>
                <div className="container">
                    <div className="row no-pad">
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-1/120x600/index.html"
                            width="120px"
                            height="600px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <br className="clear" />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-1/160x600/index.html"
                            width="160px"
                            height="600px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-1/300x250/index.html"
                            width="300px"
                            height="250px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-1/300x50/index.html"
                            width="300px"
                            height="50px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-1/300x600/index.html"
                            width="300px"
                            height="600px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-1/320x50/index.html"
                            width="320px"
                            height="50px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-1/728x90/index.html"
                            width="728px"
                            height="90px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-1/970x250/index.html"
                            width="970px"
                            height="250px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-2-get-ahead/120x600/index.html"
                            width="120px"
                            height="600px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-2-get-ahead/160x600/index.html"
                            width="160px"
                            height="600px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-2-get-ahead/300x250/index.html"
                            width="300px"
                            height="250px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-2-get-ahead/300x50/index.html"
                            width="300px"
                            height="50px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-2-get-ahead/300x600/index.html"
                            width="300px"
                            height="600px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-2-get-ahead/320x50/index.html"
                            width="320px"
                            height="50px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-2-get-ahead/728x90/index.html"
                            width="728px"
                            height="90px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe

                            src="https://archibaldbutler.com/projects/all-banners/version-2-get-ahead/970x250/index.html"
                            width="970px"
                            height="250px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                    </div >
                </div >
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">1</div>
                                <div className="text">
                                    <p>Animated banners made in Javascript.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                            <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                                Using good old Greensock and css animation, I made some more banners
                                for at least the 5th time on a project.
                            </h2>
                            <div className="row">
                                <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    They are awesome to create and satisfying getting signed off at all
                                    the different sizes.
                                </p>
                                <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                    Technologies include Greensock js and animated scss.
                                </p>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                {/* <a
          className="btn btn-secondary clearfix"
          href="https://itrust.finance"
         >
          View the work
         </a> */}
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <ApiIcon />
                                <NodeIcon />
                                <Css3Icon />
                                <GreensockIcon />
                                <SassIcon />
                            </div>
                        </div>
                    </div>
                </div>
                {/* <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Sonia_Hitzelberger/Sonia_Hetzelberger_728x90/index.html"
                                width="728px"
                                height="90px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Sonia_Hitzelberger/Sonia_Hetzelberger_160x600/index.html"
                                width="160px"
                                height="600px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Sonia_Hitzelberger/Sonia_Hetzelberger_300x250/index.html"
                                width="300px"
                                height="250px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Melanie_Welsh/Melanie_Welsh_160x600/index.html"
                                width="160px"
                                height="600px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Melanie_Welsh/Melanie_Welsh_300x250/index.html"
                                width="300px"
                                height="250px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Melanie_Welsh/Melanie_Welsh_728x90/index.html"
                                width="728px"
                                height="90px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Helen_Sawyer/Helen_Sawyer_728x90/index.html"
                                width="728px"
                                height="90px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Helen_Sawyer/Helen_Sawyer_160x600/index.html"
                                width="160px"
                                height="600px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Helen_Sawyer/Helen_Sawyer_300x250/index.html"
                                width="300px"
                                height="250px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Gavin_Wheeler/Gavin_Wheeler_300x250/index.html"
                                width="300px"
                                height="250px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Gavin_Wheeler/Gavin_Wheeler-160x600/index.html"
                                width="160px"
                                height="600px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                            <br />
                            <iframe

                                src="https://archibaldbutler.com/projects/all-banners/Gavin_Wheeler/Gavin_Wheeler_728x90/index.html"
                                width="728px"
                                height="90px"
                                loading="lazy"
                                title="ScrewFix banner"
                            ></iframe>
                        </div>
                    </div>
                </div> */}
                <div className="container">
                    <div className="row no-pad">
                        <iframe
                            src="https://archibaldbutler.com/projects/all-banners/version-3-timing-is-everything/120x600/index.html"
                            width="120px"
                            height="600px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe
                            src="https://archibaldbutler.com/projects/all-banners/version-3-timing-is-everything/160x600/index.html"
                            width="160px"
                            height="600px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                        <iframe
                            src="https://archibaldbutler.com/projects/all-banners/version-3-timing-is-everything/300x250/index.html"
                            width="300px"
                            height="250px"
                            loading="lazy"
                            title="ScrewFix banner"
                        ></iframe>
                        <br />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">2</div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                                    Banner code envolves a node environment that duplicates certain aspects
                                    of the banners to all the relevant sizes.
                                </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Theory:</h3>
                                        <p>
                                            You end up moving and shifting stuff across the coordinate system,
                                            usually pixel based.
                                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Working on it:</h3>
                                        <p>
                                            I love web animation, and now as write this, I have accumulated over
                                            15 years of professional Javascript experience!
                                        </p>
                                    </div>
                                </div>
                                {/* <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a
          className="btn btn-secondary clearfix"
          href="https://itrust.finance"
         >
          View the work
         </a>
        </div> */}
                            </div>
                            <div className="container">
                                <div className="row no-pad">

                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-3-timing-is-everything/300x50/index.html"
                                        width="300px"
                                        height="50px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-3-timing-is-everything/300x600/index.html"
                                        width="300px"
                                        height="600px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-3-timing-is-everything/320x50/index.html"
                                        width="320px"
                                        height="50px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-3-timing-is-everything/728x90/index.html"
                                        width="728px"
                                        height="90px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-3-timing-is-everything/970x250/index.html"
                                        width="970px"
                                        height="250px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-4-no-limits/120x600/index.html"
                                        width="120px"
                                        height="600px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-4-no-limits/160x600/index.html"
                                        width="160px"
                                        height="600px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-4-no-limits/300x250/index.html"
                                        width="300px"
                                        height="250px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-4-no-limits/300x50/index.html"
                                        width="300px"
                                        height="50px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-4-no-limits/300x600/index.html"
                                        width="300px"
                                        height="600px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-4-no-limits/320x50/index.html"
                                        width="320px"
                                        height="50px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-4-no-limits/728x90/index.html"
                                        width="728px"
                                        height="90px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                    <iframe

                                        src="https://archibaldbutler.com/projects/all-banners/version-4-no-limits/970x250/index.html"
                                        width="970px"
                                        height="250px"
                                        loading="lazy"
                                        title="ScrewFix banner"
                                    ></iframe>
                                    <br />
                                </div >
                            </div >
                        </div >
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">3</div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                                    Banners are sick!
                                </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            As I joke with my mum: "You know all those annoying banner ads you
                                            find on the internet?" ... "I MAKE THOSE!" XD
                                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            Super grateful that I have achieved what I wanted to work on in my
                                            career. Illustration and animation!
                                        </p>
                                    </div>
                                </div>
                                {/* <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a
          className="btn btn-secondary clearfix"
          href="https://itrust.finance"
         >
          View the work
         </a>
        </div> */}
                            </div>
                        </div>
                    </div >
                </div >
            </div >
        );
    }
}

export default Banners;
