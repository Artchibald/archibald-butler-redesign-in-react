import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';
//icons
import BootstrapIcon from '../../icons/bootstrap-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import NativeIcon from '../../icons/native-icon.js';

// img
import GucciGigImg1 from '../../../../img/internals/gucci-gig/gucci-gig-gif.gif';
import GucciGigImg2 from '../../../../img/internals/gucci-gig/portfolio1.jpg';
import GucciGigImg3 from '../../../../img/internals/gucci-gig/portfolio2.jpg';

class GucciGig extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }



    render() {
        return (
            <div className="container-fluid portfolio">
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">GucciGig: Freelancing in&nbsp;London.</h1>
                            <h3 className="projHeaderP">Two months of React&nbsp;rowdyness.</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <BootstrapIcon />
                                <ReactiveIcon />
                                <NativeIcon />                            </div>
                            <div className="clearfix">
                                <a className="btn btn-secondary readmoreBTN" href="https://archibaldbutler.com/projects/working-at-dazed/#/">
                                    View the work
                    </a>
                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={GucciGigImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>FREELANCING AT DAZED &amp;&nbsp;CONFUSED</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">I had the absolute pleasure of meeting the lovely team at DAZED&nbsp;MAGAZINE.
                    </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        It was a very fresh excentric mix of bohemian eclectics. They were having fun there. It was a really fresh&nbsp;vibe.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        I cannot thank them enough for giving me this opportunity, this looks awesome in my&nbsp;portoflio!
                        </p>

                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://archibaldbutler.com/projects/working-at-dazed/#/">
                                        View the work
                        </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <BootstrapIcon />
                                    <ReactiveIcon />
                                    <NativeIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid mt-5" alt="Kinesis Money: Archibald Butler Web Development" src={GucciGigImg2} />
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">About this GucciGig web&nbsp;project
                    </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Theory:</h3>
                                        <p>
                                            I am very keen to include animations into the work nowadays. Clients often want to leave this because of budget, but it really stands&nbsp;out.
                        </p>

                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Working on it:</h3>
                                        <p>
                                            This is a React JS environment using componentized elements and Vanilla JS. I used some node module addons for animation&nbsp;too.
                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://archibaldbutler.com/projects/working-at-dazed/#/">
                                        View the work
                        </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={GucciGigImg3} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Advanced React&nbsp;JS</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            I had deployment issues with this app on their server due to redirect settings. It seems React doesn't like to be in a sub folder in any old environment it prefers the root with default&nbsp;settings. :)
                        </p>

                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            I have really enjoyed moving the work to React. It is more time consuming but definitely more maintainable and re-usable which is another one of those buzzwords you hear&nbsp;today.
                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://archibaldbutler.com/projects/working-at-dazed/#/">
                                        View the work
                        </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default GucciGig; 