import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';
//icons
import AngularIcon from '../../../../js/components/icons/angular-icon.js';
import ApiIcon from '../../../../js/components/icons/api-icon.js';
import BitbucketIcon from '../../../../js/components/icons/bitbucket-icon.js';
import BootstrapIcon from '../../../../js/components/icons/bootstrap-icon.js';
import CmsIcon from '../../../../js/components/icons/cms-icon.js';
import CpanelIcon from '../../../../js/components/icons/cpanel-icon.js';
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import GifIcon from '../../../../js/components/icons/gif-icon.js';
import GitIcon from '../../../../js/components/icons/git-icon.js';
import GreensockIcon from '../../../../js/components/icons/greensock-icon.js';
import GruntIcon from '../../../../js/components/icons/grunt-icon.js';
import GulpIcon from '../../../../js/components/icons/gulp-icon.js';
import HtmlIcon from '../../../../js/components/icons/html-icon.js';
import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
import LessIcon from '../../../../js/components/icons/less-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import NodeIcon from '../../../../js/components/icons/node-icon.js';
import PhpIcon from '../../../../js/components/icons/php-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SassIcon from '../../icons/sass-icon.js';
import SeoIcon from '../../icons/seo-icon.js';
import SqlIcon from '../../icons/sql-icon.js';
import VideoIcon from '../../icons/video-icon.js';
import WireframeIcon from '../../icons/wireframe-icon.js';
import WordpressIcon from '../../icons/wordpress-icon.js';
//img
import iconSketches from '../../../../img/icons/icon-sketches.jpg';


class freeIcons extends Component {

    componentDidMount() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container free-icons">
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-10  offset-md-1 col-lg-10  offset-lg-1">       
                            <h1 className="mt-5 mb-5">Free illustrated icons about web development, drawn and coloured by hand.</h1>
                    </div>
                    <div className="col-12 col-sm-6 col-md-5 offset-md-1 col-lg-5 offset-lg-1">
                                <p><strong>Here are some free illustrated icons that I have created in my spare time.</strong> I tore off a couple of pages from my beloved A3 hardback sketch book and threw some shapes down in ink using what came to mind first. <br /><br />It’s nice to just draw without even thinking about it sometimes. Then a quick crayon scrub. I never seem to remember to try and stick to a colour palette. I get too eager!
                                
                                It took me about two hours to draw the <a href="https://archibaldbutler.com/news/">illustrations</a> and colour them with crayons. I didn’t use any pencil for drafting I went straight to the inking. 
                                
                                I knew that the actual drawn sizes of the images would be greater than their preview on screen even if I drew them small. It’s great combining the knowledge of code and the childhood creativity. Really enjoying the time off work.
                                </p>
                            </div>
                            <div className="col-12 col-sm-6 col-md-6 col-lg-5">
                                    <p>
                                        After adding as much light as possible to the desk area of my room, I took photographs with flash of the illustrations. It was night time.<br /><br />

                                            These were taken on iPhone 6. Afterwards the editing work and the coding work took altogether about 4 to 6 extra hours. The scripts for the CSS transition I had already written for the footer of this website. So I just re-used the CSS animations with these new images.
                                           
                                           As for the code, I also wrapped them in a Bootstrap tooltip for the hover text.
                                           
                                           I am satisfied by how quick they were to create and code, but the actual blog post writing creating a new template for this page, editing, writing 300 words, fixing bugs and alignments is where all the spare time goes.
                                                </p>
                                                <p>
                                                It’s funny how it takes more time to document the work than actually doing it.
                                                </p>
                                                </div>
                                                <div className="col-12 col-sm-12 col-md-10  offset-md-1 col-lg-10  offset-lg-1 mt-5 mb-5"> 
                                                    <a href="https://archibaldbutler.com/web-design-templates/icons-for-download.zip"  className="btn btn-secondary" onClick="window.location.href='https://archibaldbutler.com/web-design-templates/icons-for-download.zip';ga('send', 'event', 'icons page','icons','icons');">Download these icons</a></div>

                                                    <div className="col-12 col-sm-12 col-md-10  offset-md-1 col-lg-10  offset-lg-1 mt-5 mb-5">
                                                    <AngularIcon />
                                                    <ApiIcon />
                                                    <BitbucketIcon />
                                                    <BootstrapIcon />
                                                    <CmsIcon />
                                                    <CpanelIcon />
                                                    <Css3Icon />
                                                    <GifIcon />
                                                    <GitIcon />
                                                    <GreensockIcon />
                                                    <GruntIcon />
                                                    <GulpIcon />
                                                    <HtmlIcon />
                                                    <JqueryIcon />
                                                    <LessIcon />
                                                    <NativeIcon />
                                                    <NodeIcon />
                                                    <PhpIcon />
                                                    <ReactiveIcon />
                                                    <SassIcon />
                                                    <SeoIcon />
                                                    <SqlIcon />
                                                    <VideoIcon />
                                                    <WireframeIcon />
                                                    <WordpressIcon />
                                                   </div>
                                                    <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                                                        <img className="mt-5 mb-5 img-fluid" alt="The illustrator Archibald Butler" src={iconSketches}/>
                                                        </div>
                                                        </div>
                                                 </div>
                                                        );
                                                    }
                                                }
export default freeIcons; 