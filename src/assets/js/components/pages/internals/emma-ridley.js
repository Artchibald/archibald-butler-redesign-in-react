import React, { Component } from 'react';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';

import EmmaRidleyImg1 from '../../../../img/internals/emma-ridley/emma-ridley1.jpg';
import EmmaRidleyImg2 from '../../../../img/internals/emma-ridley/emma-ridley2.jpg';
import EmmaRidleyImg3 from '../../../../img/internals/emma-ridley/emma-ridley3.jpg';
import EmmaRidleyImg4 from '../../../../img/internals/emma-ridley/emma-ridley4.jpg';
//icons
import HtmlIcon from '../../../../js/components/icons/html-icon.js';
import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
import WireframeIcon from '../../icons/wireframe-icon.js';
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
// import AngularIcon from '../../../../js/components/icons/angular-icon.js';
// import ApiIcon from '../../../../js/components/icons/api-icon.js';
// import BitbucketIcon from '../../../../js/components/icons/bitbucket-icon.js';
// import BootstrapIcon from '../../../../js/components/icons/bootstrap-icon.js';
// import CmsIcon from '../../../../js/components/icons/cms-icon.js';
// import CpanelIcon from '../../../../js/components/icons/cpanel-icon.js';
// import Css3Icon from '../../../../js/components/icons/css3-icon.js';
// import GifIcon from '../../../../js/components/icons/gif-icon.js';
// import GitIcon from '../../../../js/components/icons/git-icon.js';
// import GreensockIcon from '../../../../js/components/icons/greensock-icon.js';
// import GruntIcon from '../../../../js/components/icons/grunt-icon.js';
// import GulpIcon from '../../../../js/components/icons/gulp-icon.js';
// import HtmlIcon from '../../../../js/components/icons/html-icon.js';
// import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
// import LessIcon from '../../../../js/components/icons/less-icon.js';
// import NativeIcon from '../../../../js/components/icons/native-icon.js';
// import NodeIcon from '../../../../js/components/icons/node-icon.js';
// import PhpIcon from '../../../../js/components/icons/php-icon.js';
// import ReactiveIcon from '../../icons/reactive-icon.js';
// import SassIcon from '../../icons/sass-icon.js';
// import SeoIcon from '../../icons/seo-icon.js';
// import SqlIcon from '../../icons/sql-icon.js';
// import VideoIcon from '../../icons/video-icon.js';
// import WireframeIcon from '../../icons/wireframe-icon.js';
// import WordpressIcon from '../../icons/wordpress-icon.js';
class EmmaRidley extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid portfolio">
        {/*                                 <AngularIcon />
                                <ApiIcon />
                                <BitbucketIcon />
                                <BootstrapIcon />
                                <CmsIcon />
                                <CpanelIcon />
                                <Css3Icon />
                                <GifIcon />
                                <GitIcon />
                                <GreensockIcon />
                                <GruntIcon />
                                <GulpIcon />
                                <HtmlIcon />
                                <JqueryIcon />
                                <LessIcon />
                                <NativeIcon />
                                <NodeIcon />
                                <PhpIcon />
                                <ReactiveIcon />
                                <SassIcon />
                                <SeoIcon />
                                <SqlIcon />
                                <VideoIcon />
                                <WireframeIcon />
                                <WordpressIcon /> */}
        <div className="row no-pad">
          <div className="col-12 col-sm-12 col-md-6 col-lg-6">
            <div className="textbox">
              <h1 className="projHeaderTitle">WORKING FOR EMMA RIDLEY</h1>
              <h3 className="projHeaderP">
                How to bring out your feminine side…
              </h3>
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>
              <div className="clearfix">
                <HtmlIcon />
                <JqueryIcon />
                <WireframeIcon />
                <Css3Icon />
              </div>
              <div className="clearfix mt-5">
                <a
                  aria-label="See work"
                  className="btn btn-secondary readmoreBTN d-inline"
                  href="https://archibaldbutler.com/projects/emma/index.html"
                >
                  View the work!
                </a>
                <a
                  aria-label="See work"
                  className="btn btn-secondary readmoreBTN d-inline"
                  href="https://archibaldbutler.com/free-photoshop-downloads/burlesque-website-template.zip"
                >
                  Download it!
                </a>
                <a aria-label="See work" href="#mainPara">
                  <div className="scroll-down-dude"></div>
                </a>
                <br className="c" />
                <br className="c" />
                <br className="c" />
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
            <img
              className="img-fluid"
              alt="Kinesis Money: Archibald Butler Web Development"
              src={EmmaRidleyImg1}
            />
          </div>
        </div>
        <div className="container-fluid">
          <div className="row no-pad" id="mainPara">
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">1</div>
                <div className="text">
                  <p>THE EMMA RIDLEY BRIEF</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                <h2>Working as a freelancer for Emma Ridley</h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    Emma Ridley work: A project I designed and developed using
                    gifs, javascript, plus some custom Photoshop work.
                    <br className="c" />
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    This has been a lot of fun to create. It has taken 30 hours
                    to cut out elements and code the paged template from start
                    to finish.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5 mt-5">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary readmoreBTN d-inline"
                    href="https://archibaldbutler.com/projects/emma/index.html"
                  >
                    View the work!
                  </a>
                  <a
                    aria-label="See work"
                    className="btn btn-secondary readmoreBTN d-inline"
                    href="https://archibaldbutler.com/free-photoshop-downloads/burlesque-website-template.zip"
                  >
                    Download it!
                  </a>
                </div>
              </div>
              <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                <h5 className="technoHeaderTitle">technologies used</h5>
                <div className="lineOnProject"></div>
                <div className="clearfix">
                  <HtmlIcon />
                  <JqueryIcon />
                  <WireframeIcon />
                  <Css3Icon />
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="paddedPortfolioImg img-fluid"
                  alt="Earthgang illustrations and animations in React JS"
                  src={EmmaRidleyImg2}
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">2</div>
                <div className="text">
                  <p>the code</p>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 mt-5">
                  <h2>Emma Ridley: About this project</h2>
                </div>
                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                  <p>
                    As you can see I started with a sketch with the client, then
                    the Photoshop build, and finally the cut up and integration
                    into html, css and javascript: So the first step was the
                    sketch that is down below. Wire framing your idea is great
                    to prepare a project. Then I proceeded to look online in
                    Google images for snippets of pictures that I could cut up
                    and use in my design.
                  </p>
                  <p>
                    Be sure to check the copyright of images before use. I then
                    edited these images using human saturation and editing the
                    levels in the dark and shadow in order to make the peace
                    elements come together under the same tone.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                  <p>
                    The editing photo shop stage is not the longest though. Once
                    you have your design affected you will need to cut up each
                    layer has individual images in order to link them in your
                    HTML document. The first step took 10 hours the second step
                    took 20 hours. Animated Gifs are great but you must get used
                    to the Photoshop preparation techniques in order to create
                    work like this piece. Emma Ridley giving me free reign was
                    an absolute delight I must say.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12  clearfix no-pad mb-5 mt-5 ml-2">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary readmoreBTN d-inline"
                    href="https://archibaldbutler.com/projects/emma/index.html"
                  >
                    View the work!
                  </a>
                  <a
                    aria-label="See work"
                    className="btn btn-secondary readmoreBTN d-inline"
                    href="https://archibaldbutler.com/free-photoshop-downloads/burlesque-website-template.zip"
                  >
                    Download it!
                  </a>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
              <img
                className="paddedPortfolioImg img-fluid"
                alt="Earthgang illustrations and animations in React JS"
                src={EmmaRidleyImg3}
              />
            </div>
          </div>
          <div className="container-fluid">
            <div className="row">
              <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                <div className="number" id="case-section-1">
                  <div className="ring">3</div>
                  <div className="text">
                    <p>conclusion</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 mt-5">
                <h2>
                  It’s fun to get free reign from Emma Ridley: About this
                  project
                </h2>
              </div>
              <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                <p>
                  Her brief was wild: Glitter, Hearts, Unicorns, Velvet,
                  Sparkles, Cabaret, Burlesque and a lot more.
                </p>
              </div>
              <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                <p>
                  I went wild on this one :). I managed to get this together.
                  Below is the orginal sketch I did for Emma Ridley:
                </p>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="paddedPortfolioImg img-fluid"
                  alt="Earthgang illustrations and animations in React JS"
                  src={EmmaRidleyImg4}
                />
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12  clearfix no-pad mb-5 mt-5 ml-2">
                <a
                  aria-label="See work"
                  className="btn btn-secondary readmoreBTN d-inline"
                  href="https://archibaldbutler.com/projects/emma/index.html"
                >
                  View the work!
                </a>
                <a
                  aria-label="See work"
                  className="btn btn-secondary readmoreBTN d-inline"
                  href="https://archibaldbutler.com/free-photoshop-downloads/burlesque-website-template.zip"
                >
                  Download it!
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EmmaRidley;
