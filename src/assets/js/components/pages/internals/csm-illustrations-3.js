import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";

import csmImg61 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-61.jpg";
import csmImg62 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-62.jpg";
import csmImg63 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-63.jpg";
import csmImg64 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-64.jpg";
import csmImg65 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-65.jpg";
import csmImg66 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-66.jpg";
import csmImg67 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-67.jpg";
import csmImg68 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-68.jpg";
import csmImg69 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-69.jpg";
import csmImg70 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-70.jpg";
import csmImg71 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-71.jpg";
import csmImg72 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-72.jpg";
import csmImg73 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-73.jpg";
import csmImg74 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-74.jpg";
import csmImg75 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-75.jpg";
import csmImg76 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-76.jpg";
import csmImg77 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-77.jpg";
import csmImg78 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-78.jpg";
import csmImg79 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-79.jpg";
import csmImg80 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-80.jpg";
import csmImg81 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-81.jpg";
import csmImg82 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-82.jpg";
import csmImg83 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-83.jpg";
import csmImg84 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-84.jpg";
import csmImg85 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-85.jpg";
import csmImg86 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-86.jpg";
import csmImg87 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-87.jpg";
import csmImg88 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-88.jpg";
import csmImg89 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-89.jpg";
import csmImg90 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-90.jpg";
import chevronLeftImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class csmArtwork3 extends Component {
 componentDidMount() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid">
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-12 col-lg-12">
      <h1 className="text-center mt-5">Damn boy. Your mind is twisted!</h1>
      <p className="text-center mb-5">Such a high amount of gobbledigook.</p>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg90}
       />
       <figcaption>
        <h2>
         Oh great. <span>Evil Screams’ baby.</span>
        </h2>
        <p>Just when I thought I’d seen it all.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-90.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg61}
       />
       <figcaption>
        <h2>
         Stevie <span>Wonder.</span>
        </h2>
        <p>Saw him live once, had goosebumps.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-61.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg62}
       />
       <figcaption>
        <h2>
         Reading up on <span>anatomy.</span>
        </h2>
        <p>Trying to get proportions right.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-62.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg63}
       />
       <figcaption>
        <h2>
         The gate to <span>the ocean.</span>
        </h2>
        <p>A photograph: drawn and splodged on.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-63.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg64}
       />
       <figcaption>
        <h2>
         Cyborgs getting <span>freaky.</span>
        </h2>
        <p>How did I even get my degree?</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-64.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg65}
       />
       <figcaption>
        <h2>
         Forging psychedelic <span>plants.</span>
        </h2>
        <p>That’s what I was doing during classes. Take that teacher.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-65.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg66}
       />
       <figcaption>
        <h2>
         OMG, <span>Doggy style mmonkeys.</span>
        </h2>
        <p>Should I even be uploading this?</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-66.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg67}
       />
       <figcaption>
        <h2>
         Oh look, <span>monkeys addicted to drugs.</span>
        </h2>
        <p>This must have really impressed my teachers.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-67.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg68}
       />
       <figcaption>
        <h2>
         Now they are giving us <span>the finger. Great.</span>
        </h2>
        <p>How extreme of me.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-68.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg69}
       />
       <figcaption>
        <h2>
         Symmetrical skaters bro, <span>How rad.</span>
        </h2>
        <p>Maybe it’s a mirror. Whhhooaaaa…</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-69.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg70}
       />
       <figcaption>
        <h2>
         An art teacher said <span>this one was cool.</span>
        </h2>
        <p>She was silent throughout the rest.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-70.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg71}
       />
       <figcaption>
        <h2>
         Guilty conscience <span></span>
        </h2>
        <p>That’s what this looks like.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-71.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg72}
       />
       <figcaption>
        <h2>
         A skater who thinks he is a <span>Tsar?</span>
        </h2>
        <p>This randomness is astounding.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-72.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg73}
       />
       <figcaption>
        <h2>
         Oh and who is this <span>Geeky hero?</span>
        </h2>
        <p>I know! This was inspired from a song from Fingathing.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-73.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg74}
       />
       <figcaption>
        <h2>
         Some metal gear solid <span>type dude.</span>
        </h2>
        <p>This is what snowflakes draw.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-74.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg75}
       />
       <figcaption>
        <h2>
         The Simpsons. How original. <span>Not.</span>
        </h2>
        <p>What a waste of my daddy’s money.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-75.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg76}
       />
       <figcaption>
        <h2>
         Oh and here the inspired by <span> the Simpsons Collection.</span>
        </h2>
        <p>This deserves to go in Tate Modern at the least.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-76.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg77}
       />
       <figcaption>
        <h2>
         Look: I researched <span>Tribal dances.</span>
        </h2>
        <p>I should have done an MA instead of a BA in “pissing around”.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-77.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg78}
       />
       <figcaption>
        <h2>Now we look at the microcosme.</h2>
        <p>This will really impress my rifa puffing art teacher.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-78.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg79}
       />
       <figcaption>
        <h2>I drew a caterpillar in a museum.</h2>
        <p>Now I have a £25,000.00 student debt. Awesome.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-79.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-12 col-md-12 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg80}
       />
       <figcaption>
        <h2>
         Anxious teen tries to draw <span>Horny.</span>
        </h2>
        <p>Best to get a real job mate.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-80.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg81}
       />
       <figcaption>
        <h2>Meaningless patterns</h2>
        <p>Papa is gonna be so proud.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-81.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg82}
       />
       <figcaption>
        <h2>
         Another <span>auto portrait.</span>
        </h2>
        <p>Probably because I am from the selfie generation.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-82.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg83}
       />
       <figcaption>
        <h2>
         Look a beautiful <span>Lamborghini.</span>
        </h2>
        <p>I have a 25g student debt and sketchbooks. Awesome.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-83.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg84}
       />
       <figcaption>
        <h2>
         Drawing in <span>Camden.</span>
        </h2>
        <p>While I aimlessly walk around with no money.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-84.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg85}
       />
       <figcaption>
        <h2>
         Look <span>Mummy!</span>
        </h2>
        <p>I drew a perv trans cat!</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-85.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg86}
       />
       <figcaption>
        <h2>
         Dark <span>Druggy Stuff.</span>
        </h2>
        <p>BORING..</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-86.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg87}
       />
       <figcaption>
        <h2>
         This is really <span>in your face.</span>
        </h2>
        <p>That’s all it has got going for it</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-87.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg88}
       />
       <figcaption>
        <h2>
         My parents must be <span>cringing.</span>
        </h2>
        <p>“This imbecile is what we created ???”</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-88.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg89}
       />
       <figcaption>
        <h2>
         A bit of torture <span>to round it up.</span>
        </h2>
        <p>Bravo sir, just bravo.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-89.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-1 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <a className="previous-link" href="/csm-artwork-2">
        <div className="animate1">
         <img
          className="img-fluid"
          src={chevronLeftImg}
          alt="Chevron left: Previous page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
     <div className="offset-col-10 col-1 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <a className="next-link" href="/csm-artwork-4">
        <div className="animate2">
         <img
          className="img-fluid"
          src={chevronRightImg}
          alt="Chevron right: Next page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default csmArtwork3;
