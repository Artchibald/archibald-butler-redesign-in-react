import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import BusinessCardsImg1 from '../../../../img/internals/business-cards/business-cards1.jpg';
import JqueryIcon from '../../icons/jquery-icon.js';
import BitbucketIcon from '../../icons/bitbucket-icon.js';
import Css3Icon from '../../icons/css3-icon.js';
import GulpIcon from '../../icons/gulp-icon.js';
// import AngularIcon from '../../../../js/components/icons/angular-icon.js';
// import ApiIcon from '../../../../js/components/icons/api-icon.js';
// import BitbucketIcon from '../../../../js/components/icons/bitbucket-icon.js';
// import BootstrapIcon from '../../../../js/components/icons/bootstrap-icon.js';
// import CmsIcon from '../../../../js/components/icons/cms-icon.js';
// import CpanelIcon from '../../../../js/components/icons/cpanel-icon.js'; 
// import Css3Icon from '../../../../js/components/icons/css3-icon.js';
// import GifIcon from '../../../../js/components/icons/gif-icon.js';
// import GitIcon from '../../../../js/components/icons/git-icon.js';
// import GreensockIcon from '../../../../js/components/icons/greensock-icon.js';
// import GruntIcon from '../../../../js/components/icons/grunt-icon.js';
// import GulpIcon from '../../../../js/components/icons/gulp-icon.js';
// import HtmlIcon from '../../../../js/components/icons/html-icon.js';
// import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
// import LessIcon from '../../../../js/components/icons/less-icon.js';
// import NativeIcon from '../../../../js/components/icons/native-icon.js';
// import NodeIcon from '../../../../js/components/icons/node-icon.js';
// import PhpIcon from '../../../../js/components/icons/php-icon.js';
// import ReactiveIcon from '../../icons/reactive-icon.js';
// import SassIcon from '../../icons/sass-icon.js';
// import SeoIcon from '../../icons/seo-icon.js';
// import SqlIcon from '../../icons/sql-icon.js';
// import VideoIcon from '../../icons/video-icon.js';
// import WireframeIcon from '../../icons/wireframe-icon.js';
// import WordpressIcon from '../../icons/wordpress-icon.js';
class BusinessCards extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid portfolio">
                {/*
    <AngularIcon />
    <ApiIcon />
    <BitbucketIcon />
    <BootstrapIcon />
    <CmsIcon />
    <CpanelIcon />
    <Css3Icon />
    <GifIcon />
    <GitIcon />
    <GreensockIcon />
    <GruntIcon />
    <GulpIcon />
    <HtmlIcon />
    <JqueryIcon />
    <LessIcon />
    <NativeIcon />
    <NodeIcon />
    <PhpIcon />
    <ReactiveIcon />
    <SassIcon />
    <SeoIcon />
    <SqlIcon />
    <VideoIcon />
    <WireframeIcon />
    <WordpressIcon /> */}
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">Business card mockups: Do they Work in Web Design?</h1>
                            <h3 className="projHeaderP">A free download for you! </h3>
                            <br />
                            <p>Here are some business cards placeholders for you. You can place your own design in the smart object.</p>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <JqueryIcon />
                                <Css3Icon />
                                <GulpIcon />
                                <BitbucketIcon />
                            </div>
                            <div className="clearfix">
                                <a className="btn btn-secondary readmoreBTN"
                                    href="https://archibaldbutler.com/free-photoshop-downloads/Archibald-Butler-Business-Card-mock-up.zip">
                                    Download
                    </a>
                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={BusinessCardsImg1} />
                    </div>
                </div>

            </div>
        );
    }
}

export default BusinessCards;