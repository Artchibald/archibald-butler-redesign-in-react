import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import activewearImg1 from '../../../../img/internals/activewear/Activewear-New-Look-Head-Office.jpg';
import activewearImg2 from '../../../../img/internals/activewear/new-look-head-office-activewear-project.jpg';
import activewearImg3 from '../../../../img/internals/activewear/new-look-head-office-activewear-project2.jpg';
import ApiIcon from '../../icons/api-icon.js';
import BitbucketIcon from '../../icons/bitbucket-icon.js';
import BootstrapIcon from '../../icons/bootstrap-icon.js';
import CmsIcon from '../../icons/cms-icon.js';
import CpanelIcon from '../../icons/cpanel-icon.js';
import GulpIcon from '../../icons/gulp-icon.js';
import SassIcon from '../../icons/sass-icon.js';
import VideoIcon from '../../icons/video-icon.js';
// import AngularIcon from '../../../../js/components/icons/angular-icon.js';
// import ApiIcon from '../../../../js/components/icons/api-icon.js';
// import BitbucketIcon from '../../../../js/components/icons/bitbucket-icon.js';
// import BootstrapIcon from '../../../../js/components/icons/bootstrap-icon.js';
// import CmsIcon from '../../../../js/components/icons/cms-icon.js';
// import CpanelIcon from '../../../../js/components/icons/cpanel-icon.js';
// import Css3Icon from '../../../../js/components/icons/css3-icon.js';
// import GifIcon from '../../../../js/components/icons/gif-icon.js';
// import GitIcon from '../../../../js/components/icons/git-icon.js';
// import GreensockIcon from '../../../../js/components/icons/greensock-icon.js';
// import GruntIcon from '../../../../js/components/icons/grunt-icon.js';
// import GulpIcon from '../../../../js/components/icons/gulp-icon.js';
// import HtmlIcon from '../../../../js/components/icons/html-icon.js';
// import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
// import LessIcon from '../../../../js/components/icons/less-icon.js';
// import NativeIcon from '../../../../js/components/icons/native-icon.js';
// import NodeIcon from '../../../../js/components/icons/node-icon.js';
// import PhpIcon from '../../../../js/components/icons/php-icon.js';
// import ReactiveIcon from '../../icons/reactive-icon.js';
// import SassIcon from '../../icons/sass-icon.js';
// import SeoIcon from '../../icons/seo-icon.js';
// import SqlIcon from '../../icons/sql-icon.js';
// import VideoIcon from '../../icons/video-icon.js';
// import WireframeIcon from '../../icons/wireframe-icon.js';
// import WordpressIcon from '../../icons/wordpress-icon.js';
class Activewear extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid portfolio">
                {/*                                 <AngularIcon />
                                <ApiIcon />
                                <BitbucketIcon />
                                <BootstrapIcon />
                                <CmsIcon />
                                <CpanelIcon />
                                <Css3Icon />
                                <GifIcon />
                                <GitIcon />
                                <GreensockIcon />
                                <GruntIcon />
                                <GulpIcon />
                                <HtmlIcon />
                                <JqueryIcon />
                                <LessIcon />
                                <NativeIcon />
                                <NodeIcon />
                                <PhpIcon />
                                <ReactiveIcon />
                                <SassIcon />
                                <SeoIcon />
                                <SqlIcon />
                                <VideoIcon />
                                <WireframeIcon />
                                <WordpressIcon /> */}
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">WORKING AT THE NEW LOOK HEAD OFFICE IN LONDON:</h1>
                            <h3 className="projHeaderP">An exciting 5 months of great design.</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <ApiIcon />
                                <BitbucketIcon />
                                <BootstrapIcon />
                                <CmsIcon />
                                <CpanelIcon />
                                <GulpIcon />
                                <SassIcon />
                                <VideoIcon />

                            </div>

                            <div className="clearfix">
                                <a className="btn btn-secondary readmoreBTN" href="https://www.archibaldbutler.com/projects/Activewear/">
                                    View the work
                    </a>
                                <p>(Not fully functioning due to the client removing files from their server)</p>
                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={activewearImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>BEEN DOING SOME GREAT DESIGN WORK AT THE NEW LOOK HEAD OFFICE IN LONDON!</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">The longest page I have ever
                        coded!</h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        This was created at the New Look head office,<br />(<a
                                            href="https://www.google.co.uk/search?q=new+look+london+office&amp;oq=new+look+london+office&amp;aqs=chrome..69i57j69i60l3.4765j0j7&amp;sourceid=chrome&amp;ie=UTF-8">located</a>
                                        at: 45 Mortimer St, Westminster, London W1W 8HJ). The Activewear campaign is the longest
                                        page it has ever taken me to create. The rounds of amends must have been about 300 different
                            points. I got the contract to work indefinitely at New Look from the recruiting company <a
                                            href="https://www.venngroup.com/">Venn Group</a>. In fact, if you are a freelancer, I highly recommend getting in touch with Venn group London, they have excellent contracts.
                            <br className="c" />
                                    </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        I was really excited when I saw the original photoshop design and was told by my project
                            manager <a href="https://www.jennylawdesign.com/">Jenny law</a> (who is now freelancing as a
                                                                matter of fact, highly recommended), that this was a very exciting piece of work because the
                            budget invest from <a href="http://www.newlook.com/">New Look</a> was huge and they needed
                                                                this new mens’ page to be absolutely perfect. The page has a huge variety of features and
                                                                animations that were all requested by the team or ideas that I had, recommended and were
                            implemented into the project.</p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://www.archibaldbutler.com/projects/Activewear/">
                                        View the work
                        </a>
                                    <p>(Not fully functioning due to the client removing files from their server)</p>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <ApiIcon />
                                    <BitbucketIcon />
                                    <BootstrapIcon />
                                    <CmsIcon />
                                    <CpanelIcon />
                                    <GulpIcon />
                                    <SassIcon />
                                    <VideoIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="paddedPortfolioImg img-fluid"
                                    alt="Earthgang illustrations and animations in React JS" src={activewearImg2} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">About this project</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Theory:</h3>
                                        <p>
                                            A lot of time and effort went into this work during office hours and brainstorming after
                                            hours. I get very inspired when working on a new piece and added bespoke animation into the
                                            composition that is very discreet.
                            </p>

                                        <h3>Working on it:</h3>
                                        <p>
                                            If you hover over the first yellow title at the top of the page you will see a blur affect
                                appear. I had to create this <a href="https://codepen.io/artchibald/pen/oYWjPR">from scratch
                                    on Codepen</a> because there was nothing out there like it.
                            </p>
                                        <p>
                                            I created the new Codepen that you can see here with a blur effect that retracts and expands
                                            during mouse hover states.
                            </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Javascript Frameworks:</h3>
                                        <p>
                                            The main framework used for the placement of elements is <a
                                                href="http://getbootstrap.com/">Bootstrap CSS and Bootstrap JS</a>. Then I added custom
                                3-D animations in CSS such as the blue spinning icons.
                            </p>
                                        <h3>Downsides of the project:</h3>
                                        <p>
                                            On the downside, it was necessary to have many high-quality pictures of garments in the
                                            multiple sliders. This slowed down the page load but wasn’t my shot to call.

                                            The next very painstaking part of the process was compatibility between two different CMS
                                            systems where the page would sit.

                                            Many bugs across two huge bespoke CMS systems had to be ironed out and fixed. This step took
                                            the most time in the whole project as they were very difficult environments to work with.
                            </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://www.archibaldbutler.com/projects/Activewear/">
                                        View the work
                        </a>
                                    <p>(Not fully functioning due to the client removing files from their server)</p>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="paddedPortfolioImg img-fluid"
                                    alt="Earthgang illustrations and animations in React JS" src={activewearImg3} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">It’s fun to get free reign from the client</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Best of the project:</h3>
                                        <p>
                                            The most satisfying part of the project was receiving the final imagery that we were going to be using. During the first two thirds of the project I had to use dummy images for placements as we didn’t have all the photography and video yet.
                        </p>
                                        <p>
                                            Speaking of video, that didn’t stop me from creating a very exciting video javaScript animation that is entirely bespoke(<a href="https://www.archibaldbutler.com/projects/Activewear/index.html#play-video">see on page</a>).
                        </p><h3>HTML5 video:</h3>
                                        <p>
                                            I love the way you see a loop in the preview of the HTML5 videos, then, on click/tap, that div disappears and the actual video on the layer behind it starts to play!
                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            I was also present when the site went live, some people in the design teamsemailed me to say they were very pleased with it. It’s gratifying when people appreciate your work. I would recommend New Look for web developers, it is an environment where you will thrive.
                        </p>
                                        <p>
                                            Unfortunately I had to leave the New Look head offices because I received a much better offer from a private design company in London called MBA.
                        </p>
                                        <p>
                                            I hope you enjoyed looking at this project as much as I enjoyed creating it. You can see a lot more of my work on the <a href="/News">news page</a>.
                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://www.archibaldbutler.com/projects/Activewear/">
                                        View the work
                        </a>
                                    <p>(Not fully functioning due to the client removing files from their server)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Activewear; 