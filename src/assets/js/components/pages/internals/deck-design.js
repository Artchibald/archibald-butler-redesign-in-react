import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import DeckDesignImg1 from '../../../../img/internals/deck-design/deck-design1.jpg';
import DeckDesignImg2 from '../../../../img/internals/deck-design/deck-design2.jpg';
import DeckDesignImg3 from '../../../../img/internals/deck-design/deck-design3.jpg';
import DeckDesignImg4 from '../../../../img/internals/deck-design/deck-design4.jpg';
import DeckDesignImg5 from '../../../../img/internals/deck-design/deck-design5.jpg';
import DeckDesignImg6 from '../../../../img/internals/deck-design/deck-design6.jpg';
import DeckDesignImg7 from '../../../../img/internals/deck-design/deck-design7.jpg';
import DeckDesignImg8 from '../../../../img/internals/deck-design/deck-design8.jpg';
import DeckDesignImg9 from '../../../../img/internals/deck-design/deck-design9.jpg';

class DeckDesign extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>I got me a new skateboard!</h1>
                            <p>I bought myself a Canadian maple wood board on Ebay for £23.</p>

                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={DeckDesignImg1} />
                            <figcaption>
                                <h2><span>Skate Design</span></h2>
                                <p> Illustration techniques</p>

                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={DeckDesignImg2} />
                            <figcaption>
                                <h2><span>Skate Design</span></h2>
                                <p> Illustration techniques</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Skateboard design</h1>
                            <p>I decided to ink and illustrate it, it took me most of a day with the layers drying.</p>
                            <br />
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Skate Design</h1>
                            <p>I started with a gradient of cool paint (£10 a can) then did a fairy liquid trick I learnt recently.</p>
                            <h1>All this color!</h1>
                            <p>Then created a PSD mock up of what I wanted and free handed it. I love my Posca pens they are awesome. I gave it a spray of coating at the end too. I can’t wait for my trucks to arrive now!</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={DeckDesignImg3} />
                            <figcaption>
                                <h2><span>Skate Design</span></h2>
                                <p> Illustration techniques</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={DeckDesignImg4} />
                            <figcaption>
                                <h2><span>Skate Design</span></h2>
                                <p> Illustration techniques</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={DeckDesignImg5} />
                            <figcaption>
                                <h2><span>Skate Design</span></h2>
                                <p> Illustration techniques</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={DeckDesignImg6} />
                            <figcaption>
                                <h2><span>Skate Design</span></h2>
                                <p> Illustration techniques</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={DeckDesignImg7} />
                            <figcaption>
                                <h2><span>Skate Design</span></h2>
                                <p> Illustration techniques</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={DeckDesignImg8} />
                            <figcaption>
                                <h2><span>Skate Design</span></h2>
                                <p> Illustration techniques</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={DeckDesignImg9} />
                            <figcaption>
                                <h2><span>Skate Design</span></h2>
                                <p> Illustration techniques</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default DeckDesign; 