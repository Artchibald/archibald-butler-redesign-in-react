import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import InteractiveMapImg1 from '../../../../img/internals/interactive-map/interactive-map1.png';
import InteractiveMapImg2 from '../../../../img/internals/interactive-map/interactive-map2.jpg';
import Css3Icon from '../../icons/css3-icon.js';
import HtmlIcon from '../../icons/html-icon.js';
// import AngularIcon from '../../../../js/components/icons/angular-icon.js';
// import ApiIcon from '../../../../js/components/icons/api-icon.js';
// import BitbucketIcon from '../../../../js/components/icons/bitbucket-icon.js';
// import BootstrapIcon from '../../../../js/components/icons/bootstrap-icon.js';
// import CmsIcon from '../../../../js/components/icons/cms-icon.js';
// import CpanelIcon from '../../../../js/components/icons/cpanel-icon.js';
// import Css3Icon from '../../../../js/components/icons/css3-icon.js';
// import GifIcon from '../../../../js/components/icons/gif-icon.js';
// import GitIcon from '../../../../js/components/icons/git-icon.js';
// import GreensockIcon from '../../../../js/components/icons/greensock-icon.js';
// import GruntIcon from '../../../../js/components/icons/grunt-icon.js';
// import GulpIcon from '../../../../js/components/icons/gulp-icon.js';
// import HtmlIcon from '../../../../js/components/icons/html-icon.js';
// import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
// import LessIcon from '../../../../js/components/icons/less-icon.js';
// import NativeIcon from '../../../../js/components/icons/native-icon.js';
// import NodeIcon from '../../../../js/components/icons/node-icon.js';
// import PhpIcon from '../../../../js/components/icons/php-icon.js';
// import ReactiveIcon from '../../icons/reactive-icon.js';
// import SassIcon from '../../icons/sass-icon.js';
// import SeoIcon from '../../icons/seo-icon.js';
// import SqlIcon from '../../icons/sql-icon.js';
// import VideoIcon from '../../icons/video-icon.js';
// import WireframeIcon from '../../icons/wireframe-icon.js';
// import WordpressIcon from '../../icons/wordpress-icon.js';
class InteractiveMap extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid portfolio">
                {/*                                 <AngularIcon />
                                <ApiIcon />
                                <BitbucketIcon />
                                <BootstrapIcon />
                                <CmsIcon />
                                <CpanelIcon />
                                <Css3Icon />
                                <GifIcon />
                                <GitIcon />
                                <GreensockIcon />
                                <GruntIcon />
                                <GulpIcon />
                                <HtmlIcon />
                                <JqueryIcon />
                                <LessIcon />
                                <NativeIcon />
                                <NodeIcon />
                                <PhpIcon />
                                <ReactiveIcon />
                                <SassIcon />
                                <SeoIcon />
                                <SqlIcon />
                                <VideoIcon />
                                <WireframeIcon />
                                <WordpressIcon /> */}
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">INTERACTIVE MAP FLASH: THIS IS HOW IT IS DONE.</h1>
                            <h3 className="projHeaderP">Making an Interactive Flash Map.</h3>
                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <Css3Icon />
                                <HtmlIcon />
                            </div>
                            <div className="clearfix mt-3">
                                <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/projects/interactivemap/">View the work!</a>
                                <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/free-photoshop-downloads/download-free-panoramic-photographic-html-map-illustration.zip">Download it!</a>

                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={InteractiveMapImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>DOWNLOAD THE 2005 INTERACTIVE MAP FLASH</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix">Interactive Flash Map: Illustration, Development and coding skills</h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        An interactive map flash is in fact the search result for someone looking for ways to build An “Interactive Map in Adobe Flash”. Am I right?<br /><br />

                                        Well, it just so happens that I have a tutorial for download plus an explanation below!<br /><br />

                                        Here is a Adobe Flash based illustrated and animated map I created at University around 2005. Adobe Flash, eventhough it is now completely outdated, it can be used to create some fun projects with ease.
                                    </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        WARNING! Your Flash Map will not be visible on any Apple mobile products(laptops and towers work). If you wish to learn about the Adobe suite and Javascript based equivalents I would recommend looking at their products and trying a demo. As a more comfortable developer myself, the modern day equivalent of this in 2018 (As I write) would be a mix of CSS3 animation and probably Javascript Greensock plugin for further control.</p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix mb-5">
                                    <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/projects/interactivemap/">View the work!</a>
                                    <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/free-photoshop-downloads/download-free-panoramic-photographic-html-map-illustration.zip">Download it!</a>

                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <Css3Icon />
                                    <HtmlIcon />
                                </div>
                                <br />
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                                <img className="paddedPortfolioImg img-fluid"
                                    alt="Interactive map illustrations and animations in React JS" src={InteractiveMapImg2} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix">About this Flash project</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>About the Interactive Flash Map web project</h3>
                                        <p>
                                            Even with the compatibilty issues with IPhone or IPad still present, Flash is a great piece of software and you can even remove the presence of Flash elements completely with the use of css.


                            </p>
                                        <p>
                                            Therefore I decided to make this interactive panoramic map of a seaside reort I lived in a few years(Vendays-Montalivet, France). I drew the draft from theory and painted it in watercolors. Now later down the line in 2018, after 10 years of professional experience, I am SEO ranking this page for people searching for an Interactive map flash.
                            </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Working on it:</h3>

                                        <p>
                                            I then added the hover effect and links in Photoshop and added the new images to the Flash timeline. I then linked them to panoramic photos taken with a digital camera. They were put together using PT Gui, a photo stitching program. You will need Quicktime installed to view them.
                            </p>
                                        <p>It is a shame I couldn’t get the sky and the ground in there too.</p>

                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix mb-5">
                                    <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/projects/interactivemap/">View the work!</a>
                                    <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/free-photoshop-downloads/download-free-panoramic-photographic-html-map-illustration.zip">Download it!</a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                                <img className="paddedPortfolioImg img-fluid"
                                    alt="Interactive map illustrations and animations in React JS" src={InteractiveMapImg2} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix">It’s fun to get free reign from the client</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Illustration and code</h3>
                                        <p>
                                            I put together my composition in Photoshop and proceeded to overlay all the different elements in layers and start to think about the animations I was going to include.
                                        </p>


                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            Then came the final task of coding. Creating the elements in HTML adding the background and the positioning of elements in CSS using the box model.
                                            </p>

                                    </div>
                                    <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <p>
                                            I hope you enjoyed reading about making an illustrated website as much as I did creating it.
                                      </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix mb-5">
                                    <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/projects/interactivemap/">View the work!</a>
                                    <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/free-photoshop-downloads/download-free-panoramic-photographic-html-map-illustration.zip">Download it!</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default InteractiveMap; 