import React, { Component } from 'react';
import { BrowserRouter as Route, a } from "react-router-dom";
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import HowToBeImg1 from '../../../../img/internals/how-to-be/how-to-be1.jpg';
import HowToBeImg2 from '../../../../img/internals/how-to-be/how-to-be2.jpg';
import HowToBeImg3 from '../../../../img/internals/how-to-be/how-to-be3.jpg';
//icons
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';


class HowToBe extends Component {

    componentDidMount() {
        //make sure we load at top of page 
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>How to be a freelance web designer and developer.</h1>
                            <p>An idea I cams up with with that I might start after summer.
                            </p>
                            <p>
                                I thought of it and discussed it with some friends who thought it was a good idea.</p>
                            <p><strong>Not a very readable page on the button below. My apologies, I need to address this soon.</strong></p>
                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <Css3Icon />
                                <ReactiveIcon />
                                <SeoIcon />
                                <NativeIcon />
                            </div>
                            <div className="clearfix" ></div>
                            <a className="btn btn-secondary" href="https://archibaldbutler.com/projects/how-to-be/how-to-be.png">See the art</a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={HowToBeImg1} />
                            <figcaption>
                                <h2>How to be a <span>freelance web designer and developer</span></h2>
                                <p>Design and idea creation</p>


                                <a href="https://archibaldbutler.com/projects/how-to-be/how-to-be.png">See the art</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={HowToBeImg2} />
                            <figcaption>
                                <h2>How to be a <span>freelance web designer and developer</span></h2>
                                <p>Design and idea creation</p>

                                <br />
                                <a href="https://archibaldbutler.com/projects/how-to-be/how-to-be.png">See the art</a>
                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">

                            <h1>About the build</h1>
                            <p>Hand drawn stop frame animation takes so much time to complete!</p>
                            <br />
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox mt-5">
                            <h1>Conclusion</h1>
                            <p>The animation is a bit jittery, I could have done better!</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={HowToBeImg3} />
                            <figcaption>
                                <h2>How to be a <span>freelance web designer and developer</span></h2>
                                <p>Design and idea creation</p>

                                <br />

                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default HowToBe; 