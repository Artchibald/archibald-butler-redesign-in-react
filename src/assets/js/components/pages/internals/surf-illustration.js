import React, { Component } from 'react';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';

import SurfIllustration1 from '../../../../img/internals/surf-illustration/surf-illustration.jpg';
import WireframeIcon from '../../icons/wireframe-icon.js';

class SurfIllustration extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Yin and Yang Illustration</h1>
              <p>
                Here is a drawing I put together in over 3 hours in Hawaii.
                Pencil, ink, aquarelle, and crayons. I was inspired by this new
                artist I met and wanted to create something in her style. She is
                called Alexis Mason and her work is dope as hell.{' '}
              </p>
              <br />
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>
              <div className="clearfix">
                <WireframeIcon />
              </div>
              <br className="clearfix" />
              <br className="clearfix" />
              <br className="clearfix" />
              <br className="clearfix" />
              <a
                aria-label="See work"
                className="btn btn-secondary"
                href="https://archibaldbutler.com/wp-content/themes/archibaldbutler/img/internal/news/ying-and-yang.jpg"
              >
                See Illustration
              </a>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img alt="Illustrator Archibald Butler" src={SurfIllustration1} />
              <figcaption>
                <h2>Wave illustration</h2>
                <p>Life of Archibald Butler in April 2016.</p>
                <a
                  aria-label="See work"
                  href="https://archibaldbutler.com/wp-content/themes/archibaldbutler/img/internal/news/ying-and-yang.jpg"
                >
                  View more
                </a>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    );
  }
}

export default SurfIllustration;
