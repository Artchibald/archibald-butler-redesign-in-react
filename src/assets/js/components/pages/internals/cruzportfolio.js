import React, { Component } from 'react';
import { BrowserRouter as Route, Link } from 'react-router-dom';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';
import ReactiveIcon from '../../icons/reactive-icon.js';
import WireframeIcon from '../../icons/wireframe-icon.js';
import HtmlIcon from '../../icons/html-icon.js';
import Css3Icon from '../../icons/css3-icon.js';
import NativeIcon from '../../icons/native-icon.js';

import cruzImg1 from '../../../../img/internals/work/animated-gif-illustration-animation-thumbnail.gif';
import CruzImg2 from '../../../../img/internals/cruz/illustration-sketches-animate-css.jpg';
import CruzImg3 from '../../../../img/internals/cruz/mock-up-stories-by-cruz-illustration-animation-archibald-butler-css-animation.jpg';
import CruzImg4 from '../../../../img/internals/cruz/mock-up-2-stories-by-cruz-illustration-animation-archibald-butler-css-animation.jpg';

//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Angular JS"><div className="icon-roll angular-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="APIs"><div className="icon-roll api-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="BitBucket"><div className="icon-roll bitbucket-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Bootstrap"><div className="icon-roll bootstrap-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="CMS system"><div className="icon-roll cms-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="CPanel"><div className="icon-roll cpanel-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Css3 Animations"><div className="icon-roll css3-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title=".GIF images"><div className="icon-roll gif-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="GIT"><div className="icon-roll git-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Greensock"><div className="icon-roll greensock-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Grunt"><div className="icon-roll grunt-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Gulp"><div className="icon-roll gulp-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="HTML"><div className="icon-roll html-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="JQuery"><div className="icon-roll jquery-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="LESS"><div className="icon-roll less-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Native Javascript"><div className="icon-roll native-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Node JS"><div className="icon-roll node-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title=".PHP"><div className="icon-roll php-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="React JS"><div className="icon-roll react-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Sass Css"><div className="icon-roll sass-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="SEO"><div className="icon-roll seo-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="MySql"><div className="icon-roll sql-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="HTML5  Video"><div className="icon-roll video-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Wireframing"><div className="icon-roll wireframes-roll"></div></a>
//   <a  aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Wordpress"><div className="icon-roll wordpress-roll"></div></a>

class CruzPortfolio extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid portfolio">
        <Route />
        <div className="row no-pad">
          <div className="col-12 col-sm-12 col-md-6 col-lg-6">
            <div className="textbox">
              <h1 className="projHeaderTitle">
                CLIENT BRIEF FOR AN ARTWORK COMMISSION INVOLVING ANIMATING CSS:
              </h1>
              <h3 className="projHeaderP">
                A story telling blog about childhood and being a father, for the
                nicest of clients: digital specialist Chima Amechi.
              </h3>
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>

              <div className="clearfix">
                <ReactiveIcon />
                <WireframeIcon />
                <HtmlIcon />
                <Css3Icon />
                <NativeIcon />
              </div>
              <div className="clearfix">
                <a
                  aria-label="See work"
                  className="btn btn-secondary readmoreBTN"
                  href="/animate-css-stories/"
                >
                  View the work
                </a>
                <a aria-label="See work" href="#mainPara">
                  <div className="scroll-down-dude"></div>
                </a>
                <br className="c" />
                <br className="c" />
                <br className="c" />
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
            <img
              className="img-fluid"
              alt="Kinesis Money: Archibald Butler Web Development"
              src={cruzImg1}
            />
          </div>
        </div>
        <div className="container-fluid">
          <div className="row no-pad" id="mainPara">
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">1</div>
                <div className="text">
                  <p>Preparing the animate CSS Project</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Hand drawing the different elements of the commission process:
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    This is my fourth project out of a hopeful 10. A successful
                    commission I got through one of my new clients Chima Amechi.
                    He called me up and explained he had just seen my website.
                    He really liked the illustrations and the animations I had
                    coded. He asked me about the process involved in creating
                    these CSS3 and HTML5 effects. I explained how I take care of
                    Art business! A great look at what you can do when you
                    animate CSS!
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    I really wanted to do a good job on this project for him.
                    Therefore I put together a structure of how I was going to
                    deliver the work. The first step in that involves creating
                    the following vignettes so that Chima could select which
                    elements from each drawings he liked best but also would
                    suit his ideas most in the composition. It’s great to go
                    from start to finish: from paper to animate css canvases.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <Link
                    className="btn btn-secondary"
                    id="firstnav"
                    to="/animate-css-stories/"
                  >
                    View the work
                  </Link>
                </div>
              </div>
              <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                <h5 className="technoHeaderTitle">technologies used</h5>
                <div className="lineOnProject"></div>
                <div className="clearfix">
                  <div className="clearfix">
                    <ReactiveIcon />
                    <WireframeIcon />
                    <HtmlIcon />
                    <Css3Icon />
                    <NativeIcon />
                  </div>
                </div>
              </div>
              <div className="col-12 col-sm-12 offset-md-3 col-md-6 offset-lg-3 col-lg-6">
                <img
                  className="paddedPortfolioImg img-fluid"
                  alt="Earthgang illustrations and animations in React JS"
                  src={CruzImg2}
                />
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="paddedPortfolioImg img-fluid"
                  alt="Earthgang illustrations and animations in React JS"
                  src={CruzImg3}
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">2</div>
                <div className="text">
                  <p>the code</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Next stage of the project: Creating the Photoshop composition
                  for review
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    After he was happy with which elements we had chosen, I
                    started to create the artwork in Photoshop. This was again
                    sent to him for edits and amends. I created this Photoshop
                    piece at higher dimensions of 2500px width in size. More
                    than the previous ones I had created. After we got through
                    the second stage of approval I started to cut up the
                    Photoshop file in order to create the HTML and CSS.
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    As an extra layer of complexity I took it upon myself to
                    involve react JS in this project. It was a great learning
                    curve and the whole page appears through the React JS
                    asynchronous loading system. This is an exciting new
                    development in my career as I wrap my head around the next
                    generation of JavaScript frameworks.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <Link
                    className="btn btn-secondary"
                    id="firstnav"
                    to="/animate-css-stories/"
                  >
                    View the work
                  </Link>
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="paddedPortfolioImg img-fluid"
                  alt="Earthgang illustrations and animations in React JS"
                  src={CruzImg4}
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">3</div>
                <div className="text">
                  <p>conclusion</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Built in React JS no less my good sir….
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    I added my personal page loader in JavaScript after the
                    Reacts JS render method. On the back of that puppy, I also
                    synced it with our famous and favourite CMS system:
                    WordPress. I then sent this bad boy back to the client for
                    his review. I explained how I had used new technologies and
                    because he already works in digital he knew exactly where I
                    was going with this… He was so happy with it, he insisted on
                    taking me out to lunch to discuss. I kindly agreed to this
                    meeting which we had two days ago. He was really happy with
                    the work to say the least. I also showed him other
                    intricacies and symbols that he hadn’t noticed which are
                    hidden within the composition.
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    Chima gave me other minor amends to move images around that
                    more suited the idea behind his project. Which was, to
                    remind you, a place to share stories, interviews and videos
                    about:
                    <br />
                    <br />
                    -What it’s like being a father
                    <br />
                    -How do you remember your father
                    <br />
                    -Other diverse social stories
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <Link
                    className="btn btn-secondary"
                    id="firstnav"
                    to="/animate-css-stories/"
                  >
                    View the work
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CruzPortfolio;
