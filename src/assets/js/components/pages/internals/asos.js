import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';
//icons
import ReactiveIcon from '../../icons/reactive-icon.js';
import ApiIcon from '../../icons/api-icon.js';
import CmsIcon from '../../icons/cms-icon.js';
//img
import asosImg1 from '../../../../img/internals/asos/Archibald-Butler-web-development-asos-tumblr-account.jpg';
import asosImg2 from '../../../../img/internals/asos/Archibald-Butler-web-development-asos-tumblr-account-2.jpg';
import asosImg3 from '../../../../img/internals/asos/Archibald-Butler-web-development-asos-tumblr-account-3.jpg';

class Asos extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }



    render() {
        return (
            <div className="container-fluid portfolio">
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">WORKING FOR ASOS: A FREELANCER’S DREAM</h1>
                            <h3 className="projHeaderP">One of the best places I have ever worked at.</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <ReactiveIcon />
                                <ApiIcon />
                                <CmsIcon />
                            </div>
                            <div className="clearfix">
                                <a className="btn btn-secondary readmoreBTN" href="https://asoslive.tumblr.com/">
                                    View the work
                    </a>
                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={asosImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>ASOS JOBS: A FANTASTIC BESPOKE THEME CUSTOMISATION</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">This new site boosted and attracted half a million new visitors a month (OMG!!!) since it has been updated (statistic heard at the quarterly ASOS review!)
                    </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        I have been working on an ASOS job for a couple of weeks on Tumblr (something I was not familiar with) and to my surprise, the code/tags for Tumblr are bespoke.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        There was a bit of a learning curve there. Anyhow, these bad boys combine infinite scroll js and masonry js.
                        </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://asoslive.tumblr.com/">
                                        View the work
                        </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <ReactiveIcon />
                                    <ApiIcon />
                                    <CmsIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid mt-5" alt="Kinesis Money: Archibald Butler Web Development" src={asosImg2} />
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">About this project
                    </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        The homepage hover effects were superimposed in JS for optimum clickability and I had to build custom iframes for the like buttons.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        Most of which was custom javascript. These themes are not available for download, so this is the only place you will see them:
                        </p>
                                    <p className="col-12 col-sm-12 col-md-12 col-lg-12">
                                        Women’s wear: <a href="http://asoslive.tumblr.com/">http://asoslive.tumblr.com/</a>
                                        <br />
                                        Men’s wear: <a href="http://asosmenswear.tumblr.com/">http://asosmenswear.tumblr.com/</a>
                                    </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://asoslive.tumblr.com/">
                                        View the work
                        </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={asosImg3} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">8 weeks of work (amongst my three years there).</h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        I very much enjoy working with the boys at ASOS. As a freelancer, I have worked almost three years there full time.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        We study Github, Jquery, SASS, LESS, sitecore CMS, project management tools and a lot more.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-12 col-lg-12">
                                        I hope you enjoyed this project, feel free to <a href="https://archibaldbutler.com/contact/">get in touch</a> if you have any questions.
                        <br className="c" /><br className="c" />
                                    </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="https://asoslive.tumblr.com/">
                                        View the work
                        </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Asos; 