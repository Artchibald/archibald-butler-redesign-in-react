import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
import csmImg90 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-90.jpg";
import csmImg91 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-91.jpg";
import csmImg92 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-92.jpg";
import csmImg93 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-93.jpg";
import csmImg94 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-94.jpg";
import csmImg95 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-95.jpg";
import csmImg96 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-96.jpg";
import csmImg97 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-97.jpg";
import csmImg98 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-98.jpg";
import csmImg99 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-99.jpg";
import csmImg100 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-100.jpg";
import csmImg101 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-101.jpg";
import csmImg102 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-102.jpg";
import csmImg103 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-103.jpg";
import csmImg104 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-104.jpg";
import csmImg105 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-105.jpg";
import csmImg106 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-106.jpg";
import csmImg107 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-107.jpg";
import csmImg108 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-108.jpg";
import csmImg109 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-109.jpg";
import csmImg110 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-110.jpg";
import csmImg111 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-111.jpg";
import csmImg112 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-112.jpg";
import csmImg113 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-113.jpg";
import csmImg114 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-114.jpg";
import csmImg115 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-115.jpg";
import csmImg116 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-116.jpg";
import csmImg117 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-117.jpg";
import csmImg118 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-118.jpg";
import csmImg119 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-119.jpg";

import chevronLeftImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class csmArtwork4 extends Component {
 componentDidMount() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid">
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-12 col-lg-12">
      <h1 className="text-center mt-5">May the horror continue!</h1>
      <p className="text-center mb-5">
       These drawings cost me a £25,000.00 student debt. Wow… Just wow.
      </p>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg118}
       />
       <figcaption>
        <h2>This is like that Van Gogh.</h2>
        <p>No it isn’t. Shut up Archie.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-118.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg90}
       />
       <figcaption>
        <h2>
         No! Not the Scream mask! <span>AGAIN!</span>
        </h2>
        <p>This is whole situation is more sad than angry though.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-90.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg91}
       />
       <figcaption>
        <h2>
         Drawing of self inflicted <span>tooth pain.</span>
        </h2>
        <p>I really must sign this one.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-91.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg92}
       />
       <figcaption>
        <h2>
         Repeat 3, boring, <span>BORING.</span>
        </h2>
        <p>You’re not still here, are you?</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-92.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg93}
       />
       <figcaption>
        <h2>
         I like the depressing <span>edge.</span>
        </h2>
        <p>It’s very Art Nouveau. NOT.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-93.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg94}
       />
       <figcaption>
        <h2>
         Burn <span>Everyone.</span>
        </h2>
        <p>Just great.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-94.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg95}
       />
       <figcaption>
        <h2>
         Cocky little <span>b*st*rd.</span>
        </h2>
        <p>That’s what the old you replies!</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-95.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg96}
       />
       <figcaption>
        <h2>
         A burning <span>Emo adolescent.</span>
        </h2>
        <p>Splendid work on your BA, young man.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-96.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg97}
       />
       <figcaption>
        <h2>
         This is <span>political!</span>
        </h2>
        <p>Good effort young gun!</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-97.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg98}
       />
       <figcaption>
        <h2>
         This one <span>I kinda like</span>
        </h2>
        <p>Nice colors and a bit of innocence at last!</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-98.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg99}
       />
       <figcaption>
        <h2>
         Draft illustrations for <span>surfboards.</span>
        </h2>
        <p>My main hobby at the time.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-99.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg100}
       />
       <figcaption>
        <h2>
         Ocean dragon answers <span>Samurai prayer…</span>
        </h2>
        <p>Pure depth me breda</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-100.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg101}
       />
       <figcaption>
        <h2>
         Some graffiti <span>a mate did.</span>
        </h2>
        <p>In my sketch books.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-101.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg102}
       />
       <figcaption>
        <h2>Cool composition</h2>
        <p>No pencils involved.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-102.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg103}
       />
       <figcaption>
        <h2>
         F**k <span>the underground!</span>
        </h2>
        <p>I would have to agree, being from London.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-103.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg104}
       />
       <figcaption>
        <h2>
         Love this hungry <span>teddy.</span>
        </h2>
        <p>I must do something with this one.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-104.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg105}
       />
       <figcaption>
        <h2>
         Baby <span>torture!</span>
        </h2>
        <p>Had to go there at some point, didn’t I?.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-105.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg106}
       />
       <figcaption>
        <h2>
         More weird <span>Scramblings.</span>
        </h2>
        <p>Not much to say about this really.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-106.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg107}
       />
       <figcaption>
        <h2>
         Shaman <span>Vision.</span>
        </h2>
        <p>I love the colors on this.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-107.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg108}
       />
       <figcaption>
        <h2>
         The <span>Dalai Lama.</span>
        </h2>
        <p>A sketch from photo I believe.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-108.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg109}
       />
       <figcaption>
        <h2>
         Some weird ganster <span>rap stuff.</span>
        </h2>
        <p>Hope you vibe his quick sketch style.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-109.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg119}
       />
       <figcaption>
        <h2>
         Museum <span>Work.</span>
        </h2>
        <p>Drawing sculptures.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-119.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-12 col-md-12 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg110}
       />
       <figcaption>
        <h2>
         An Egyptian <span>Scarab.</span>
        </h2>
        <p>My dad gave it to me but I may have lost it :(.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-110.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg111}
       />
       <figcaption>
        <h2>
         More <span>randomness.</span>
        </h2>
        <p>This was all at my expense.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-111.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg112}
       />
       <figcaption>
        <h2>
         A battlefield <span>sketch.</span>
        </h2>
        <p>Done in museum.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-112.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg113}
       />
       <figcaption>
        <h2>
         Not sure where <span>this came from.</span>
        </h2>
        <p>But I like it.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-113.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg114}
       />
       <figcaption>
        <h2>
         A friend <span>I drew.</span>
        </h2>
        <p>She looks as depressed as I felt.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-114.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg115}
       />
       <figcaption>
        <h2>
         Jack the Ripper’s <span>alleway.</span>
        </h2>
        <p>Shame he didn’t take care of my drawings.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-115.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg116}
       />
       <figcaption>
        <h2>
         Random <span>old man.</span>
        </h2>
        <p>Enjoy this wonderful sketch.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-116.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration of World poverty"
        src={csmImg117}
       />
       <figcaption>
        <h2>
         Another mate <span>from uni.</span>
        </h2>
        <p>Studying. #NoFuture.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-117.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-1 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <a className="previous-link" href="/csm-artwork-3">
        <div className="animate1">
         <img
          className="img-fluid"
          src={chevronLeftImg}
          alt="Chevron left: Previous page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
     <div className="offset-col-10 col-1 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <a className="next-link" href="/csm-artwork-5">
        <div className="animate2">
         <img
          className="img-fluid"
          src={chevronRightImg}
          alt="Chevron right: Next page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default csmArtwork4;
