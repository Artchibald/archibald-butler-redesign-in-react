import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import OneMega1 from '../../../../img/internals/one-mega/one-mega1.jpg';
import OneMega2 from '../../../../img/internals/one-mega/one-mega2.jpg';
import OneMega3 from '../../../../img/internals/one-mega/one-mega3.jpg';
//icons
import HtmlIcon from '../../../../js/components/icons/html-icon.js';
import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
import WireframeIcon from '../../icons/wireframe-icon.js';
import Css3Icon from '../../../../js/components/icons/css3-icon.js';

class OneMega extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>One Mega Management.</h1>
                            <p>I met Michael, the founder of One Mega in a pub near Leicester Square in London.</p>
                            <p>We started chatting and he became my client.</p>
                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <HtmlIcon />
                                <JqueryIcon />
                                <WireframeIcon />
                                <Css3Icon />
                            </div>
                            <br className="clearfix" />
                            <br className="clearfix" />
                            <br className="clearfix" />
                            <br className="clearfix" />
                            <a className="btn btn-secondary"
                                href="http://onemegacollective.com/">View the website (sold &amp; discontinued)</a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={OneMega1} />
                            <figcaption>
                                <h2><span>One</span> Mega Management</h2>
                                <p>Full des/dev on my own</p>
                                <a href="http://onemegacollective.com/">View more</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={OneMega2} />
                            <figcaption>
                                <h2><span>One</span> Mega Management</h2>
                                <p>Full des/dev on my own</p>
                                <br />
                                <a
                                    href="http://onemegacollective.com/">View the website (sold &amp; discontinued)</a>
                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>About the build</h1>
                            <p>The design and brief he described reminded me instantly of a new full width template I wanted to try.
                                                </p>
                            <p>I sent him over a design losely based on the framework I had in mind and he loved it.</p>
                            <br />
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Conclusion</h1>
                            <p>The final result is a blend of multiple frameworks (plugins, custom elements), all streamed to work
                                                    bug free and attract new visitors to the business.</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={OneMega3} />
                            <figcaption>
                                <h2><span>One</span> Mega Management</h2>
                                <p>Full des/dev on my own</p>
                                <br />
                                <a
                                    href="http://onemegacollective.com/">View the website (sold &amp; discontinued)</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default OneMega; 