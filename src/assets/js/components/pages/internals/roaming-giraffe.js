import React, { Component } from 'react';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';

import RoamingGiraffeImg1 from '../../../../img/internals/roaming-giraffe/roaming-giraffe-1.jpg';
import RoamingGiraffeImg2 from '../../../../img/internals/roaming-giraffe/roaming-giraffe-2.jpg';
import RoamingGiraffeImg3 from '../../../../img/internals/roaming-giraffe/roaming-giraffe-3.jpg';
//icons
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';

class RoamingGiraffe extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Roaming Giraffe.</h1>
              <p>A pop up catering business founded by Hen and jack</p>
              <p>
                I was very touched when she told me she liked my design style
                and I got straight to work. I tried to go above and beyond with
                the Photoshop work as a result. It is a fully custom WordPress
                theme, carefully implemented using SEO techniques for pop up
                catering.
              </p>
              <br />
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>
              <div className="clearfix">
                <Css3Icon />
                <ReactiveIcon />
                <SeoIcon />
                <NativeIcon />
              </div>
              <div className="clearfix"></div>
              <a
                aria-label="See work"
                className="btn btn-secondary"
                href="https://archibaldbutler.com/projects/roaming-giraffe/index.html"
              >
                View the composition
              </a>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="Illustrator Archibald Butler"
                src={RoamingGiraffeImg1}
              />
              <figcaption>
                <h2>
                  Roaming <span>Giraffe</span>
                </h2>
                <p>Web Design and development</p>
                <a
                  aria-label="See work"
                  href="https://archibaldbutler.com/projects/roaming-giraffe/index.html"
                >
                  View more
                </a>
              </figcaption>
            </figure>
          </div>
        </div>
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="Illustrator Archibald Butler"
                src={RoamingGiraffeImg2}
              />
              <figcaption>
                <h2>
                  Roaming <span>Giraffe</span>
                </h2>
                <p>Web Design and development</p>
                <br />
                <a href="https://archibaldbutler.com/projects/roaming-giraffe/index.html">
                  View the composition
                </a>
              </figcaption>
            </figure>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Layering that PSD!</h1>
              <p>
                The design was very much refined for a lengthy amount of time: 3
                – 4 days.
              </p>
              <br />
            </div>
          </div>
        </div>
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Conclusion</h1>
              <p>
                I was very pleased with this project and my friend Louis said he
                loved it too which was positive!
              </p>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="A web design illustration by Archibald Butler"
                src={RoamingGiraffeImg3}
              />
              <figcaption>
                <h2>
                  Roaming <span>Giraffe</span>
                </h2>
                <p>Web Design and development</p>
                <br />
                <a href="https://archibaldbutler.com/projects/roaming-giraffe/index.html">
                  View the composition
                </a>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    );
  }
}

export default RoamingGiraffe;
