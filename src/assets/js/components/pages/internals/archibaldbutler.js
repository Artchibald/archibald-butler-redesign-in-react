import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
//icon
import BootstrapIcon from "../../icons/bootstrap-icon.js";
import ReactiveIcon from "../../icons/reactive-icon.js";
import GreensockIcon from "../../icons/greensock-icon.js";
//img
import archibaldButlerImg1 from "../../../../img/internals/archibaldbutler/archibaldbutler-com-making-of-1.jpg";
import archibaldButlerImg2 from "../../../../img/internals/archibaldbutler/archibaldbutler-com-making-of-2.jpg";
import archibaldButlerImg3 from "../../../../img/internals/archibaldbutler/archibaldbutler-com-making-of-3.jpg";

class ArchibaldButler extends Component {
 componentDidMount() {
  //make sure we load at top of page
  window.scrollTo(0, 0);
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid portfolio">
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-6 col-lg-6">
      <div className="textbox">
       <h1 className="projHeaderTitle">
        A BESPOKE WORDPRESS THEME INTEGRATED WITH BOOTSTRAP AND GREENSOCK:
       </h1>
       <h3 className="projHeaderP">
        Making an illustrated website: archibaldbutler.com. I really enjoyed
        creating&nbsp;
        <Link to="/">this web project</Link>.
       </h3>
       <br />
       <h5 className="technoHeaderTitle">technologies used</h5>
       <div className="lineOnProject"></div>

       <div className="clearfix">
        <BootstrapIcon />
        <ReactiveIcon />
        <GreensockIcon />
       </div>
       <div className="clearfix">
        <Link className="btn btn-secondary" to="/">
         View the work
        </Link>
        <a href="#mainPara">
         <div className="scroll-down-dude"></div>
        </a>
        <br className="c" />
        <br className="c" />
        <br className="c" />
       </div>
      </div>
     </div>
     <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
      <img
       className="img-fluid"
       alt="Kinesis Money: Archibald Butler Web Development"
       src={archibaldButlerImg1}
      />
     </div>
    </div>
    <div className="container-fluid">
     <div className="row no-pad" id="mainPara">
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">1</div>
        <div className="text">
         <p>MAKING AN ILLUSTRATED WEBSITE.</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         A lot of heart went into this project!
        </h2>
        <div className="row">
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          I must say I do absolutely love the homepage I created for this
          website. I drew the illustrations whilst I was{" "}
          <a href="https://www.flickr.com/photos/136938688@N03/albums">
           travelling and backpacking
          </a>{" "}
          around the world with my beloved surfboard Betty looking for amazing
          waves to surf.
         </p>
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          Some of the illustrations were done in Brazil, some of them were done
          in Mexico and some of them were done in California. I cut up the
          design and integrated it into code using Greensock whilst I was
          staying with friends in California.
         </p>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <Link className="btn btn-secondary" to="/">
          View the work
         </Link>
        </div>
       </div>
       <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
        <h5 className="technoHeaderTitle">technologies used</h5>
        <div className="lineOnProject"></div>
        <div className="clearfix">
         <BootstrapIcon />
         <ReactiveIcon />
         <GreensockIcon />
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img
         className="img-fluid mt-5"
         alt="Kinesis Money: Archibald Butler Web Development"
         src={archibaldButlerImg2}
        />
       </div>
      </div>

      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">2</div>
        <div className="text">
         <p>the code</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         About this animated and illustrated project
        </h2>
        <div className="row">
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Live date:</h3>
          <p>
           It actually went live in Cali! I got some great positive feedback
           from my friends on{" "}
           <a href="https://www.facebook.com/ArchibaldButlerMarketing">
            Facebook
           </a>{" "}
           which was really encouraging. This is why you will find on my
           portfolio page some other <a href="/work/">similar illustrations</a>.
          </p>
         </div>
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Illustrating it:</h3>
          <p>
           The illustrations themselves were started with pencil, then inked,
           then I erased the pencil marks and proceeded to paint the elements
           individually with watercolours. I then did a bit more inking and
           added some subtle crayon effects to make some of the textures pop.
          </p>
         </div>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <Link className="btn btn-secondary" to="/">
          View the work
         </Link>
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img
         className="img-fluid"
         alt="Kinesis Money: Archibald Butler Web Development"
         src={archibaldButlerImg3}
        />
       </div>
      </div>
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">3</div>
        <div className="text">
         <p>conclusion</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         Illustration and code
        </h2>
        <div className="row">
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <p>
           I put together my composition in Photoshop and proceeded to overlay
           all the different elements in layers and start to think about the
           animations I was going to include.
          </p>
          <p>
           Then came the final task of coding. Creating the elements in HTML
           adding the background and the positioning of elements in CSS using
           the{" "}
           <a href="https://www.w3schools.com/css/css_boxmodel.asp">
            box model
           </a>
           .
          </p>
         </div>
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <p>
           Finally, adding the animations in JavaScript using Greenstock.
           Originally the site was static because I had heard the SEO was good
           on static websites so I wanted to try it out.
          </p>
          <p>
           I hope you enjoyed reading about making an illustrated website as
           much as I did creating it.
          </p>
         </div>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <Link className="btn btn-secondary" to="/">
          View the work
         </Link>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default ArchibaldButler;
