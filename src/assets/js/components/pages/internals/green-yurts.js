import React, { Component } from 'react';
import { BrowserRouter as Route, a } from "react-router-dom";
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import GreenYurtsImg1 from '../../../../img/internals/green-yurts/green-yurts1.jpg';
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';


class GreenYurts extends Component {

    componentDidMount() {
        //make sure we load at top of page 
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>This was for my good friends Phil and Al.</h1>
                            <p>This is a fully custom, Bootstrap integration of a custom WordPress theme! The owners are good friends of mine and I give them a good deal on my work. The site was built just before Bootstrap CSS appeared. After adding all the content and php theme files that included many templates, I then had to refactor all those files to accomodate Bootstrap and its features.</p>
                            <p>
                                It was frustrating as I don’t like long code amends that be can’t be shortcuted. Nevertheless they are great buddies and it all pays off in the end <span aria-label="happy face" role="img">😉</span>
                            </p>

                            <br />

                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <Css3Icon />
                                <ReactiveIcon />
                                <SeoIcon />
                                <NativeIcon />
                            </div>
                            <div className="clearfix" ></div>
                            <a className="btn btn-secondary"
                                href="https://archibaldbutler.com/projects/green-yurts/?page_id=19">View the composition</a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={GreenYurtsImg1} />
                            <figcaption>
                                <h2>A  <span>Green Yurts Events Hire UK Theme</span></h2>
                                <p>developed by Archie</p>


                                <a href="https://archibaldbutler.com/projects/green-yurts/?page_id=19">View more</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default GreenYurts; 