import React, { Component } from 'react';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';
//icons
import WireframeIcon from '../../icons/wireframe-icon.js';
import HtmlIcon from '../../icons/html-icon.js';
import JqueryIcon from '../../icons/jquery-icon.js';
import GreensockIcon from '../../icons/greensock-icon.js';
import Css3Icon from '../../icons/css3-icon.js';
//img
import killuminatiImg1 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-1.jpg';
import killuminatiImg2 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-2.jpg';
import killuminatiImg3 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-3.jpg';
import killuminatiImg4 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-4.jpg';
import killuminatiImg5 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-5.jpg';
import killuminatiImg6 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-6.jpg';
import killuminatiImg7 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-7.jpg';
import killuminatiImg8 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-8.jpg';
import killuminatiImg9 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-9.jpg';
import killuminatiImg10 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-10.jpg';
import killuminatiImg11 from '../../../../img/internals/killuminati/html5-animated-illustrations-killuminati-11.jpg';

class Killuminati extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid portfolio">
        <div className="row no-pad">
          <div className="col-12 col-sm-12 col-md-6 col-lg-6">
            <div className="textbox">
              <h1 className="projHeaderTitle">
                POLITICAL CARTOON WEB ANIMATION:
              </h1>
              <h3 className="projHeaderP">
                Trying to create art that is different
              </h3>
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>

              <div className="clearfix">
                <WireframeIcon />
                <HtmlIcon />
                <JqueryIcon />
                <GreensockIcon />
                <Css3Icon />
              </div>

              <div className="clearfix">
                <a
                  aria-label="See work"
                  className="btn btn-secondary readmoreBTN"
                  href="/killuminati-first-political-cartoon/"
                >
                  View the work
                </a>

                <a aria-label="See work" href="#mainPara">
                  <div className="scroll-down-dude"></div>
                </a>
                <br className="c" />
                <br className="c" />
                <br className="c" />
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
            <img
              className="img-fluid"
              alt="HTML5 animated illustration"
              src={killuminatiImg1}
            />
          </div>
        </div>
        <div className="container-fluid">
          <div className="row no-pad" id="mainPara">
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">1</div>
                <div className="text">
                  <p>PREPARING THE POLITICAL CARTOON</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Hand drawing the political cartoon:
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    It was inspiring to create this piece called{' '}
                    <a
                      aria-label="See work"
                      href="/killuminati-first-political-cartoon/"
                    >
                      Killuminati
                    </a>{' '}
                    by current tragic political situations. I was keen to see if
                    people would be interested in my work if I had a political
                    edge.
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    You can see the orginal sketches at the bottom of this page.
                    They took about 12 hours to draw. I love spending time
                    illustrating colourful compositions like this with a variety
                    of pens and paints: Posca, Water color crayons and black
                    markers.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary clearfix"
                    href="/killuminati-first-political-cartoon/"
                  >
                    View the work
                  </a>
                </div>
              </div>
              <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                <h5 className="technoHeaderTitle">technologies used</h5>
                <div className="lineOnProject"></div>
                <div className="clearfix">
                  <WireframeIcon />
                  <HtmlIcon />
                  <JqueryIcon />
                  <GreensockIcon />
                  <Css3Icon />
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="paddedPortfolioImg img-fluid"
                  alt="Earthgang illustrations and animations in React JS"
                  src={killuminatiImg2}
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">2</div>
                <div className="text">
                  <p>the code</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Political Cartoons: About this project
                </h2>
                <div className="row">
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <p>
                      I then imported all these sketches by taking pictures of
                      them with my SLR camera. These were then imported into
                      Photoshop to create the composition. This stage took about
                      5 hours to complete. It is time consuming to cut out
                      everything, edit the colors of the layers so they pop
                      together.
                    </p>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <p>
                      The canvas size I usually use is 2400 pixels by a height
                      just over the screen size of a portrait iPhone browser. I
                      save the PNGs and the JPEGs with the maximum width of 1500
                      pixels. This will mean they will stretch when they’re on
                      the large 1920 pixels monitor but it will improve the load
                      speed significantly.
                    </p>
                  </div>
                </div>

                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary clearfix"
                    href="/killuminati-first-political-cartoon/"
                  >
                    View the work
                  </a>
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="paddedPortfolioImg img-fluid"
                  alt="Earthgang illustrations and animations in React JS"
                  src={killuminatiImg3}
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">3</div>
                <div className="text">
                  <p>conclusion</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Dump Trump{' '}
                  <a
                    aria-label="See work"
                    href="https://www.youtube.com/watch?v=E4i3bAtEuJE"
                  >
                    SNOOP DOG STYLE
                  </a>
                </h2>
                <div className="row">
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <h3>Best of the project:</h3>
                    <p>
                      You can always run your images through tinyPNG.com to
                      reduce the file size without affecting the quality.
                    </p>
                  </div>
                  <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <p>
                      The coding aspect of this project which is integrated with
                      Greensock JS took about six hours. That is because I
                      already had the template from the homepage of{' '}
                      <a
                        aria-label="See work"
                        href="https://archibaldbutler.com/"
                      >
                        ArchibaldButler.com
                      </a>
                      .
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4 mt-5">
                    <img
                      className="img-fluid"
                      alt="Animated illustrations by Archibald Butler"
                      src={killuminatiImg4}
                    />
                  </div>

                  <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4 mt-5">
                    <img
                      className="img-fluid"
                      alt="Animated illustrations by Archibald Butler"
                      src={killuminatiImg5}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                    <img
                      className="img-fluid"
                      alt="Animated illustrations by Archibald Butler"
                      src={killuminatiImg6}
                    />
                  </div>

                  <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                    <img
                      className="img-fluid"
                      alt="Animated illustrations by Archibald Butler"
                      src={killuminatiImg7}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                    <img
                      className="img-fluid"
                      alt="Animated illustrations by Archibald Butler"
                      src={killuminatiImg8}
                    />
                  </div>

                  <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                    <img
                      className="img-fluid"
                      alt="Animated illustrations by Archibald Butler"
                      src={killuminatiImg9}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                    <img
                      className="img-fluid"
                      alt="Animated illustrations by Archibald Butler"
                      src={killuminatiImg10}
                    />
                  </div>

                  <div className="col-12 col-sm-6 col-md-6 col-lg-6 mb-4">
                    <img
                      className="img-fluid"
                      alt="Animated illustrations by Archibald Butler"
                      src={killuminatiImg11}
                    />
                  </div>
                </div>

                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <p>
                    I hope you enjoyed this project, feel free to get in touch
                    if you have any questions.
                  </p>
                  <a
                    aria-label="See work"
                    className="btn btn-secondary clearfix"
                    href="/killuminati-first-political-cartoon/"
                  >
                    View the work
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Killuminati;
