import React, { Component } from 'react';
import { BrowserRouter as Route, Link } from "react-router-dom";
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';
import ReactiveIcon from '../../icons/reactive-icon.js';
import GreensockIcon from '../../icons/greensock-icon.js';
import Css3Icon from '../../icons/css3-icon.js';
import NativeIcon from '../../icons/native-icon.js';

import earthgangImg1 from '../../../../img/internals/earthgang/earthgang-1-Archibald-Butler-Web-Development.jpg';
import earthgangImg2 from '../../../../img/internals/earthgang/earthgang-2-Archibald-Butler-Web-Development.jpg';
import earthgangImg3 from '../../../../img/internals/earthgang/earthgang-3-Archibald-Butler-Web-Development.jpg';
import earthgangImg4 from '../../../../img/internals/earthgang/earthgang-4-Archibald-Butler-Web-Development.jpg';


//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Angular JS"><div className="icon-roll angular-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="APIs"><div className="icon-roll api-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="BitBucket"><div className="icon-roll bitbucket-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Bootstrap"><div className="icon-roll bootstrap-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="CMS system"><div className="icon-roll cms-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="CPanel"><div className="icon-roll cpanel-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Css3 Animations"><div className="icon-roll css3-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title=".GIF images"><div className="icon-roll gif-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="GIT"><div className="icon-roll git-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Greensock"><div className="icon-roll greensock-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Grunt"><div className="icon-roll grunt-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Gulp"><div className="icon-roll gulp-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="HTML"><div className="icon-roll html-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="JQuery"><div className="icon-roll jquery-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="LESS"><div className="icon-roll less-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Native Javascript"><div className="icon-roll native-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Node JS"><div className="icon-roll node-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title=".PHP"><div className="icon-roll php-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="React JS"><div className="icon-roll react-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Sass Css"><div className="icon-roll sass-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="SEO"><div className="icon-roll seo-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="MySql"><div className="icon-roll sql-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="HTML5  Video"><div className="icon-roll video-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Wireframing"><div className="icon-roll wireframes-roll"></div></a>
//   <a className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Wordpress"><div className="icon-roll wordpress-roll"></div></a>



class EarthGangPortfolio extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }



    render() {
        return (
            <div className="container-fluid portfolio">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">EARTHGANG: A PERSONAL PET PROJECT.</h1>
                            <h3 className="projHeaderP">With some January spare time on my hands, I decided to animate some custom
                    illustrations.</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <ReactiveIcon />
                                <GreensockIcon />
                                <Css3Icon />
                                <NativeIcon />

                            </div>
                            <div className="clearfix">
                                <Link
                                    className="btn btn-secondary readmoreBTN"
                                    id="firstnav" to="/earthgang-an-html-art-project/">View the work
                                </Link>

                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={earthgangImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>the brief</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Creating wacky custom art
                    </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <Link
                                            to="/earthgang-an-html-art-project/">Earthgang
                                </Link> is a
                                                                                                                                                                                        piece of art I developed in my spare time for a band that a friend loves. Their style is
                                                                                                                                                                                        pretty close to insanity so I thought I would add my own dose of madness to their audio
                                                                                                                                                                                        poison. I think it worked out rather well if I may say so!
                            </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        It was my buddies' recommendation that I should do some digital artwork for their new
                                        website and send it to them to see what they say. I obliged and very much enjoyed
                                        creating this strange project for them. BTW this is the "commercial" version, the
                                        original is NSFW!
                            </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">

                                    <Link
                                        className="btn btn-secondary"
                                        to="/earthgang-an-html-art-project/">View the work
                                </Link>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <ReactiveIcon />
                                    <GreensockIcon />
                                    <Css3Icon />
                                    <NativeIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="paddedPortfolioImg img-fluid mt-5" alt="Earthgang illustrations and animations in React JS"
                                    src={earthgangImg2} />
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Earthgang: About this project
                    </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        The website is a fusion of multiple front end tech stacks. HTML5, CSS3, Bootstrap, and
                                        React. I created a single page application to improve my skillset here.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        I spent a lot of time coding the illustrations in React. It’s a very technical process,
                                        which required a lot of practice to perfect. The Greensock NPM is very delicate to use
                                        within a React App.

                        </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <Link
                                        className="btn btn-secondary"
                                        to="/earthgang-an-html-art-project/">View the work
                                </Link>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="paddedPortfolioImg img-fluid" alt="Earthgang illustrations and animations in React JS"
                                    src={earthgangImg3} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Conclusion about Earthgang.
                    </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        This thing is pure madness! <span role="img" aria-label="Smile Face">🙂</span> Note the
                                        pyramid that slowly flows gold lava throughout.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        Art takes on a whole new sphere when it comes to modern painting and illustration.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-12 col-lg-12">
                                        I hope you enjoyed the read my rarest of wild stallions.
                        </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <Link
                                        className="btn btn-secondary"
                                        to="/earthgang-an-html-art-project/">View the work
                                </Link>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="paddedPortfolioImg img-fluid mb-0 no-pad" alt="Earthgang illustrations and animations in React JS"
                                    src={earthgangImg4} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EarthGangPortfolio; 