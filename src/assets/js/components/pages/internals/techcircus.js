import React, { Component } from 'react';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';

//icons
import BootstrapIcon from '../../icons/bootstrap-icon.js';
import BitbucketIcon from '../../icons/bitbucket-icon.js';
import GulpIcon from '../../icons/gulp-icon.js';
import PhpIcon from '../../icons/php-icon.js';

import techcircusImg1 from '../../../../img/internals/techcircus/techcircus1-Archibald-Butler-Web-Development.jpg';
import techcircusImg2 from '../../../../img/internals/techcircus/techcircus2-Archibald-Butler-Web-Development.jpg';
import techcircusImg3 from '../../../../img/internals/techcircus/techcircus3-Archibald-Butler-Web-Development.jpg';

//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Angular JS"><div className="icon-roll angular-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="APIs"><div className="icon-roll api-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="BitBucket"><div className="icon-roll bitbucket-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Bootstrap"><div className="icon-roll bootstrap-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="CMS system"><div className="icon-roll cms-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="CPanel"><div className="icon-roll cpanel-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Css3 Animations"><div className="icon-roll css3-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title=".GIF images"><div className="icon-roll gif-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="GIT"><div className="icon-roll git-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Greensock"><div className="icon-roll greensock-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Grunt"><div className="icon-roll grunt-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Gulp"><div className="icon-roll gulp-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="HTML"><div className="icon-roll html-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="JQuery"><div className="icon-roll jquery-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="LESS"><div className="icon-roll less-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Native Javascript"><div className="icon-roll native-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Node JS"><div className="icon-roll node-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title=".PHP"><div className="icon-roll php-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="React JS"><div className="icon-roll react-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Sass Css"><div className="icon-roll sass-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="SEO"><div className="icon-roll seo-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="MySql"><div className="icon-roll sql-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="HTML5  Video"><div className="icon-roll video-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Wireframing"><div className="icon-roll wireframes-roll"></div></a>
//   <a aria-label="See work" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="Wordpress"><div className="icon-roll wordpress-roll"></div></a>

class TechCircus extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid portfolio">
        <div className="row no-pad">
          <div className="col-12 col-sm-12 col-md-6 col-lg-6">
            <div className="textbox">
              <h1 className="projHeaderTitle">
                TECHCIRCUS.IO: A UX CONFERENCE ORGANISER WEBSITE.
              </h1>
              <h3 className="projHeaderP">
                I was recently hired by the delightful team at Techcircus.io to
                help them with their new website.
              </h3>
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>

              <div className="clearfix">
                <BootstrapIcon />
                <BitbucketIcon />
                <GulpIcon />
                <PhpIcon />
              </div>
              <div className="clearfix">
                <a
                  aria-label="See work"
                  className="btn btn-secondary readmoreBTN"
                  href="https://techcircus.io/"
                >
                  View the work
                </a>
                <a aria-label="See work" href="#mainPara">
                  <div className="scroll-down-dude"></div>
                </a>
                <br className="c" />
                <br className="c" />
                <br className="c" />
              </div>
            </div>
          </div>
          <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
            <img
              className="img-fluid"
              alt="Kinesis Money: Archibald Butler Web Development"
              src={techcircusImg1}
            />
          </div>
        </div>
        <div className="container-fluid">
          <div className="row no-pad" id="mainPara">
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">1</div>
                <div className="text">
                  <p>the brief</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Working as a freelancer At Techcircus
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <a aria-label="See work" href="https://techcircus.io/">
                      Tech Circus
                    </a>{' '}
                    is a UX conference organiser. A wonderful startup founded by
                    the charismatic Luke Reed.
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    I was hired to take part in the redesign and development of
                    their business site. It was a pleasure to work with such a
                    friendly and creative bunch.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary clearfix"
                    href="https://techcircus.io/"
                  >
                    View the work
                  </a>
                </div>
              </div>
              <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                <h5 className="technoHeaderTitle">technologies used</h5>
                <div className="lineOnProject"></div>
                <div className="clearfix">
                  <BootstrapIcon />
                  <BitbucketIcon />
                  <GulpIcon />
                  <PhpIcon />
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="paddedPortfolioImg img-fluid"
                  alt="Earthgang illustrations and animations in React JS"
                  src={techcircusImg2}
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">2</div>
                <div className="text">
                  <p>the code</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Techcircus: About this project
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    The website is a fusion of multiple tech stacks, both back
                    and front end. HTML5, CSS3, Bootstrap, Php, MySql,
                    WordPress, ACF plus the incorporation of external CRMs. I
                    promised fast turn around and was awarded very good reviews.
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    Custom post types, and taxonomy filtration was the most
                    difficult obstacle in this project. I very much enjoyed
                    taking part in the design aspect too, the team have
                    excellent recommendations when it comes to layout and
                    functionality.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary clearfix"
                    href="https://techcircus.io/"
                  >
                    View the work
                  </a>
                </div>
              </div>
              <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                <img
                  className="paddedPortfolioImg img-fluid"
                  alt="Earthgang illustrations and animations in React JS"
                  src={techcircusImg3}
                />
              </div>
            </div>
            <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
              <div className="number" id="case-section-1">
                <div className="ring">3</div>
                <div className="text">
                  <p>conclusion</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
                  Conclusion about this UX event website.
                </h2>
                <div className="row">
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    I had the pleasure of learning a lot about UX and also
                    attended some of their conferences which I would highly
                    recommend. They are very talented team who are evolving
                    rapidly (get your CVs in on their website!).
                  </p>
                  <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                    Regarding the code base, it was a case of refining my
                    toolset to accomodate their needs as quickly as possible.
                    Automation tools like Grunt and Gulp are excellent tools for
                    this type of project.
                  </p>
                </div>
                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                  <a
                    aria-label="See work"
                    className="btn btn-secondary clearfix"
                    href="https://techcircus.io/"
                  >
                    View the work
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TechCircus;
