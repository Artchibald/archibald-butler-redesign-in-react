import React, { Component } from 'react';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';

import MonkAndFoxIllustrationImg1 from '../../../../img/internals/monk-and-fox-illustration/buddhist-monk-animal-spirit-illustration1.gif';
import MonkAndFoxIllustrationImg2 from '../../../../img/internals/monk-and-fox-illustration/buddhist-monk-animal-spirit-illustration2.jpg';
import MonkAndFoxIllustrationImg3 from '../../../../img/internals/monk-and-fox-illustration/buddhist-monk-animal-spirit-illustration3.jpg';
//icons
import WireframeIcon from '../../../../js/components/icons/wireframe-icon.js';

class MonkAndFoxIllustration extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Archie Butler’s cosmic planet illustration.</h1>
              <p>
                I just got a Macbook pro… I tried to replicate a drawing style I
                used to sketch as a kid…Whereby I would draw really vivid indian
                temples with felt tip pens. I remember loving the bright colors.
                My older sister asked me to make one for her caravan. My first
                commission ! XD
              </p>
              <br />
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>
              <div className="clearfix">
                <WireframeIcon />
              </div>
              <div className="clearfix"></div>
              <a
                aria-label="See work"
                className="btn btn-secondary"
                href="https://archibaldbutler.com/projects/cosmic-planet/buddhist-monk-animal-spirit-illustration4.png"
              >
                View the composition
              </a>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="Illustrator Archibald Butler"
                src={MonkAndFoxIllustrationImg1}
              />
              <figcaption>
                <h2>
                  A Cosmic <span>Temple</span>
                </h2>
                <p>Design and idea creation</p>
                <a
                  aria-label="See work"
                  href="https://archibaldbutler.com/projects/cosmic-planet/buddhist-monk-animal-spirit-illustration4.png"
                >
                  View more
                </a>
              </figcaption>
            </figure>
          </div>
        </div>
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="Illustrator Archibald Butler"
                src={MonkAndFoxIllustrationImg2}
              />
              <figcaption>
                <h2>
                  A Cosmic <span>Temple</span>
                </h2>
                <p>Design and idea creation</p>
                <br />
                <a href="https://archibaldbutler.com/projects/cosmic-planet/buddhist-monk-animal-spirit-illustration4.png">
                  View the composition
                </a>
              </figcaption>
            </figure>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Layering that PSD!</h1>
              <p>
                I was cutting up images from all sorts of different places to
                interpret the unique vision I had for the composition.
              </p>
              <br />
            </div>
          </div>
        </div>
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Conclusion</h1>
              <p>
                I really like the colors, I remember using a lot of purple when
                I was a kid(Wish I could find them again!). I am totally in love
                with this image I created in 3-4 hours over the weekend. It was
                fun because I visualized it then I was able to just recreate it
                from a selection of images. What do you think?
              </p>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="A web design illustration by Archibald Butler"
                src={MonkAndFoxIllustrationImg3}
              />
              <figcaption>
                <h2>
                  A Cosmic <span>Temple</span>
                </h2>
                <p>Design and idea creation</p>
                <br />
                <a href="https://archibaldbutler.com/projects/cosmic-planet/buddhist-monk-animal-spirit-illustration4.png">
                  View the composition
                </a>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    );
  }
}

export default MonkAndFoxIllustration;
