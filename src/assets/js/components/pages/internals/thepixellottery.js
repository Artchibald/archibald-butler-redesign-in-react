import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

//icons
import CmsIcon from '../../icons/cms-icon.js';
import NativeIcon from '../../icons/native-icon.js';
import PhpIcon from '../../icons/php-icon.js';
import ApiIcon from '../../icons/api-icon.js';
import HtmlIcon from '../../icons/html-icon.js';
import CpanelIcon from '../../icons/cpanel-icon.js';

//img
import ThePixelLotteryImg1 from '../../../../img/internals/the-pixel-lottery/the-pixel-lottery1.jpg';
import ThePixelLotteryImg2 from '../../../../img/internals/the-pixel-lottery/the-pixel-lottery2.jpg';
import ThePixelLotteryImg3 from '../../../../img/internals/the-pixel-lottery/the-pixel-lottery3.jpg';


class ThePixelLottery extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }



    render() {
        return (
            <div className="container-fluid portfolio">
                <div className="row no-pad">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1 className="projHeaderTitle">A custom Drupal project in partnership with <a href="http://ugli.london/">UGLI</a>:</h1>
                            <h3 className="projHeaderP">1 chance in one million of winning 1 million dollars for $1,40.</h3>
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>

                            <div className="clearfix">
                                <CmsIcon />
                                <NativeIcon />
                                <PhpIcon />
                                <ApiIcon />
                                <HtmlIcon />
                                <CpanelIcon />
                            </div>
                            <div className="clearfix">
                                <a className="btn btn-secondary readmoreBTN" href="http://the-pixel-lottery.com/">
                                    View the work
                    </a>
                                <a href="#mainPara">
                                    <div className="scroll-down-dude"></div>
                                </a>
                                <br className="c" />
                                <br className="c" />
                                <br className="c" />
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
                        <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={ThePixelLotteryImg1} />
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row no-pad" id="mainPara">
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    1
                    </div>
                                <div className="text">
                                    <p>AN ORIGINAL IDEA… EXECUTED!</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">I really wanted this website built!
                    </h2>
                                <div className="row">
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        The pixel lottery is an idea I came up with almost 10 years ago now. I really like the idea of the million dollar homepage and drew inspiration from that to create a new kind of game. After carefully debating on the subject with a close friend of mine I came up with the concept of the pixel lottery.
                        </p>
                                    <p className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        I really like the idea of having an image that is 1000 pixels by 1000 pixels totalling exactly 1,000,000 squares within.
                        <br /><br />
                                        In JavaScript, it is very difficult to write something that has 1 million different clickable objects in it.
                        </p>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="http://the-pixel-lottery.com/">
                                        View the work
                        </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
                                <h5 className="technoHeaderTitle">technologies used</h5>
                                <div className="lineOnProject"></div>
                                <div className="clearfix">
                                    <CmsIcon />
                                    <NativeIcon />
                                    <PhpIcon />
                                    <ApiIcon />
                                    <HtmlIcon />
                                    <CpanelIcon />
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid mt-5" alt="Kinesis Money: Archibald Butler Web Development" src={ThePixelLotteryImg2} />
                            </div>
                        </div>

                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    2
                    </div>
                                <div className="text">
                                    <p>the code</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">About this lottery project
                    </h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Theory:</h3>
                                        <p>
                                            For this I did require some extra help from a developer and friend of mine called Tom Fallowfield. He is a highly skilled backend web developer.
                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <h3>Working on it:</h3>
                                        <p>
                                            We started the partnership to build up this site and get it to where it is today. I am very excited at the fact that it is fully live and working on the web.
                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="http://the-pixel-lottery.com/">
                                        View the work
                        </a>
                                </div>
                            </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
                                <img className="img-fluid" alt="Kinesis Money: Archibald Butler Web Development" src={ThePixelLotteryImg3} />
                            </div>
                        </div>
                        <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
                            <div className="number" id="case-section-1">
                                <div className="ring">
                                    3
                    </div>
                                <div className="text">
                                    <p>conclusion</p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
                                <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">Advanced Drupal development</h2>
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            This is an incredible piece of work in my eyes, building something bespoke like this is not an easy process. A lot of thought went into the design although people’s views on it are varied!
                        </p>
                                        <p>
                                            I was going for a hypsterised vibe with a gentlemanly flare to exult  a feeling of wealthiness.
                        </p>
                                    </div>
                                    <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                        <p>
                                            Although at present I have not made any sales. I have done no marketing whatsoever on this website I’m just satisfied that it is built and online and indexed on Google with the keyword search term “pixel lottery”.
                        </p>
                                        <p>
                                            I hope you enjoyed reading about making an illustrated website as much as I did creating it.
                        </p>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
                                    <a className="btn btn-secondary clearfix" href="http://the-pixel-lottery.com/">
                                        View the work
                        </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ThePixelLottery; 