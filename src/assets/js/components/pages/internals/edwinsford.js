import React, { Component } from 'react';
import $ from 'jquery';
import '../../../../css/i_portfolio_internal_styles.scss';

import EdwinsfordImg1 from '../../../../img/internals/edwinsford/edwinsford1.jpg';
import EdwinsfordImg2 from '../../../../img/internals/edwinsford/edwinsford2.jpg';
import EdwinsfordImg3 from '../../../../img/internals/edwinsford/edwinsford3.jpg';
//icons
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';

class Edwinsford extends Component {
  componentDidMount() {
    //make sure we load at top of page
    window.scrollTo(0, 0);
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>The Edwinsford Estate</h1>
              <p>
                Here is a recent underwater website design I created in
                Photoshop. The design is going to be Iphone responsive in the
                coded version. The moving fish were added using Jquery, a
                language I have been wanting to look into in more depth for
                quite a while.
              </p>
              <p>
                The animation is also IPhone compatible, an essential
                requirement in today’s market. I love the underwater world
                (perhaps because I am a pisces!). Feel free to get in touch if
                you have any questions about how I made this website template.
              </p>
              <p>
                The underwater theme is predominant taking a lot of time to
                create.
              </p>
              <br />
              <br />
              <h5 className="technoHeaderTitle">technologies used</h5>
              <div className="lineOnProject"></div>
              <div className="clearfix">
                <Css3Icon />
                <ReactiveIcon />
                <SeoIcon />
                <NativeIcon />
              </div>
              <div className="clearfix"></div>
              <a
                aria-label="See artwork"
                className="btn btn-secondary"
                href="https://archibaldbutler.com/projects/edwinsfordestate2/index.html"
              >
                View the composition
              </a>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img alt="Illustrator Archibald Butler" src={EdwinsfordImg1} />
              <figcaption>
                <h2>
                  A Good Day of <span>Work</span>
                </h2>
                <p> Website illustration and animation techniques</p>

                <a
                  aria-label="See artwork"
                  href="https://archibaldbutler.com/projects/edwinsfordestate2/index.html"
                >
                  View more
                </a>
              </figcaption>
            </figure>
          </div>
        </div>
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img alt="Illustrator Archibald Butler" src={EdwinsfordImg2} />
              <figcaption>
                <h2>
                  The Edwinsford <span>Estate</span>
                </h2>
                <p> Website illustration and animation techniques</p>
                <br />
                <a href="https://archibaldbutler.com/projects/edwinsfordestate2/index.html">
                  View the composition
                </a>
              </figcaption>
            </figure>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Layering that PSD!</h1>
              <p>
                If you want to create a fully bespoke design like this yourself,
                then I suggest that you get familiar with using Photoshop.
              </p>
              <p>
                It is the key tool for web designers today. This particular
                website template took me about two days to put together. As I
                like to emphasize my work spending that little bit of extra time
                creating something truly unique. If you would like to get a copy
                of the files I used when creating this, feel free to{' '}
                <a aria-label="See artwork" href="/contact/">
                  get in touch
                </a>{' '}
                with me via the contact page or by posting a response under this
                article.
              </p>
              <br />
            </div>
          </div>
        </div>
        <div className="row no-pad">
          <div className="col-12 col-sm-6 col-md-6 col-lg-6">
            <div className="textbox">
              <h1>Conclusion</h1>
              <p>
                Feel free to take a moment to check out some of my other work
                and download some of my templates by going to the{' '}
                <a aria-label="See artwork" href="/news-1/">
                  news
                </a>{' '}
                section.
              </p>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
            <figure className="effect-roxy">
              <img
                alt="A web design illustration by Archibald Butler"
                src={EdwinsfordImg3}
              />
              <figcaption>
                <h2>
                  The Edwinsford <span>Estate</span>
                </h2>
                <p> Website illustration and animation techniques</p>
                <br />
                <a href="https://archibaldbutler.com/projects/edwinsfordestate2/index.html">
                  View the composition
                </a>
              </figcaption>
            </figure>
          </div>
        </div>
      </div>
    );
  }
}

export default Edwinsford;
