import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import AdvancedPhotoshop1 from '../../../../img/internals/advanced-photoshop/advanced.jpg';
//icons
import HtmlIcon from '../../../../js/components/icons/html-icon.js';
import JqueryIcon from '../../../../js/components/icons/jquery-icon.js';
import WireframeIcon from '../../icons/wireframe-icon.js';
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import ReactiveIcon from '../../../../js/components/icons/reactive-icon.js';


class AdvancedPhotoshop extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Advanced Photoshop Magazine interviewed Archie Butler!</h1>
                            <p>They called me up to ask if I wanted to be interviewed for an article on wood textures. I accepted instantly.</p>
                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <HtmlIcon />
                                <JqueryIcon />
                                <WireframeIcon />
                                <Css3Icon />
                                <ReactiveIcon />
                            </div>
                            <br className="clearfix" />
                            <br className="clearfix" />
                            <br className="clearfix" />
                            <br className="clearfix" />
                            <a className="btn btn-secondary"
                                href="https://archibaldbutler.com/projects/ADVANCED-PHOTOSHOP-MAG/Archibald-Butler-mentionned-in-Advanced-Photoshop-magazine.jpg">Read The article</a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={AdvancedPhotoshop1} />
                            <figcaption>
                                <h2><span>Advanced</span> Photoshop Magazine</h2>
                                <p>Award claimed!</p>
                                <a className="btn btn-secondary"
                                    href="https://archibaldbutler.com/projects/ADVANCED-PHOTOSHOP-MAG/Archibald-Butler-mentionned-in-Advanced-Photoshop-magazine.jpg">Read The article</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default AdvancedPhotoshop; 