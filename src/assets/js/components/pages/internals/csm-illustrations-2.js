import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";

import csmImg31 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-31.jpg";
import csmImg32 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-32.jpg";
import csmImg33 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-33.jpg";
import csmImg34 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-34.jpg";
import csmImg35 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-35.jpg";
import csmImg36 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-36.jpg";
import csmImg37 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-37.jpg";
import csmImg38 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-38.jpg";
import csmImg39 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-39.jpg";
import csmImg40 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-40.jpg";
import csmImg41 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-41.jpg";
import csmImg42 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-42.jpg";
import csmImg43 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-43.jpg";
import csmImg44 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-44.jpg";
import csmImg45 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-45.jpg";
import csmImg46 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-46.jpg";
import csmImg47 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-47.jpg";
import csmImg48 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-48.jpg";
import csmImg49 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-49.jpg";
import csmImg50 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-50.jpg";
import csmImg51 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-51.jpg";
import csmImg52 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-52.jpg";
import csmImg53 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-53.jpg";
import csmImg54 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-54.jpg";
import csmImg55 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-55.jpg";
import csmImg56 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-56.jpg";
import csmImg57 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-57.jpg";
import csmImg58 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-58.jpg";
import csmImg59 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-59.jpg";
import csmImg60 from "../../../../img/internals/csm-artwork/archibald-butler-illustration-60.jpg";
import chevronLeftImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class csmArtwork2 extends Component {
 componentDidMount() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid">
    <div className="row no-pad">
     <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h1 className="text-center mt-5">Seriously. What was I on??</h1>
      <p className="text-center mb-5">
       My mind works in mysterious ways. On on.
      </p>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg31}
       />
       <figcaption>
        <h2>
         Deep rooted dream of <span>riots.</span>
        </h2>
        <p>This was a Banksy bomb.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-31.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg32}
       />
       <figcaption>
        <h2>
         Wow, <span>drugs.</span>
        </h2>
        <p>No comment.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-32.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg33}
       />
       <figcaption>
        <h2>
         Many were <span>done…</span>
        </h2>
        <p>With no pencil draft.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-33.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg34}
       />
       <figcaption>
        <h2>
         A sleeping <span>beauty.</span>
        </h2>
        <p>A life session BIC portrait.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-34.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg35}
       />
       <figcaption>
        <h2>
         Alphabets <span>everywhere.</span>
        </h2>
        <p>Trying to find letters in London’s landscape.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-35.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg36}
       />
       <figcaption>
        <h2>
         Rastaman prayer <span>tiiiing.</span>
        </h2>
        <p>Jah bless rudeboi.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-36.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg37}
       />
       <figcaption>
        <h2>
         Cosmic <span>Alien.</span>
        </h2>
        <p>Love your face shape, obscure fragment of my mind.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-37.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg38}
       />
       <figcaption>
        <h2>
         Wow, <span>This is colourful.</span>
        </h2>
        <p>Sensing some real graphic torment here.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-38.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg39}
       />
       <figcaption>
        <h2>
         This will have <span>been done…</span>
        </h2>
        <p>During a class I was bored in.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-39.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg40}
       />
       <figcaption>
        <h2>
         Was this meant to be <span>me?</span>
        </h2>
        <p>Bizarre, no doubt.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-40.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg41}
       />
       <figcaption>
        <h2>
         Samurai <span>Tears.</span>
        </h2>
        <p>Oceans of pain…. Deep.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-41.jpg">
         See it
        </a>
       </figcaption>
      </figure>

      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg42}
       />
       <figcaption>
        <h2>
         Sketching in my <span>garden.</span>
        </h2>
        <p>Documenting the everyday</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-42.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg43}
       />
       <figcaption>
        <h2>
         My oldest friend: <span>Louis Drummond Kelly.</span>
        </h2>
        <p>A quick draw up, no drafts</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-43.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg44}
       />
       <figcaption>
        <h2>
         A rough <span>self portrait</span>
        </h2>
        <p>Through the looking glass.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-44.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg45}
       />
       <figcaption>
        <h2>
         St <span>Michael</span>
        </h2>
        <p>Random sketch, no model.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-45.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg46}
       />
       <figcaption>
        <h2>
         Heathrow <span>Departure terminal.</span>
        </h2>
        <p>From when I had nothing to spend.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-46.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg47}
       />
       <figcaption>
        <h2>
         Testing out <span>some pens.</span>
        </h2>
        <p>Good for subtle textures.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-47.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg48}
       />
       <figcaption>
        <h2>
         A surfer’s <span>Dream.</span>
        </h2>
        <p>
         I used to trip out on water as I fell asleep after a big day of
         surfing.
        </p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-48.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg49}
       />
       <figcaption>
        <h2>
         The <span>Illuminati.</span>
        </h2>
        <p>Behind the lizard’s eye…ooooh</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-49.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg50}
       />
       <figcaption>
        <h2>
         Random <span>and from the mind.</span>
        </h2>
        <p>A spirit from the spectral plane perhaps.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-50.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg51}
       />
       <figcaption>
        <h2>
         My <span>old kitchen.</span>
        </h2>
        <p>It burnt down once.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-51.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg52}
       />
       <figcaption>
        <h2>
         Good <span>speed feel.</span>
        </h2>
        <p>Simplified background too.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-52.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg53}
       />
       <figcaption>
        <h2>
         Pausing <span>Skate videos.</span>
        </h2>
        <p>Then drawing them.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-53.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg54}
       />
       <figcaption>
        <h2>
         Pausing <span>Skate videos 2.</span>
        </h2>
        <p>Then drawing them.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-54.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg55}
       />
       <figcaption>
        <h2>
         A cover for an art <span>project.</span>
        </h2>
        <p>Choose some celebrities and document them.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-55.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg56}
       />
       <figcaption>
        <h2>
         James <span>Dean.</span>
        </h2>
        <p>I chose a ladies’ man obvs :).</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-56.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg57}
       />
       <figcaption>
        <h2>
         About <span>James Dean.</span>
        </h2>
        <p>Fascinated with his need for speed.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-57.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg58}
       />
       <figcaption>
        <h2>
         Push Pin <span>Studios.</span>
        </h2>
        <p>I love the desk kid.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-58.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg59}
       />
       <figcaption>
        <h2>
         How my <span>mind works.</span>
        </h2>
        <p>These dudes haunt me every night.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-59.jpg">
         See it
        </a>
       </figcaption>
      </figure>
      <figure className="effect-roxy">
       <img
        className="img-fluid"
        alt="illustration by Archibald Butler at Central Saint Martis Art school"
        src={csmImg60}
       />
       <figcaption>
        <h2>
         More from <span>Push Pin.</span>
        </h2>
        <p>I liked their work a lot.</p>
        <a href="https://archibaldbutler.com/all-illustrations-from-art-school-2013/archibald-butler-illustration-60.jpg">
         See it
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-1 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <a className="previous-link" href="/csm-artwork">
        <div className="animate1">
         <img
          className="img-fluid"
          src={chevronLeftImg}
          alt="Chevron left: Previous page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
     <div className="offset-col-10 col-1 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <a className="next-link" href="/csm-artwork-3">
        <div className="animate2">
         <img
          className="img-fluid"
          src={chevronRightImg}
          alt="Chevron right: Next page of old illustrations"
         />
        </div>
       </a>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default csmArtwork2;
