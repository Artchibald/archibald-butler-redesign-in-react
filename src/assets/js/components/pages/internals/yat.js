import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
//icons
import ApiIcon from "../../icons/api-icon.js";
import NodeIcon from "../../icons/node-icon.js";
import Css3Icon from "../../icons/css3-icon";
import ReactiveIcon from "../../icons/reactive-icon";
// img
import yatHero from "../../../../img/internals/yat/yat.png";
import yat1 from "../../../../img/internals/yat/yat1.png";
import yat2 from "../../../../img/internals/yat/yat2.png";
import yat3 from "../../../../img/internals/yat/yat3.png";
import yatVid from "../../../../img/video/yat2.mp4";

class yat extends Component {
 componentDidMount() {
  //make sure we load at top of page
  window.scrollTo(0, 0);
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid portfolio">
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-6 col-lg-6">
      <div className="textbox">
       <h1 className="projHeaderTitle">Y.AT</h1>
       <h3 className="projHeaderP">Emotionally Emitive Emojis!</h3>
       <br />
       <h5 className="technoHeaderTitle">technologies used</h5>
       <div className="lineOnProject"></div>

       <div className="clearfix">
        {/*                                 <AngularIcon />
                                <ApiIcon />
                                <BitbucketIcon />
                                <BootstrapIcon />
                                <CmsIcon />
                                <CpanelIcon />
                                <Css3Icon />
                                <GifIcon />
                                <GitIcon />
                                <GreensockIcon />
                                <GruntIcon />
                                <GulpIcon />
                                <HtmlIcon />
                                <JqueryIcon />
                                <LessIcon />
                                <NativeIcon />
                                <NodeIcon />
                                <PhpIcon />
                                <ReactiveIcon />
                                <SassIcon />
                                <SeoIcon />
                                <SqlIcon />
                                <VideoIcon />
                                <WireframeIcon />
                                <WordpressIcon /> */}
        <ApiIcon />
        <NodeIcon />
        <Css3Icon />
        <ReactiveIcon />
       </div>
       <div className="clearfix">
        <a className="btn btn-secondary readmoreBTN inline" rel="nofollow" href="https://y.at">
         View website
        </a>

        <a href="#mainPara">
         <div className="scroll-down-dude"></div>
        </a>
        <br className="c" />
        <br className="c" />
        <br className="c" />
       </div>
      </div>
     </div>
     <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
      <img className="img-fluid" alt="y.at" src={yatHero} />
     </div>
    </div>
    <div className="video_wrapper screen_bordered container-fluid">
     <div className="row no-pad">
      <video className="w-100" autoPlay loop muted>
       <source src={yatVid} type="video/mp4" />
      </video>
     </div>
    </div>
    <div className="container-fluid">
     <div className="row no-pad" id="mainPara">
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">1</div>
        <div className="text">
         <p>I designed and built a website for the crypto sector again!</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         Working for US based company y.at was
         flipping awesome!
        </h2>
        <div className="row">
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          I had to jump into a complex codebase environment with
          Typescript, React, and all the other rather complex bells and
          whistles.
         </p>
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          My revisit of Microsoft Teams was not the most enjoyable either! Jira
          boards and tight deadlines, some challenging work at times.
         </p>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a className="btn btn-secondary clearfix" rel="nofollow" href="https://y.at">
          View the work
         </a>
        </div>
       </div>
       <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
        <h5 className="technoHeaderTitle">technologies used</h5>
        <div className="lineOnProject"></div>
        <div className="clearfix">
         <ApiIcon />
         <NodeIcon />
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img className="img-fluid mt-5" alt="y.at" src={yat1} />
       </div>
      </div>

      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">2</div>
        <div className="text">
         <p>the code</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         Highly functional based components throughout made picking up the
         tickets&nbsp;difficult.
        </h2>
        <div className="row">
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Theory:</h3>
          <p>
           Gave my very best at recommending UI implementations and boxing up
           the parts that I could while I waited for more proficiency.
          </p>
         </div>
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Working on it:</h3>
          <p>
           Code like takes acclimatizing to. How is the tree structured? Where is
           this component? Sometimes the rabbit hole is rather deep.
          </p>
         </div>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a className="btn btn-secondary clearfix" rel="nofollow" href="https://y.at">
          View the work
         </a>
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img className="img-fluid" alt="y.at" src={yat3} />
       </div>
      </div>
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">3</div>
        <div className="text">
         <p>conclusion</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-4">
         I saw some excellent implementations of Greensock here, such as
         incredible draggable UX and UI boxes that were really marvellous from a
         usability perspective!
        </h2>
        <div className="row">
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <p>
           This codebase was using all the latest tech stacks, I worked there helping get tickets over the line, a lovely team there too.
          </p>
         </div>
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <p>
           It took me some time to get use to this environment but I do love a
           good challenge. Thanks for the opportunity!
          </p>
         </div>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a className="btn btn-secondary clearfix" rel="nofollow" href="https://y.at">
          View the work
         </a>
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img className="img-fluid" alt="y.at" src={yat2} />
       </div>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default yat;
