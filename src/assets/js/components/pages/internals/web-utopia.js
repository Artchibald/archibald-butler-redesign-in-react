import React, { Component } from 'react';
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import WebUtopiaImg1 from '../../../../img/internals/web-utopia/web-utopia1.jpg';
import WebUtopiaImg2 from '../../../../img/internals/web-utopia/web-utopia2.jpg';
import WebUtopiaImg3 from '../../../../img/internals/web-utopia/web-utopia3.jpg';


class WebUtopia extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>The Web Utopia Design</h1>
                            <p>
                                This was an intial sketch for this website I was thinking of creating a few years back. 
                                This is my dream home! That is what inspired me to draw it. I used a collage of photography for the background image, that I then vectorized to give them that illustrated look (not enough time to hand draw that part!).
                        <br /><br />
                                I then hand drew all the elements as I usually do in my sketchbook. I would love to add accuracy to the more finite details but these things take up so much time. Plus the rent in London isn’t cheap… I then photographed them, sharpened, to bring out the byro lines I had drawn previously.
                        </p>
                            <br />
                            <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/web-design-templates/archiesutopia/">View the work!</a>
                            <a className="btn btn-secondary readmoreBTN d-inline" href="https://archibaldbutler.com/free-photoshop-downloads/cartoon-website-illustration-template.zip">Download it!</a>

                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler" src={WebUtopiaImg1} />
                            <figcaption>
                                <h2>Web <span>Utopia</span></h2>
                                <p>Web Design and development</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler" src={WebUtopiaImg2} />
                            <figcaption>
                                <h2>Web <span>Utopia</span></h2>
                                <p>Web Design and development</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>A good days work</h1>
                            <p>This one uses a mix of both traditional illustration by myself as well as collage.</p>
                            <br />
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Conclusion</h1>
                            <p>It is ok but needs more work!</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler" src={WebUtopiaImg3} />
                            <figcaption>
                                <h2>Web <span>Utopia</span></h2>
                                <p>Web Design and development</p>
                                <br />

                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default WebUtopia;