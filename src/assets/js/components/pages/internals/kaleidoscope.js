import React, { Component } from 'react';
import { BrowserRouter as Route, a } from "react-router-dom";
import $ from "jquery";
import '../../../../css/i_portfolio_internal_styles.scss';

import KaleidoscopeImg1 from '../../../../img/internals/kaleidoscope/kaleidoscope1.jpg';
import KaleidoscopeImg2 from '../../../../img/internals/kaleidoscope/kaleidoscope2.jpg';
import KaleidoscopeImg3 from '../../../../img/internals/kaleidoscope/kaleidoscope3.jpg';
//icons
import Css3Icon from '../../../../js/components/icons/css3-icon.js';
import NativeIcon from '../../../../js/components/icons/native-icon.js';
import ReactiveIcon from '../../icons/reactive-icon.js';
import SeoIcon from '../../icons/seo-icon.js';


class Kaleidoscope extends Component {

    componentDidMount() {
        //make sure we load at top of page 
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }

    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }

    render() {
        return (
            <div className="container-fluid">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>The Javascript Kaleidoscope.</h1>
                            <p>An idea I cam up with with a friend at work.

I developed it within a couple of hours in my spare time thanks to some help online.</p>

                            <br />
                            <br />
                            <h5 className="technoHeaderTitle">technologies used</h5>
                            <div className="lineOnProject"></div>
                            <div className="clearfix">
                                <Css3Icon />
                                <ReactiveIcon />
                                <SeoIcon />
                                <NativeIcon />
                            </div>
                            <div className="clearfix" ></div>
                            <a className="btn btn-secondary"
                                href="https://archibaldbutler.com/projects/kaleidoscope/">View the thing move! <i class="fas fa-glasses"></i></a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={KaleidoscopeImg1} />
                            <figcaption>
                                <h2>A Good Day of <span>Work</span></h2>
                                <p> Website illustration and animation techniques</p>

                                <a href="https://archibaldbutler.com/projects/kaleidoscope/">View more</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler"
                                src={KaleidoscopeImg2} />
                            <figcaption>
                                <h2>The Kaleidoscope <span>Estate</span></h2>
                                <p> Website illustration and animation techniques</p>
                                <br />
                                <a
                                    href="https://archibaldbutler.com/projects/kaleidoscope/">Enter</a>
                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>About the build</h1>
                            <p>This is essentially is a duplicated bg image, that moves across its div at a diagonal angle. A script insures the animation moves when the mouse does.</p>
                            <br />
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Conclusion</h1>
                            <p>It could be touch sensitive too with some work which would be fun!</p>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler"
                                src={KaleidoscopeImg3} />
                            <figcaption>
                                <h2>The Kaleidoscope <span>Estate</span></h2>
                                <p> Website illustration and animation techniques</p>
                                <br />
                                <a
                                    href="https://archibaldbutler.com/projects/kaleidoscope/">Enter</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}

export default Kaleidoscope; 