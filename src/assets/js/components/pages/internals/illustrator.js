import React, { Component } from "react";
import $ from "jquery";
import "../../../../css/i_portfolio_internal_styles.scss";
//icons
import ApiIcon from "../../icons/api-icon.js";
import NodeIcon from "../../icons/node-icon.js";

// img
import illustratorTop from "../../../../img/internals/illustrator/illustrator.png";
import illustrator1 from "../../../../img/internals/illustrator/showcase_illustrator.png";
import illustrator2 from "../../../../img/internals/illustrator/showcase_illustrator_2.png";
import illustratorRecording from "../../../../img/video/illustrator.mp4";
class illustrator extends Component {
 componentDidMount() {
  //make sure we load at top of page
  window.scrollTo(0, 0);
  $('[data-toggle="tooltip"]').tooltip();
 }

 componentDidUpdate() {
  $('[data-toggle="tooltip"]').tooltip();
 }

 render() {
  return (
   <div className="container-fluid portfolio">
    <div className="row no-pad">
     <div className="col-12 col-sm-12 col-md-6 col-lg-6">
      <div className="textbox">
       <h1 className="projHeaderTitle">Illustrator Adobe Script</h1>
       <h3 className="projHeaderP">Typescript rules!</h3>
       <br />
       <h5 className="technoHeaderTitle">technologies used</h5>
       <div className="lineOnProject"></div>

       <div className="clearfix">
        <ApiIcon />
        <NodeIcon />
       </div>
       <div className="clearfix">
        <a
         className="btn btn-secondary readmoreBTN inline"
         href="https://github.com/Artchibald/2022_icon_rebrand_scripts"
        >
         Github repo
        </a>

        <a href="#mainPara">
         <div className="scroll-down-dude"></div>
        </a>
        <br className="c" />
        <br className="c" />
        <br className="c" />
       </div>
      </div>
     </div>
     <div className="col-12 col-sm-12 col-md-6 col-lg-6 no-pad">
      <img className="img-fluid" alt="itrust.finance" src={illustratorTop} />
     </div>
    </div>
    <div className="video_wrapper screen_bordered container-fluid">
     <div className="row no-pad">
      <video className="w-100" autoPlay loop muted>
       <source src={illustratorRecording} type="video/mp4" />
      </video>
     </div>
    </div>
    <div className="container-fluid">
     <div className="row no-pad" id="mainPara">
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">1</div>
        <div className="text">
         <p>
          Exploring Adobe script again with typescript under my belt was
          awesome!
         </p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-6 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
         The brief was to automate exports of icons across a variety of formats
         such as EPS, JPG, PNG, and SVG.
        </h2>
        <div className="row">
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          Spent a lot of time on the Adobe Forum (https://community.adobe.com/
          and https://ai-scripting.docsforadobe.dev/) to understand the API
          docs.
         </p>
         <p className="col-12 col-sm-12 col-md-6 col-lg-6">
          It is actually really satisfying to fix this type of script because it
          is quite easy to understand and the community is really helpful.
         </p>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a
          className="btn btn-secondary clearfix"
          href="https://github.com/Artchibald/2022_icon_rebrand_scripts"
         >
          View the work
         </a>
        </div>
       </div>
       <div className="col-12 col-sm-12 offset-md-1 col-md-3 offset-lg-1 col-lg-2 mt-5">
        <h5 className="technoHeaderTitle">technologies used</h5>
        <div className="lineOnProject"></div>
        <div className="clearfix">
         <ApiIcon />
         <NodeIcon />
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
        <img
         className="img-fluid mt-5"
         alt="itrust.finance"
         src={illustrator1}
        />
       </div>
      </div>

      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">2</div>
        <div className="text">
         <p>the code</p>
        </div>
       </div>
      </div>
      <div className="row">
       <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
        <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         I did have some major blockers throughout but they were resolvable with
         all the ressources. Lots of typescript interfaces, reusable functions
         and coordinate based systems for Illustrator.
        </h2>
        <div className="row">
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Theory:</h3>
          <p>
           Exporting from typescript to ECMA3 was a really satisfying framework
           that I created from scratch. See <br />
           - https://github.com/Artchibald/WTW-illustrator-script <br />-
           https://github.com/Artchibald/2022_icon_rebrand_scripts
          </p>
         </div>
         <div className="col-12 col-sm-12 col-md-6 col-lg-6">
          <h3>Working on it:</h3>
          <p>
           Once my compiler, tsconfig, git and node were all set up, it worked
           like a dream. Had to work on someone else's code from the start, so
           getting accustomised took a few days.
          </p>
         </div>
        </div>
        <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
         <a
          className="btn btn-secondary clearfix"
          href="https://github.com/Artchibald/WTW-illustrator-script"
         >
          View the work
         </a>
        </div>
       </div>
      </div>
      <div className="col-12 col-sm-12 col-md-12 col-lg-12 no-pad">
       <img className="img-fluid" alt="itrust.finance" src={illustrator2} />
      </div>
      <div className="col-12 col-sm-12 offset-md-2 col-md-10  offset-lg-2 col-lg-10 mt-5">
       <div className="number" id="case-section-1">
        <div className="ring">3</div>
        <div className="text">
         <p>conclusion</p>
        </div>
       </div>
      </div>
      <div className="col-12 col-sm-12 col-md-6 offset-md-3 col-lg-8 offset-lg-2 mt-5">
       <h2 className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad">
        Loving Adobe Script!
       </h2>
       <div className="row">
        <div className="col-12 col-sm-12 col-md-6 col-lg-6">
         <p>Watch out designers, I am coming for your jobs! XD</p>
        </div>
        <div className="col-12 col-sm-12 col-md-6 col-lg-6">
         <p>Automation is a repetitive but enjoyable codebase to work with.</p>
        </div>
       </div>
       <div className="col-12 col-sm-12 col-md-12 col-lg-12 clearfix no-pad mb-5">
        <a
         className="btn btn-secondary clearfix"
         href="https://github.com/Artchibald/WTW-illustrator-script"
        >
         View the work
        </a>
       </div>
      </div>
     </div>
    </div>
   </div>
  );
 }
}

export default illustrator;
