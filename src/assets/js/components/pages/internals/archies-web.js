import React, { Component } from 'react';
import $ from "jquery";
import { BrowserRouter as Route, Link } from "react-router-dom";
import '../../../../css/i_portfolio_internal_styles.scss';

import ArchieWebImg1 from '../../../../img/internals/archies-web/archies-web1.jpg';
import ArchieWebImg2 from '../../../../img/internals/archies-web/archies-web2.jpg';
import ArchieWebImg3 from '../../../../img/internals/archies-web/archies-web3.jpg';

class ArchieWeb extends Component {

    componentDidMount() {
        //make sure we load at top of page
        window.scrollTo(0, 0);
        $('[data-toggle="tooltip"]').tooltip();
    }
    componentDidUpdate() {
        $('[data-toggle="tooltip"]').tooltip();
    }
    render() {
        return (
            <div className="container-fluid">
                <Route />
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>This one was a turning point!</h1>
                            <p>
                                I got contracted about no other than ADVANCED PHOTOSHOP MAGAZINE!
                                  <br /><br />
                                <Link className="btn btn-secondary readmoreBTN" to="/advanced-photoshop/">Read the article</Link>
                                <br /><br />
                                Download a free photoshop web template full of woody textures! A great design to give your website a more natural feel. Did you know that if you have a photoshop file website, I can make it into a working online site? Send me an email to archie@archibaldbutler.com.
                                  <br /><br />
                                I wanted to portray a real desk feel with lots of illustration when I created this piece of work. I will be posting a lot more photoshop web design templates to my blog soon so keep an eye out.
                                  Please feel free to download the files for this photoshop website for free!
                                  <br /><br />
                                I wanted to portray a real desk feel with lots of illustration when I created this piece of work. I will be posting a lot more photoshop web design templates to my blog soon so keep an eye out.
                              Please feel free to download the files for this photoshop website for free!
                             </p>
                            <br />
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/web-design-templates/archibald-butler/">View the work!</a>
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/free-photoshop-downloads/wood-woody-web-template-photoshop-free-download.zip">Download
                    it!</a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler" src={ArchieWebImg1} />
                            <figcaption>
                                <h2><span>The Archie’s web design</span></h2>
                                <p> Website illustration and animation techniques</p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="Illustrator Archibald Butler" src={ArchieWebImg2} />
                            <figcaption>
                                <h2><span>The Archie’s web design</span></h2>
                                <p> Website illustration and animation techniques</p>
                            </figcaption>
                        </figure>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>This was one my first recognized awards in design.</h1>
                            <p>
                                I was chuffed <span aria-label="happy face" role="img">🙂</span>
                            </p>
                            <br />
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/web-design-templates/archibald-butler/">View the work!</a>
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/free-photoshop-downloads/wood-woody-web-template-photoshop-free-download.zip">Download
                    it!</a>v
                        </div>
                    </div>
                </div>
                <div className="row no-pad">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div className="textbox">
                            <h1>Conclusion</h1>
                            <p>
                                Again, creating this piece seems to have helped me understand and forge my path in design work.
                            </p>
                            <br />
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/web-design-templates/archibald-butler/">View the work!</a>
                            <a className="btn btn-secondary readmoreBTN d-inline"
                                href="https://archibaldbutler.com/free-photoshop-downloads/wood-woody-web-template-photoshop-free-download.zip">Download
                    it!</a>
                        </div>
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A web design illustration by Archibald Butler" src={ArchieWebImg3} />
                            <figcaption>
                                <h2><span>The Archie’s web design</span></h2>
                                <p> Website illustration and animation techniques</p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        );
    }
}
export default ArchieWeb;