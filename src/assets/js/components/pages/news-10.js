import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import "../../../css/h_thumbs.scss";

import News4Img13 from "../../../img/internals/news-4/news-4-img-13.jpg";
import News4Img14 from "../../../img/internals/news-4/news-4-img-14.jpg";
import News4Img15 from "../../../img/internals/news-4/news-4-img-15.jpg";
import News4Img16 from "../../../img/internals/news-4/news-4-img-16.jpg";

import News4Img12 from "../../../img/internals/news-4/news-4-img-12.png";
import News4Img11 from "../../../img/internals/news-4/news-4-img-11.jpg";

import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News10 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A website development into bespoke WordPress by Archibald Butler"
        src={News4Img11}
       />
       <figcaption>
        <h2>
         A Photoshop <span>Design</span>
        </h2>
        <p>I did in 2006.</p>
        <a href="https://archibaldbutler.com/web-design-templates/whimsy/">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An illustration of a family on the beach with dogs in california"
        src={News4Img12}
       />
       <figcaption>
        <h2>
         An interactive <span>Illustration</span> from 2005
        </h2>
        <p>
         When I was still at Uni I produced this. One of my first pieces of
         interaction design.
        </p>
        <Link to="/interactive-map/">See</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A bespoke web dev job on Tumblr for ASOS by Archibald Butler"
        src={News4Img13}
       />
       <figcaption>
        <h2>
         A graffiti <span>Stencil</span>
        </h2>
        <p>
         I don’t like this very much.{" "}
         <span aria-label="happy face" role="img">
          🙁
         </span>
        </p>
        <Link to="/smokey/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="RELLIK Fashion" src={News4Img14} />
       <figcaption>
        <h2>
         One of my old school <span>Illustrations</span>
        </h2>
        <p>
         This is when I first started understanding my art and web synergy at 26
         years old.
        </p>
        <Link to="/web-utopia/">See the art</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A business website design for Roaming Giraffe by illustrator Archibald Butler"
        src={News4Img15}
       />
       <figcaption>
        <h2>
         See a free PSD: Fresh and <span>Green</span>
        </h2>
        <p>A Photoshop website template with custom fonts and logo.</p>
        <a href="https://archibaldbutler.com/web-design-templates/fresh/">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A fishing lake website design illustration by Archibald Butler"
        src={News4Img16}
       />
       <figcaption>
        <h2>
         One of my favorite <span>Photoshop designs</span>
        </h2>
        <p>
         I love this drawing, I wish I could have animated the frog to speak!
        </p>
        <Link to="/sarah-beeny/">View more</Link>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-9/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-11/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News10;
