import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import "../../../css/h_thumbs.scss";

import advancedPhotoshopImg from "../../../img/internals/news/6-archibald-butler-featured-in-advanced-photoshop-magazine.jpg";
import News2Img1 from "../../../img/internals/news-2/news-2-img-1.jpg";
import News2Img2 from "../../../img/internals/news-2/news-2-img-2.gif";
import News2Img3 from "../../../img/internals/news-2/news-2-img-3.jpg";
import activeWear from "../../../img/internals/work/activewear.jpg";
import allIllustrationsImg from "../../../img/internals/news/1-all-illustrations-from-art-school-london-archibald-butler.jpg";

import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News5 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A bespoke web dev job on Tumblr for ASOS by Archibald Butler"
        src={activeWear}
       />
       <figcaption>
        <h2>
         New Look Menswear Campaign: <span>Activewear</span>
        </h2>
        <p>An elaborate html page with lots of cool features!</p>

        <Link to="/new-look-head-office/">See it</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An animated website design by Archibald Butler"
        src={allIllustrationsImg}
       />
       <figcaption>
        <h2>
         All my Art school work <span>From CSM!</span>
        </h2>
        <p>Check out 251 hand drawings of mine!</p>

        <Link to="/illustrations-archibald-butler-art-school/">See Icons</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A bespoke web dev job on Tumblr for ASOS by Archibald Butler"
        src={advancedPhotoshopImg}
       />
       <figcaption>
        <h2>
         A 4 page feature <span>on my web design work</span>
        </h2>
        <p>
         Advanced Photoshop Magazine interviewed me for an article on wood
         textures. Needless to say: I was chuffed!
        </p>
        <Link to="/advanced-photoshop/">View more</Link>
       </figcaption>
      </figure>
     </div>

     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="An illustration of a surfers work dilemma" src={News2Img1} />
       <figcaption>
        <h2>Surfer’s dilemma</h2>
        <p>3.5 hours tops from blank paper.</p>
        <Link to="/news/ying-and-yang-illustration/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="Skateboard illustration of Archie comics" src={News2Img2} />
       <figcaption>
        <h2>
         Ha! <span>Click to see some work.</span>
        </h2>
        <p>Been busy sketching on a rainy Saturday.</p>
        <Link to="/work/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A website development into bespoke WordPress by Archibald Butler"
        src={News2Img3}
       />
       <figcaption>
        <h2>
         A Highly Renowned <span>Agency in London</span>
        </h2>
        <p>
         A full development into a CMS from their PSD designs as sole developer.
        </p>
        <Link to="/our-design-agency/">View more</Link>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-4/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-6/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News5;
