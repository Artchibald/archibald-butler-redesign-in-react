import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";

import "../../../css/h_thumbs.scss";

import archImg from "../../../img/internals/work/archibaldbutler-com-making-of.jpg";
import brewed from "../../../img/internals/news/brewed-coffee.jpg";
import lizBradley from "../../../img/internals/liz-bradley/liz-bradley-thumb.jpg";
import elandCables from "../../../img/internals/eland/eland.jpg";
import iTrust from "../../../img/internals/itrust/itrust-compressed.png";
import LondonPrestigeImg1 from "../../../img/internals/london-prestige/london-prestige1.jpg";

import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News2 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="itrust" src={iTrust} />
       <figcaption>
        <h2>
         iTrust <span>Finance</span>
        </h2>
        <p>
         Des and dev'd a fantastic bespoke crypto DeFi in React and Material UI
        </p>
        <Link to="/itrust/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="Brewed coffee UK" src={brewed} />
       <figcaption>
        <h2>
         Brewed <span>Online</span>
        </h2>
        <p>Worked on a fantastic bespoke des and dev coffee shop</p>
        <Link to="/brewed-online/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A business website design for Roaming Giraffe by illustrator Archibald Butler"
        src={lizBradley}
       />
       <figcaption>
        <h2>
         Liz Bradley <span>Watercolors</span>
        </h2>
        <p>She has sold loads since launch!</p>
        <a aria-label="Visit work" href="https://www.lizbradley.co.uk/">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A business website design for Roaming Giraffe by illustrator Archibald Butler"
        src={elandCables}
       />
       <figcaption>
        <h2>
         Eland <span>Cables</span>
        </h2>
        <p>Working as a front end guy in a great team!</p>
        <a aria-label="Visit work" href="https://www.elandcables.com/">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="The making of this website designed by Archibald Butler"
        src={archImg}
       />
       <figcaption>
        <h2>
         The Making of <span>this website</span>
        </h2>
        <p>Find out about my illustration techniques and css animations!</p>
        <Link to="/making-an-illustrated-website/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A UX event website by Archibald Butler"
        src={LondonPrestigeImg1}
       />
       <figcaption>
        <h2>
         It's raining <span>Nerd Awards</span>
        </h2>
        <p>Hallelujah! It's raining Web Developer of the year.</p>
        <Link to="/london-prestige/">See it</Link>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-1/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-3/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News2;
