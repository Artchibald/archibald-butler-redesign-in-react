import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import "../../../css/h_thumbs.scss";

import News4Img3 from "../../../img/internals/news-4/news-4-img-3.jpg";
import News3Img7 from "../../../img/internals/news-3/news-3-img-7.jpg";
import News3Img8 from "../../../img/internals/news-3/news-3-img-8.jpg";
import News3Img9 from "../../../img/internals/news-3/news-3-img-9.jpg";
import News2Img4 from "../../../img/internals/news-2/news-2-img-4.jpg";
import News4Img6 from "../../../img/internals/news-4/news-4-img-6.jpg";

import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News8 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A bespoke web dev job on Tumblr for ASOS by Archibald Butler"
        src={News2Img4}
       />
       <figcaption>
        <h2>
         New Look <span>Extended sizes</span>
        </h2>
        <p>A cool help page for finding your size at New Look</p>
        <a
         href="https://www.archibaldbutler.com/projects/new-look-extended-sizes/index.html"
         //onClick="ga('send', 'event', 'work','extendedSizes','extendedSizesimgLink');"
        >
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A business website design for Roaming Giraffe by illustrator Archibald Butler"
        src={News4Img6}
       />
       <figcaption>
        <h2>
         J Hair <span>and Beauty</span>
        </h2>
        <p>A custom psd I integrated into a CMS. Solo developer style.</p>
        <Link to="/how-to-be/">View more</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A fishing lake website design illustration by Archibald Butler"
        src={News3Img7}
       />
       <figcaption>
        <h2>
         Become One <span>Marketing</span>
        </h2>
        <p>
         A marketing agency whose site I designed, I worked on this development
         project alone.
        </p>
        <a href="/become-one/">View more</a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="Freddie to get fit a wordpress template designed and developed by Archibald Butler"
        src={News3Img8}
       />
       <figcaption>
        <h2>
         B7 <span>Innovation</span>
        </h2>
        <p>
         Another marketing agency website with custom fonts and functions, I
         worked on this project alone.
        </p>
        <a
         target="_blank"
         rel="noopener noreferrer"
         className="btn btn-primary"
         href="https://b7innovation.com/"
        >
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An illustration of a family on the beach with dogs in california"
        src={News3Img9}
       />
       <figcaption>
        <h2>
         Field<span>lounge</span>
        </h2>
        <p>
         Another website I worked on alone for a private client who rents
         gazebos.
        </p>
        <a href="https://archibaldbutler.com/projects/fieldlounge/logos.html">
         See
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A website development into bespoke WordPress by Archibald Butler"
        src={News4Img3}
       />
       <figcaption>
        <h2>
         Ripper <span>T-shirts</span>
        </h2>
        <p>
         An E-commerce website with custom fonts and logos, I worked on this
         site solo.
        </p>
        <a href="https://rippertshirts.co.uk/">View more</a>
       </figcaption>
      </figure>
     </div>
    </div>

    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-7/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-9/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     {/*<div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A bespoke web dev job on Tumblr for ASOS by Archibald Butler" src={News2Img4} />
                            <figcaption>
                                <h2>New Look <span>Extended sizes</span></h2>
                                <p>A cool help page for finding your size at New Look</p>
                                <a href="https://www.archibaldbutler.com/projects/new-look-extended-sizes/index.html" onClick="ga('send', 'event', 'work','extendedSizes','extendedSizesimgLink');">View more</a>
                            </figcaption>
                        </figure>
                    </div>*/}
     {/*<div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
                        <figure className="effect-roxy">
                            <img alt="A business website design for Roaming Giraffe by illustrator Archibald Butler"
                                src={News4Img6} />
                            <figcaption>
                                <h2>J Hair <span>and Beauty</span></h2>
                                <p>A custom psd I integrated into a CMS. Solo developer style.</p>
                                <Link to="/how-to-be/">View more</Link>
                            </figcaption>
                        </figure>
                    </div>  */}
    </div>
   </div>
  );
 }
}
export default News8;
