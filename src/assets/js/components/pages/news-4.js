import React, { Component } from "react";
import { BrowserRouter as Route, Link } from "react-router-dom";
import "../../../css/h_thumbs.scss";
import "../../../css/h_1_3d_web_package.scss";

import gifAnimationImg from "../../../img/internals/news/2-gif-css-animate-illustration.gif";
import freeIconsImg from "../../../img/internals/news/3-icons-for-free-download.jpg";
import emmaRidleyImg from "../../../img/internals/news/4-html-animated-illustration-for-emma-ridley.jpg";
import earthGangImg from "../../../img/internals/work/earthgang-Archibald-Butler-Web-Development.jpg";
import tvDanceLive from "../../../img/internals/tv-dance-live/tv-dance-live.jpg";

import chevronLeftImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-left.png";
import chevronRightImg from "../../../img/internals/news/archibald-butler-animations-for-web-chevron-right.png";

class News4 extends Component {
 render() {
  return (
   <div className="container-fluid news">
    <Route />
    <div className="row no-pad">
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A business website design for Roaming Giraffe by illustrator Archibald Butler"
        src={tvDanceLive}
       />
       <figcaption>
        <h2>
         Pro Dancing <span>Live!</span>
        </h2>
        <p>Custom built A to Z by me</p>
        <a aria-label="Visit work" href="https://tvdance.live/">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="A UX event website by Archibald Butler" src={freeIconsImg} />
       <figcaption>
        <h2>
         FREE ICONS <span>DOWNLOAD!</span>
        </h2>
        <p>You saw it here first!</p>
        <Link to="/free-illustrated-icons/">See Icons</Link>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="A UX event website by Archibald Butler"
        src={gifAnimationImg}
       />

       <figcaption>
        <h2>
         New <span>illustrations!</span>
        </h2>
        <p>Been busy drawing and coding for new clients</p>
        <Link to="/study-right/">See Icons</Link>
       </figcaption>
      </figure>
     </div>

     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img
        alt="An animated web design illustration by Archibald Butler"
        src={emmaRidleyImg}
       />
       <figcaption>
        <h2>
         Burlesque <span>Shenanigans</span>
        </h2>
        <p>This is a fun project I did for a friend with GIFs and .js.</p>
        <a aria-label="See work" href="/emma-ridley/">
         View more
        </a>
       </figcaption>
      </figure>
     </div>
     <div className="col-12 col-sm-6 col-md-4 col-lg-4 no-pad three-d-box">
      <div
       style={{
        borderTop: "1.5px solid white",
        borderLeft: "2.5px solid white",
        borderRight: "2.5px solid white",
        borderBottom: "0px solid white",
        position: "relative",
        display: "block",
        clear: "both",
        height: "100%",
        backgroundColor: "#b8e4eb",
       }}
      >
       <div id="tridiv">
        <div
         className="scene"
         style={{ transform: "rotateX(349deg) rotateY(347deg)" }}
        >
         <div className="shape cuboid-1 cub-1">
          <div className="face ft"></div>
          <div className="face bk"></div>
          <div className="face rt"></div>
          <div className="face lt"></div>
          <div className="face bm"></div>
          <div className="face tp"></div>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-pad">
      <figure className="effect-roxy">
       <img alt="A UX event website by Archibald Butler" src={earthGangImg} />
       <figcaption>
        <h2>
         Doing more <span>Art</span>
        </h2>
        <p>A side project for a band</p>
        <Link to="/earthgang-portfolio/">See it</Link>
       </figcaption>
      </figure>
     </div>
    </div>
    <div className="row no-pad">
     <div className="col-4 col-sm-1 col-md-1 col-lg-1 no-pad">
      <div className="crop1">
       <Link className="previous-link" to="/news-3/">
        <div className="animate1">
         <img
          src={chevronLeftImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
     <div className="offset-4 col-4 offset-sm-10 col-sm-1 offset-md-10 col-md-1 offset-lg-10 col-lg-1 no-pad">
      <div className="crop2">
       <Link className="next-link" to="/news-5/">
        <div className="animate2">
         <img
          src={chevronRightImg}
          alt="Chevron right: Next page of blog article news"
          scale="0"
         />
        </div>
       </Link>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
export default News4;
