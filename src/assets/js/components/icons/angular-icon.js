import React, { Component } from 'react';
import angular from '../../../img/icons/angular.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var angularBg = {
    backgroundImage: `url(${angular})`
};
class AngularIcon extends Component {
    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Angular"><div className="icon-roll" style={angularBg}></div></button>

        );
    }
}

export default AngularIcon;