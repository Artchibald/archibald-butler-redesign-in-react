import React, { Component } from 'react';
import Cpanel from '../../../img/icons/cpanel.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var CpanelBg = {
    backgroundImage: `url(${Cpanel})`
};
class CpanelIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Cpanel"><div className="icon-roll" style={CpanelBg}></div></button>

        );
    }
}
export default CpanelIcon;