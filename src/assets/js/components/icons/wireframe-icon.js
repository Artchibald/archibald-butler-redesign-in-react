import React, { Component } from 'react';
import Wireframe from '../../../img/icons/wireframes.jpg';
// import { BrowserRouter as Link } from "Wireframe-router-dom";
//import artBg from '../img/archies-art.png';
var WireframeBg = {
    backgroundImage: `url(${Wireframe})`
};
class WireframeIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Wireframes"><div className="icon-roll" style={WireframeBg}></div></button>

        );
    }
}
export default WireframeIcon;