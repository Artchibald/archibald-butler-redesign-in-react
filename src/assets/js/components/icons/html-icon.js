import React, { Component } from 'react';
import Html from '../../../img/icons/html.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var HtmlBg = {
    backgroundImage: `url(${Html})`
};
class HtmlIcon extends Component {


    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Html"><div className="icon-roll" style={HtmlBg}></div></button>

        );
    }
}
export default HtmlIcon;