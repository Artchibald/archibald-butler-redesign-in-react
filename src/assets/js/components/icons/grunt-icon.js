import React, { Component } from "react";
import Grunt from "../../../img/icons/grunt.jpg";
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var GruntBg = {
 backgroundImage: `url(${Grunt})`,
};
class GruntIcon extends Component {
 render() {
  return (
   <button
    href="#"
    className="linkForTooltip"
    data-toggle="tooltip"
    data-placement="bottom"
    title=""
    data-original-title="Grunt"
   >
    <div className="icon-roll" style={GruntBg}></div>
   </button>
  );
 }
}
export default GruntIcon;
