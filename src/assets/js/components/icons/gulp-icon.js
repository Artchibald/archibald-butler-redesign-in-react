import React, { Component } from 'react';
import Gulp from '../../../img/icons/gulp.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var GulpBg = {
    backgroundImage: `url(${Gulp})`
};
class GulpIcon extends Component {


    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Gulp"><div className="icon-roll" style={GulpBg}></div></button>

        );
    }
}
export default GulpIcon;