import React, { Component } from 'react';
import Jquery from '../../../img/icons/jquery.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var JqueryBg = {
    backgroundImage: `url(${Jquery})`
};
class JqueryIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Jquery"><div className="icon-roll" style={JqueryBg}></div></button>

        );
    }
}
export default JqueryIcon;