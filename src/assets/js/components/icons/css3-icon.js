import React, { Component } from 'react';
import Css3 from '../../../img/icons/css3.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var Css3Bg = {
    backgroundImage: `url(${Css3})`
};
class Css3Icon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Css3"><div className="icon-roll" style={Css3Bg}></div></button>

        );
    }
}
export default Css3Icon;