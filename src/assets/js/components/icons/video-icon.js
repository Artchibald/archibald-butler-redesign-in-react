import React, { Component } from 'react';
import Video from '../../../img/icons/video.jpg';
// import { BrowserRouter as Link } from "Video-router-dom";
//import artBg from '../img/archies-art.png';
var VideoBg = {
    backgroundImage: `url(${Video})`
};
class VideoIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Video Css"><div className="icon-roll" style={VideoBg}></div></button>

        );
    }
}
export default VideoIcon;