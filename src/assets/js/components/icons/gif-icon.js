import React, { Component } from 'react';
import Gif from '../../../img/icons/gif.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var GifBg = {
    backgroundImage: `url(${Gif})`
};
class GifIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Gif"><div className="icon-roll" style={GifBg}></div></button>

        );
    }
}
export default GifIcon;