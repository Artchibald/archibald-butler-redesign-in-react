import React, { Component } from 'react';
import Bitbucket from '../../../img/icons/bitbucket.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var BitbucketBg = {
    backgroundImage: `url(${Bitbucket})`
};
class BitbucketIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Bitbucket"><div className="icon-roll" style={BitbucketBg}></div></button>

        );
    }
}
export default BitbucketIcon;