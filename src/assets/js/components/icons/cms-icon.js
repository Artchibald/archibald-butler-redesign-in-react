import React, { Component } from 'react';
import Cms from '../../../img/icons/cms.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var CmsBg = {
    backgroundImage: `url(${Cms})`
};
class CmsIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Cms"><div className="icon-roll" style={CmsBg}></div></button>

        );
    }
}
export default CmsIcon;