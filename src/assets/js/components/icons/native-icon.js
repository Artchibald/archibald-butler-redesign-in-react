import React, { Component } from 'react';
import Native from '../../../img/icons/native.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var NativeBg = {
    backgroundImage: `url(${Native})`
};
class NativeIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Native Js"><div className="icon-roll" style={NativeBg}></div></button>

        );
    }
}
export default NativeIcon;