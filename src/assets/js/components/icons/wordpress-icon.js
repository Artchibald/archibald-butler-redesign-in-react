import React, { Component } from 'react';
import Wordpres from '../../../img/icons/wordpress.jpg';
// import { BrowserRouter as Link } from "Wordpres-router-dom";
//import artBg from '../img/archies-art.png';
var WordpresBg = {
    backgroundImage: `url(${Wordpres})`
};
class WordpressIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Wordpress"><div className="icon-roll" style={WordpresBg}></div></button>

        );
    }
}
export default WordpressIcon;