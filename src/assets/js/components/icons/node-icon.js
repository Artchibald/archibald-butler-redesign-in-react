import React, { Component } from 'react';
import Node from '../../../img/icons/node.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var NodeBg = {
    backgroundImage: `url(${Node})`
};
class NodeIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Node"><div className="icon-roll" style={NodeBg}></div></button>

        );
    }
}
export default NodeIcon;