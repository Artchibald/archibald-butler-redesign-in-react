import React, { Component } from 'react';
import Bootstrap from '../../../img/icons/bootstrap.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var BootstrapBg = {
    backgroundImage: `url(${Bootstrap})`
};
class BootstrapIcon extends Component {
    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Bootstrap"><div className="icon-roll" style={BootstrapBg}></div></button>

        );
    }
}
export default BootstrapIcon;