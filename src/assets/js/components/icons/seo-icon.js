import React, { Component } from 'react';
import Seo from '../../../img/icons/seo.jpg';
// import { BrowserRouter as Link } from "Seo-router-dom";
//import artBg from '../img/archies-art.png';
var SeoBg = {
    backgroundImage: `url(${Seo})`
};
class SeoIcon extends Component {


    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Seo Css"><div className="icon-roll" style={SeoBg}></div></button>

        );
    }
}
export default SeoIcon;