import React, { Component } from 'react';
import Sql from '../../../img/icons/sql.jpg';
// import { BrowserRouter as Link } from "Sql-router-dom";
//import artBg from '../img/archies-art.png';
var SqlBg = {
    backgroundImage: `url(${Sql})`
};
class SqlIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Sql Css"><div className="icon-roll" style={SqlBg}></div></button>

        );
    }
}
export default SqlIcon;