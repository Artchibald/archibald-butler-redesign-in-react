import React, { Component } from 'react';
import Less from '../../../img/icons/less.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var LessBg = {
    backgroundImage: `url(${Less})`
};
class LessIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Less"><div className="icon-roll" style={LessBg}></div></button>

        );
    }
}
export default LessIcon;