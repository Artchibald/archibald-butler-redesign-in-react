import React, { Component } from 'react';
import Php from '../../../img/icons/php.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var PhpBg = {
    backgroundImage: `url(${Php})`
};
class PhpIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Php"><div className="icon-roll" style={PhpBg}></div></button>

        );
    }
}
export default PhpIcon;