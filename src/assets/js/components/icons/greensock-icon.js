import React, { Component } from 'react';
import Greensock from '../../../img/icons/greensock.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var GreensockBg = {
    backgroundImage: `url(${Greensock})`
};
class GreensockIcon extends Component {


    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Greensock"><div className="icon-roll" style={GreensockBg}></div></button>

        );
    }
}
export default GreensockIcon;