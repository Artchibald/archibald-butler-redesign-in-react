import React, { Component } from 'react';
import api from '../../../img/icons/api.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var apiBg = {
    backgroundImage: `url(${api})`
};
class ApiIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Api"><div className="icon-roll" style={apiBg}></div></button>

        );
    }
}
export default ApiIcon;