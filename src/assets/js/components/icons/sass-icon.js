import React, { Component } from 'react';
import Sass from '../../../img/icons/sass.jpg';
// import { BrowserRouter as Link } from "Sass-router-dom";
//import artBg from '../img/archies-art.png';
var SassBg = {
    backgroundImage: `url(${Sass})`
};
class SassIcon extends Component {


    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Sass Css"><div className="icon-roll" style={SassBg}></div></button>

        );
    }
}
export default SassIcon;