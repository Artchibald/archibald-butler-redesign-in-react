import React, { Component } from 'react';
import Reactive from '../../../img/icons/react.jpg';
// import { BrowserRouter as Link } from "reactive-router-dom";
//import artBg from '../img/archies-art.png';
var ReactiveBg = {
    backgroundImage: `url(${Reactive})`
};
class ReactiveIcon extends Component {

    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="React JS"><div className="icon-roll" style={ReactiveBg}></div></button>

        );
    }
}
export default ReactiveIcon;