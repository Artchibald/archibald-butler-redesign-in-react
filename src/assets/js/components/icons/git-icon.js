import React, { Component } from 'react';
import Git from '../../../img/icons/git.jpg';
// import { BrowserRouter as Link } from "react-router-dom";
//import artBg from '../img/archies-art.png';
var GitBg = {
    backgroundImage: `url(${Git})`
};
class GitIcon extends Component {

    // .greensock-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/greensockbw.jpg');
    // }
    // .grunt-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/gruntbw.jpg');
    // }
    // .gulp-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/gulpbw.jpg');
    // }
    // .html-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/htmlbw.jpg');
    // }
    // .jquery-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/jquerybw.jpg');
    // }
    // .less-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/lessbw.jpg');
    // }
    // .native-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/nativebw.jpg');
    // }
    // .node-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/nodebw.jpg');
    // }
    // .php-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/phpbw.jpg');
    // }
    // .react-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/reactbw.jpg');
    // }
    // .sass-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/sassbw.jpg');
    // }
    // .seo-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/seobw.jpg');
    // }
    // .sql-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/sqlbw.jpg');
    // }
    // .video-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/videobw.jpg');
    // }
    // .wireframes-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/wireframesbw.jpg');
    // }
    // .wordpress-roll {
    //     background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/icons/wordpressbw.jpg');
    // }
    render() {
        return (
            <button href="#" className="linkForTooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Git"><div className="icon-roll" style={GitBg}></div></button>

        );
    }
}
export default GitIcon;