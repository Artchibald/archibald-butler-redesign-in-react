import React, { Component } from 'react';
import '../css/c_loading.scss';

class testImgLoad extends Component {
    state = {
        src: `https://placeimg.com/295/295/any/tech?t=${new Date().getMilliseconds()}`,
        loaded: false
    };

    onImageLoaded = () => {
        this.setState({ loaded: true });
    };

    refreshImage = () => {
        this.setState({
            loaded: false,
            src: `https://placeimg.com/295/295/any/tech?t=${new Date().getMilliseconds()}`
        });
    };

    render() {
        const { src, loaded } = this.state;

        return (
            <div>
                <div className="image-container">
                    <img
                        alt="alt"
                        src={src}
                        onLoad={this.onImageLoaded}
                        onError={this.onImageError}
                    />
                    {!loaded && (
                        <div className="image-container-overlay">
                            <div className="loader">
                                <svg className="circular" viewBox="25 25 50 50">
                                    <circle
                                        className="path"
                                        cx="50"
                                        cy="50"
                                        r="20"
                                        fill="none"
                                        strokeWidth="2"
                                        strokeMiterlimit="10"
                                    />
                                </svg>
                            </div>
                        </div>
                    )}
                </div>
                <div>
                    <button onClick={this.refreshImage} className="button">
                        Refresh Image
        </button>
                </div>
            </div>
        );


    }
}
export default testImgLoad;