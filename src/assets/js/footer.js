import React from "react";

import facebookIcon from "../img/icons/facebook.png";
import googleIcon from "../img/icons/google.png";
import linkedinIcon from "../img/icons/linkedin.png";
import youtubeIcon from "../img/icons/youtube.png";
import instagramIcon from "../img/icons/instagram.png";
import pinterestIcon from "../img/icons/pinterest.png";

var facebookRoll = {
 backgroundImage: `url(${facebookIcon})`,
};
var googleRoll = {
 backgroundImage: `url(${googleIcon})`,
};
var linkedinRoll = {
 backgroundImage: `url(${linkedinIcon})`,
};
var youtubeRoll = {
 backgroundImage: `url(${youtubeIcon})`,
};
var instagramRoll = {
 backgroundImage: `url(${instagramIcon})`,
};
var pinterestRoll = {
 backgroundImage: `url(${pinterestIcon})`,
};

// .facebook-roll {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/facebook-bw.png');
// }
// .facebook-roll:hover {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/facebook.png');
// }
// .google-roll {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/google-bw.png');
// }
// .google-roll:hover {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/google.png');
// }
// .linkedin-roll {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/linkedin-bw.png');
// }
// .linkedin-roll:hover {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/linkedin.png');
// }
// .youtube-roll {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/youtube-bw.png');
// }
// .youtube-roll:hover {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/youtube.png');
// }
// .pinterest-roll {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/pinterest-bw.png');
// }
// .pinterest-roll:hover {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/pinterest.png');
// }
// .instagram-roll {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/instagram-bw.png');
// }
// .instagram-roll:hover {
// 	background-image: url('http://archibaldbutler.com/wp-content/themes/archibaldbutler/img/social/instagram.png');
// }

const footer = (props) => {
 return (
  <footer id="footer" className="app-footer">
   <div className="social-roll-wrap">
    <a
     aria-label="Social links"
     href="https://www.facebook.com/archibald.butler"
    >
     <span className="social-roll facebook-roll" style={facebookRoll}></span>
    </a>
    <a
     aria-label="Social links"
     href="https://plus.google.com/u/0/+ArchibaldOctaviusButler/posts"
    >
     <span className="social-roll google-roll" style={googleRoll}></span>
    </a>
    <a
     aria-label="Social links"
     href="https://www.linkedin.com/in/frontenddeveloperabutler"
    >
     <span className="social-roll linkedin-roll" style={linkedinRoll}></span>
    </a>
    <a
     aria-label="Social links"
     href="https://www.youtube.com/channel/UCxU4Lug4WBmLpGNyoEE165Q"
    >
     <span className="social-roll youtube-roll" style={youtubeRoll}></span>
    </a>
    <a
     aria-label="Social links"
     href="https://www.instagram.com/archibaldoctavius/"
    >
     <span className="social-roll instagram-roll" style={instagramRoll}></span>
    </a>
    <a
     aria-label="Social links"
     href="https://uk.pinterest.com/artchibaldsweb/"
    >
     <span className="social-roll pinterest-roll" style={pinterestRoll}></span>
    </a>
   </div>

   <p className="thanksVisit">Thanks for visiting</p>
  </footer>
 );
};
export default footer;
