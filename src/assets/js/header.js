import React, { Component } from 'react';
import { BrowserRouter as Route, Link } from 'react-router-dom';
import logo from '../img/archibald-butler-web-design-surfing-travel-logo.gif';

//import artBg from '../img/archies-art.png';
function showMenu() {
  var toggleVariable = document.getElementById('main-menu');
  //if shown
  if (toggleVariable.style.display === 'block') {
    // hide it after.5
    setTimeout(function () {
      toggleVariable.style.display = 'none';
    }, 300);
    // change the class
    toggleVariable.classList.remove('menu-on');
    toggleVariable.classList.add('menu-off');
    //stuff to remove cross from burger
    var showCross = document.getElementById('burger-cross');
    showCross.classList.remove('show-cross');
  } else {
    //otherwise show it
    toggleVariable.style.display = 'block';
    // otherwise add the class
    toggleVariable.classList.add('menu-on');
    toggleVariable.classList.remove('menu-off');
    //stuff to add cross from burger
    var showCross2 = document.getElementById('burger-cross');
    showCross2.classList.add('show-cross');
  }
}

class header extends Component {
  render() {
    return (
      <header className="app-header" id="header">
        <nav>
          <Route />
          <div id="main-menu">
            <h3>MENU</h3>

            <Link onClick={showMenu} id="firstnav" to="/">
              HOME
            </Link>
            <Link onClick={showMenu} to="/web-animation-specialist/">
              ABOUT
            </Link>
            <Link onClick={showMenu} to="/work/">
              WORK
            </Link>
            <Link onClick={showMenu} to="/news-1/">
              NEWS
            </Link>
            <Link onClick={showMenu} to="/contact/">
              CONTACT
            </Link>
            {/* <Link to="/does-not-exist">Catch all route</Link> */}
          </div>
          <Link to="/">
            <img src={logo} className="App-logo" alt="logo" />
          </Link>

          <button
            aria-label="open menu"
            className="menu-button"
            onClick={showMenu}
          >
            <div id="burger-cross" className="burger-menu">
              <div className="burger"></div>
            </div>
          </button>
        </nav>
      </header>
    );
  }
}
export default header;
